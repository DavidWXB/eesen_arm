// standard
#include <stdio.h>
#include <stdlib.h>

// projects
//#include "sleep.h"
#include "xiicps.h"
#include "xil_io.h"
#include "xuartps.h"
#include "xil_printf.h"
#include "xparameters.h"

// create ourself
#include "audio.h"
#include "./utils/platform.h"
#include "./lstm/lstm_main.h"
#include "./wfst/wfst_main.h"

// Parameter definitions
#define UART_BASEADDR XPAR_PS7_UART_1_BASEADDR
#define GPIO_BASE     XPAR_GPIO_0_BASEADDR
#define LED_CHANNEL   1

// PROTOTYPE FUNCTIONS
unsigned char IicConfig(unsigned int DeviceIdPS);
void AudioPllConfig();
void AudioWriteToReg(unsigned char u8RegAddr, unsigned char u8Data);
void AudioConfigureJacks();
void LineinLineoutConfig();
void read_superpose_play();
void audio_test();
void start();
void play();
void print();
void init_samp_data();
void SpeechRecognition();



// FBANK
int main_fbank();
int main_copyfeats();
int main_cmvn();
int main_apply_cmvn();
int main_add_deltas();

// Global variables
XIicPs Iic;
u32 I=0;
//u32 const Words=145464;
u32 const Words=64000;
u32 Val=1;
u32 WordMem32;

u32 data_temp[Words]={0};
float data_audio[Words]={0};
u32 ACQ_NUM=0;


// global variable declare

/*******************************************************
 * 			       processs flow                       *
 * 	step 1 : hardware initialize                       *
 * 	step 2 : software get data from data pool, loop    *
 * 	step 3 : software process data                     *
 ******************************************************/
int main()
{
	//init_platform();
	// step 1 : hardware initialize
	xil_printf("------------------Enter Main Fun------------------------------\r\n");

	// config the IIC data structure
	IicConfig(XPAR_XIICPS_0_DEVICE_ID);

	// config the Audio Codec's PLL
	AudioPllConfig();

	// configs the Line in and Line out ports.
	AudioConfigureJacks();

	xil_printf("-----------------ADAU1761 configured--------------------------\n\r");

	// step 2 : fetch data from pool
	// audio_test();

	// step 3 : init
	wfstInit();

	Xil_Out32(AUDIO_BASE+0x00,0x0);
	start();
	// step 4 : loop
	//while(true) {
		// printf("please speak...\n");
		// step 4.1 : get data
		// step 4.2 : features
		// step 4.3 : lstm
		// step 4.4 : wfst
		// step 4.5 : free memory
	//}
	// step 3 : extract features

	//SpeechRecognition();

	xil_printf("-----------------ADAU1761 terminated--------------------------\n\r");

    return 0;
}


void SpeechRecognition() {
	init_samp_data();

	main_fbank();
	main_copyfeats();
	main_cmvn();
	main_apply_cmvn();
	main_add_deltas();

	// step 4 : lstm
	Matrix_Float mat;
	LstmMain(&mat);
	/*
	for (int a = 0; a < mat.num_rows_; a++) {
		printf("    ");
		for (int b = 0; b < mat.num_cols_; b++)
			printf("%f,", mat.data_[a*mat.stride_ + b]);
		printf("\n");
	}
	*/

	// step 5 : wfst
	EesenMain(&mat);
}



void start() {
	u8 inp = 0x00;
	u32 CntrlRegister;

	CntrlRegister = XUartPs_ReadReg(UART_BASEADDR, XUARTPS_CR_OFFSET);

	XUartPs_WriteReg(UART_BASEADDR, XUARTPS_CR_OFFSET,
				  ((CntrlRegister & ~XUARTPS_CR_EN_DIS_MASK) |
				   XUARTPS_CR_TX_EN | XUARTPS_CR_RX_EN));



	// initialization the ctrl_reg
	Xil_Out32(AUDIO_BASE+0x00,0x0);
	printf("audio Demo\r\n");
	printf("Enter 's' to start audio acquisition\n");
	printf("Enter 'n' to entry speech recognition\n");
	printf("-----------------------------\n");

	while (!XUartPs_IsReceiveData(UART_BASEADDR));
		inp = XUartPs_ReadReg(UART_BASEADDR, XUARTPS_FIFO_OFFSET);
	switch(inp){
		case 's':
			printf("Press 'q' to restart audio acquisition\n");
			printf("Press 'p' to play the audio of acquisition\n");
			I=0;
			audio_test();
			break;
		case 'n':
			SpeechRecognition();
			break;
		default:
			start();
			break;
		} // switch

}

void audio_test() {
	u8 inx=0x00;


	//*********clk_sel_en i2s_en************/
	//Xil_Out32(AUDIO_BASE+0x00,0x1);
	//**************************************/

	//Xil_Out32(AUDIO_BASE+0x14,0x08000020); //48KHz

	//**************************************/
	//16KHz
	Xil_Out32(AUDIO_BASE+0x14,0x00400061); //16KHz


	// 48KHz
	//Xil_Out32(AUDIO_BASE+0x14,0x00410020); //ok
	//Xil_Out32(AUDIO_BASE+0x14,0x0030002b); //ok
	Xil_Out32(AUDIO_BASE+0x00,0x3);
	//**************************************/

	 while (1) {
		if(((Xil_In32(AUDIO_BASE+0x04)&0x01)==0x00)) {
			//data_temp[I]= Xil_In32(AUDIO_BASE+0x08);
			data_temp[I]= Xil_In32(AUDIO_BASE+0x10);
			Xil_Out32(AUDIO_BASE+0x0c, data_temp[I]);
			I++;
		}
		if(I>Words) break;
	}

	printf("the number of audio data is %ld\n", I);

	//inx=XUartPs_ReadReg(UART_BASEADDR, XUARTPS_FIFO_OFFSET);
	while(1) {
		inx=XUartPs_ReadReg(UART_BASEADDR, XUARTPS_FIFO_OFFSET);
		switch(inx){
				case 'q':
					start();
					break;
				case 'p':
					SpeechRecognition();
					start();
					break;
				default:
					//audio_test();
					break;
		}
	}
}

void init_samp_data() {
	s16 x;
	for(int i=0;i<Words;i++) {
		x=data_temp[i];
		data_audio[i]=x;
	}
}


void print() {
	s16 x;
	printf("return the acquisition number : %d\n", ACQ_NUM);
	printf("data_audio[32000]=");
	for(int i=0; i<64000; i++) {
		x=data_temp[i];
		data_audio[i]=x;
		printf("%.1f,",data_audio[i]);
		if(i%1000==999) printf("\n");
	}
}

void play() {
	for (int i = 0; i < Words; i++) {
			WordMem32 = data_temp[i];
			if(!XUartPs_IsTransmitFull(UART_BASEADDR))
				Xil_Out32(AUDIO_BASE+0x0c, WordMem32);
	}
}

void read_superpose_play(void)
{
	u32  in_left, in_right, out_left, out_right;

	while (!XUartPs_IsReceiveData(UART_BASEADDR)){


		// 采集到的左右声道的数据
		in_left = Xil_In32(I2S_DATA_RX_L_REG);
		in_right = Xil_In32(I2S_DATA_RX_R_REG);

		out_left =    in_left ;
		out_right =   in_right ;

		//输出
		Xil_Out32(I2S_DATA_TX_L_REG, out_left);
		Xil_Out32(I2S_DATA_TX_R_REG, out_right);

		}
}

/* ---------------------------------------------------------------------------- *
 * 									IicConfig()									*
 * ---------------------------------------------------------------------------- *
 * Initialises the IIC driver by looking up the configuration in the config
 * table and then initialising it. Also sets the IIC serial clock rate.
 * ---------------------------------------------------------------------------- */
unsigned char IicConfig(unsigned int DeviceIdPS)
{
	XIicPs_Config *Config;
	int Status;

	/* Initialise the IIC driver so that it's ready to use */

	// Look up the configuration in the config table
	Config = XIicPs_LookupConfig(DeviceIdPS);
	if(NULL == Config) {
		return XST_FAILURE;
	}

	// Initialise the IIC driver configuration
	Status = XIicPs_CfgInitialize(&Iic, Config, Config->BaseAddress);
	if(Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	//Set the IIC serial clock rate.
	XIicPs_SetSClk(&Iic, IIC_SCLK_RATE);

	return XST_SUCCESS;
}

/* ---------------------------------------------------------------------------- *
 * 								AudioPllConfig()								*
 * ---------------------------------------------------------------------------- *
 * Configures audio codes's internal PLL. With MCLK = 10 MHz it configures the
 * PLL for a VCO frequency = 49.152 MHz, and an audio sample rate of 48 KHz.
 * ---------------------------------------------------------------------------- */
void AudioPllConfig() {

	unsigned char u8TxData[8], u8RxData[6];
	int Status;

	Status = IicConfig(XPAR_XIICPS_0_DEVICE_ID);
	if(Status != XST_SUCCESS) {
		xil_printf("\nError initializing IIC");

	}

	// Disable Core Clock
	AudioWriteToReg(R0_CLOCK_CONTROL, 0x0E);

	/* 	MCLK = 10 MHz
		R = 0100 = 4, N = 0x02 0x3C = 572, M = 0x02 0x71 = 625

		PLL required output = 1024x48 KHz
		(PLLout)			= 49.152 MHz

		PLLout/MCLK			= 49.152 MHz/10 MHz
							= 4.9152 MHz
							= R + (N/M)
							= 4 + (572/625) */

	// Write 6 bytes to R1 @ register address 0x4002
	/*u8TxData[0] = 0x40; // Register write address [15:8]
	u8TxData[1] = 0x02; // Register write address [7:0]
	u8TxData[2] = 0x02; // byte 6 - M[15:8]
	u8TxData[3] = 0x71; // byte 5 - M[7:0]
	u8TxData[4] = 0x02; // byte 4 - N[15:8]
	u8TxData[5] = 0x3C; // byte 3 - N[7:0]
	u8TxData[6] = 0x21; // byte 2 - 7 = reserved, bits 6:3 = R[3:0], 2:1 = X[1:0], 0 = PLL operation mode
	u8TxData[7] = 0x01; // byte 1 - 7:2 = reserved, 1 = PLL Lock, 0 = Core clock enable
	*/



	/* 	MCLK = 8 MHz
		R = 0010 = 2, N = 0x00 0x06 = 6, M = 0x00 0x7d = 125

		PLL required output = 1024x16 KHz
		(PLLout)			= 16.384 MHz

		PLLout/MCLK			= 16.384 MHz/8 MHz
							= R + (N/M)
							= 2 + (6/125) */

	// Write 6 bytes to R1 @ register address 0x4002
	u8TxData[0] = 0x40; // Register write address [15:8]
	u8TxData[1] = 0x02; // Register write address [7:0]
	u8TxData[2] = 0x00; // byte 6 - M[15:8]
	u8TxData[3] = 0x7d; // byte 5 - M[7:0]
	u8TxData[4] = 0x00; // byte 4 - N[15:8]
	u8TxData[5] = 0x12; // byte 3 - N[7:0]
	u8TxData[6] = 0x31; // byte 2 - 7 = reserved, bits 6:3 = R[3:0], 2:1 = X[1:0], 0 = PLL operation mode
	u8TxData[7] = 0x01; // byte 1 - 7:2 = reserved, 1 = PLL Lock, 0 = Core clock enable



	// Write bytes to PLL Control register R1 @ 0x4002
	XIicPs_MasterSendPolled(&Iic, u8TxData, 8, (IIC_SLAVE_ADDR >> 1));
	while(XIicPs_BusIsBusy(&Iic));

	// Register address set: 0x4002
	u8TxData[0] = 0x40;
	u8TxData[1] = 0x02;

	// Poll PLL Lock bit
	do {
		XIicPs_MasterSendPolled(&Iic, u8TxData, 2, (IIC_SLAVE_ADDR >> 1));
		while(XIicPs_BusIsBusy(&Iic));
		XIicPs_MasterRecvPolled(&Iic, u8RxData, 6, (IIC_SLAVE_ADDR >> 1));
		while(XIicPs_BusIsBusy(&Iic));
	}
	while((u8RxData[5] & 0x02) == 0); // while not locked

	AudioWriteToReg(R0_CLOCK_CONTROL, 0x0F);	// 1111
												// bit 3:		CLKSRC = PLL Clock input
												// bits 2:1:	INFREQ = 1024 x fs
												// bit 0:		COREN = Core Clock enabled
}


/* ---------------------------------------------------------------------------- *
 * 								AudioWriteToReg									*
 * ---------------------------------------------------------------------------- *
 * Function to write one byte (8-bits) to one of the registers from the audio
 * controller.
 * ---------------------------------------------------------------------------- */
void AudioWriteToReg(unsigned char u8RegAddr, unsigned char u8Data) {

	unsigned char u8TxData[3];

	u8TxData[0] = 0x40;
	u8TxData[1] = u8RegAddr;
	u8TxData[2] = u8Data;

	XIicPs_MasterSendPolled(&Iic, u8TxData, 3, (IIC_SLAVE_ADDR >> 1));
	while(XIicPs_BusIsBusy(&Iic));
}

/* ---------------------------------------------------------------------------- *
 * 								AudioConfigureJacks()							*
 * ---------------------------------------------------------------------------- *
 * Configures audio codes's various mixers, ADC's, DAC's, and amplifiers to
 * accept stereo input from line in and push stereo output to line out.
 * ---------------------------------------------------------------------------- */
void AudioConfigureJacks()
{
	//AudioWriteToReg(R4_RECORD_MIXER_LEFT_CONTROL_0, 0x01); //enable mixer 1
	AudioWriteToReg(R4_RECORD_MIXER_LEFT_CONTROL_0, 0x0f);
	AudioWriteToReg(R5_RECORD_MIXER_LEFT_CONTROL_1, 0x07); //unmute Left channel of line in into mxr 1 and set gain to 6 db
	AudioWriteToReg(R6_RECORD_MIXER_RIGHT_CONTROL_0, 0x0f); //enable mixer 2


	AudioWriteToReg(R8_LEFT_DIFFERENTIAL_INPUT_VOLUME_CONTROL,0x03);
	AudioWriteToReg(R9_RIGHT_DIFFERENTIAL_INPUT_VOLUME_CONTROL,0x03);
	AudioWriteToReg(R10_RECORD_MICROPHONE_BIAS_CONTROL, 0x01);

	AudioWriteToReg(R7_RECORD_MIXER_RIGHT_CONTROL_1, 0x07); //unmute Right channel of line in into mxr 2 and set gain to 6 db
	AudioWriteToReg(R19_ADC_CONTROL, 0x13); //enable ADCs

	AudioWriteToReg(R22_PLAYBACK_MIXER_LEFT_CONTROL_0, 0x21); //unmute Left DAC into Mxr 3; enable mxr 3
	AudioWriteToReg(R24_PLAYBACK_MIXER_RIGHT_CONTROL_0, 0x41); //unmute Right DAC into Mxr4; enable mxr 4
	AudioWriteToReg(R26_PLAYBACK_LR_MIXER_LEFT_LINE_OUTPUT_CONTROL, 0x05); //unmute Mxr3 into Mxr5 and set gain to 6db; enable mxr 5
	AudioWriteToReg(R27_PLAYBACK_LR_MIXER_RIGHT_LINE_OUTPUT_CONTROL, 0x11); //unmute Mxr4 into Mxr6 and set gain to 6db; enable mxr 6
	AudioWriteToReg(R29_PLAYBACK_HEADPHONE_LEFT_VOLUME_CONTROL, 0xFF);//Mute Left channel of HP port (LHP)
	AudioWriteToReg(R30_PLAYBACK_HEADPHONE_RIGHT_VOLUME_CONTROL, 0xFF); //Mute Right channel of HP port (LHP)
	//AudioWriteToReg(R31_PLAYBACK_LINE_OUTPUT_LEFT_VOLUME_CONTROL, 0xE6); //set LOUT volume (0db); unmute left channel of Line out port; set Line out port to line out mode
	//AudioWriteToReg(R32_PLAYBACK_LINE_OUTPUT_RIGHT_VOLUME_CONTROL, 0xE6); // set ROUT volume (0db); unmute right channel of Line out port; set Line out port to line out mode
	AudioWriteToReg(R31_PLAYBACK_LINE_OUTPUT_LEFT_VOLUME_CONTROL, 0xFE); //set LOUT volume (0db); unmute left channel of Line out port; set Line out port to line out mode
	AudioWriteToReg(R32_PLAYBACK_LINE_OUTPUT_RIGHT_VOLUME_CONTROL, 0xFE); // set ROUT volume (0db); unmute right channel of Line out port; set Line out port to line out mode
	AudioWriteToReg(R35_PLAYBACK_POWER_MANAGEMENT, 0x03); //enable left and right channel playback (not sure exactly what this does...)
	AudioWriteToReg(R36_DAC_CONTROL_0, 0x03); //enable both DACs

	AudioWriteToReg(R58_SERIAL_INPUT_ROUTE_CONTROL, 0x01); //Connect I2S serial port output (SDATA_O) to DACs
	AudioWriteToReg(R59_SERIAL_OUTPUT_ROUTE_CONTROL, 0x01); //connect I2S serial port input (SDATA_I) to ADCs

	AudioWriteToReg(R65_CLOCK_ENABLE_0, 0x7F); //Enable clocks
	AudioWriteToReg(R66_CLOCK_ENABLE_1, 0x03); //Enable rest of clocks

	//AudioWriteToReg(R17_CONVERTER_CONTROL_0, 0x00);//48 KHz
	//AudioWriteToReg(R64_SERIAL_PORT_SAMPLING_RATE, 0x00);//48 KHz

	AudioWriteToReg(R17_CONVERTER_CONTROL_0, 0x03);//16 KHz
	AudioWriteToReg(R64_SERIAL_PORT_SAMPLING_RATE, 0x03);//16 KHz
}

/* ---------------------------------------------------------------------------- *
 * 								LineinLineoutConfig()							*
 * ---------------------------------------------------------------------------- *
 * Configures Line-In input, ADC's, DAC's, Line-Out and HP-Out.
 * ---------------------------------------------------------------------------- */
void LineinLineoutConfig() {

	AudioWriteToReg(R17_CONVERTER_CONTROL_0, 0x05);//48 KHz
	AudioWriteToReg(R64_SERIAL_PORT_SAMPLING_RATE, 0x05);//48 KHz
	AudioWriteToReg(R19_ADC_CONTROL, 0x13);
	AudioWriteToReg(R36_DAC_CONTROL_0, 0x03);
	AudioWriteToReg(R35_PLAYBACK_POWER_MANAGEMENT, 0x03);
	AudioWriteToReg(R58_SERIAL_INPUT_ROUTE_CONTROL, 0x01);
	AudioWriteToReg(R59_SERIAL_OUTPUT_ROUTE_CONTROL, 0x01);
	AudioWriteToReg(R65_CLOCK_ENABLE_0, 0x7F);
	AudioWriteToReg(R66_CLOCK_ENABLE_1, 0x03);

	AudioWriteToReg(R4_RECORD_MIXER_LEFT_CONTROL_0, 0x01);
	AudioWriteToReg(R5_RECORD_MIXER_LEFT_CONTROL_1, 0x05);//0 dB gain
	AudioWriteToReg(R6_RECORD_MIXER_RIGHT_CONTROL_0, 0x01);
	AudioWriteToReg(R7_RECORD_MIXER_RIGHT_CONTROL_1, 0x05);//0 dB gain

	AudioWriteToReg(R22_PLAYBACK_MIXER_LEFT_CONTROL_0, 0x21);
	AudioWriteToReg(R24_PLAYBACK_MIXER_RIGHT_CONTROL_0, 0x41);
	AudioWriteToReg(R26_PLAYBACK_LR_MIXER_LEFT_LINE_OUTPUT_CONTROL, 0x03);//0 dB
	AudioWriteToReg(R27_PLAYBACK_LR_MIXER_RIGHT_LINE_OUTPUT_CONTROL, 0x09);//0 dB
	AudioWriteToReg(R29_PLAYBACK_HEADPHONE_LEFT_VOLUME_CONTROL, 0xE7);//0 dB
	AudioWriteToReg(R30_PLAYBACK_HEADPHONE_RIGHT_VOLUME_CONTROL, 0xE7);//0 dB
	AudioWriteToReg(R31_PLAYBACK_LINE_OUTPUT_LEFT_VOLUME_CONTROL, 0xE6);//0 dB
	AudioWriteToReg(R32_PLAYBACK_LINE_OUTPUT_RIGHT_VOLUME_CONTROL, 0xE6);//0 dB
}

