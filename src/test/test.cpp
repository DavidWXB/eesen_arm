#include <string>
#include <limits>
#include <float.h>
#include <fstream>
#include <iostream>
#include <string.h>
#include "test.h"
#include "../decode/TokenList.h"
#include "../decode/DecodableInterface.h"
#include "../wfst/VectorFstOfCompactLatticeArc.h"

using namespace std;

#ifdef TEST_WFST
float g_dataTestUse[248*48*3];
void TestReadMatrix2(Matrix_Float* mat)
{
	std::ifstream  infile;
	infile.open("E:\\source_code\\essen_opt\\WFSTForC\\wfst_model\\priorafter");
	std::istream *ids = &infile;
	std::istream &is = *ids;
	bool binary;
	bool add;
	std::string str;
	is >> str;          // get a token
	if (is.fail()) {
	}

	// At this point, we have read "[".
	std::vector<std::vector<float>* > data;
	std::vector<float> *cur_row = new std::vector<float>;
	while (1) {
		int i = is.peek();
		if (i == -1) {
		}
		else if (static_cast<char>(i) == ']') {  // Finished reading matrix.
			is.get();  // eat the "]".
			i = is.peek();
			if (static_cast<char>(i) == '\r') {
				is.get();
				is.get();   // get \r\n (must eat what we wrote)
			}
			else if (static_cast<char>(i) == '\n') {
                is.get();   // get \n (must eat what we wrote)
            }
			if (is.fail()) {
				// we got the data we needed, so just warn for this error.
			}
			// Now process the data.
			if (!cur_row->empty())
                data.push_back(cur_row);
			else
                delete(cur_row);

			if (data.empty()) {
            }
			else {
				int32 num_rows = data.size(), num_cols = data[0]->size();

				int skip = ((16 / sizeof(float)) - num_cols % (16 / sizeof(float)))
					% (16 / sizeof(float));
				int real_cols = num_cols + skip;
				mat->data_ = g_dataTestUse;
				mat->num_cols_ = num_cols;
				mat->num_rows_ = num_rows;
				mat->stride_ = real_cols;

				for (int32 i = 0; i < num_rows; i++) {
					if (static_cast<int32>(data[i]->size()) != num_cols) {
						goto cleanup;
					}
					for (int32 j = 0; j < num_cols; j++) {
						g_dataTestUse[i*real_cols + j] = (*(data[i]))[j];
					}
					delete data[i];
				}
			}
			return;
		}
		else if (static_cast<char>(i) == '\n' || static_cast<char>(i) == ';') {
			// End of matrix row.
			is.get();
			if (cur_row->size() != 0) {
				data.push_back(cur_row);
				cur_row = new std::vector<float>;
				cur_row->reserve(data.back()->size());
			}
		}
		else if ((i >= '0' && i <= '9') || i == '-') {  // A number...
			float r;
			is >> r;
			if (is.fail()) {
				goto cleanup;
			}
			cur_row->push_back(r);
		}
		else if (isspace(i)) {
			is.get();  // eat the space and do nothing.
		}
		else {  // NaN or inf or error.
			std::string str;
			is >> str;

			if (!strncasecmp(str.c_str(), "inf", 3) ||
				!strncasecmp(str.c_str(), "infinity", 8)) {
				cur_row->push_back(FLT_MAX);
			}
			else if (!strncasecmp(str.c_str(), "nan", 3)) {
				cur_row->push_back(std::numeric_limits<float>::quiet_NaN());
			}
			else {
				goto cleanup;
			}
		}
	}
	// Note, we never leave the while () loop before this
	// line (we return from it.)
cleanup: // We only reach here in case of error in the while loop above.
	delete cur_row;
	for (size_t i = 0; i < data.size(); i++)
		delete data[i];
	// and then go on to "bad" below, where we print error.
	mat->data_ = g_dataTestUse;
	mat->num_cols_ = 46;
	mat->num_rows_ = 248;
	mat->stride_   = 46;
}

void TokenWrite(std::vector<TokenList> active_toks_)
{
	ofstream is_;
	is_.open("E://test.txt", true ? std::ios_base::out | std::ios_base::binary : std::ios_base::out);

	for (int i = 0; i < active_toks_.size(); i++) {
		Token *t = active_toks_[i].toks;
		while (true) {
			if (t->next==NULL) {
				break;
			}
			is_.write(reinterpret_cast<const char *>(&t->tot_cost), sizeof(t->tot_cost));
			is_.write(reinterpret_cast<const char *>(&t->extra_cost), sizeof(t->extra_cost));
			t = t->next;
		}
	}
	is_.close();
}

void TokenListWrite(vector<Token*> token_list)
{
	ofstream is_;
	is_.open("E://testTokenList.txt", std::ios_base::out | std::ios_base::app);

	for (int k = 0; k < token_list.size(); k++) {
		if (token_list[k]==NULL) {
			continue;
		}
		for (ForwardLink *link = token_list[k]->links; link != NULL; link = link->next) {
			char str[100];
			sprintf(str, "%f", token_list[k]->tot_cost);

			is_.write(str, strlen(str));
			is_.write(" ", sizeof(" "));
			sprintf(str, "%f", token_list[k]->extra_cost);
			is_.write(str, strlen(str));
			is_.write(" - ", sizeof(" - "));
			sprintf(str, "%f", link->next_tok->tot_cost);
			is_.write(str, strlen(str));
			is_.write(" ", sizeof(" "));
			sprintf(str, "%f", link->next_tok->extra_cost);
			is_.write(str, strlen(str));
			is_.write("\n", sizeof("\n"));
		}
	}
	is_.close();
}

#endif
