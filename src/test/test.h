#ifndef TEST_USE_
#define TEST_USE_

#include <vector>
#include "../decode/TokenList.h"
#include "../decode/DecodableInterface.h"

using namespace std;

void TestReadMatrix(Matrix_Float* mat);
void TestReadMatrix2(Matrix_Float* mat);
void TokenWrite(vector<TokenList> active_toks_);
void TokenListWrite(vector<Token*> token_list);

#endif
