#ifndef DETERMINEZELATTICEPRUNED_H_
#define DETERMINEZELATTICEPRUNED_H_

#include "../wfst/VectorFstOfLatticeArc.h"
#include "../wfst/VectorFstOfCompactLatticeArc.h"
#include "../decode/LatticeFasterDecoderConfig.h"

bool DeterminizeLatticePhonePrunedWrapper(
	VectorFstOfLatticeArc *ifst,
	double beam,
	VectorFstOfCompactLatticeArc *ofst,
	DeterminizeLatticePhonePrunedOptions opts);

#endif
