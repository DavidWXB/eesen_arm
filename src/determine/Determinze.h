#ifndef DETERMINZE_H_
#define DETERMINZE_H_

#include <cmath>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include "../wfst/VectorFstOfLatticeArc.h"
#include "../decode/LatticeFasterDecoderConfig.h"
#include "../wfst/VectorFstOfCompactLatticeArc.h"

#define kNoStateId -1
enum IsymbolOrFinal { OSF_UNKNOWN = 0, OSF_NO = 1, OSF_YES = 2 };

const float kDelta = 1.0F / 1024.0F;

struct Entry {
	Entry *parent; 	// NULL for empty string.
	int i;			//IntType
	inline bool operator == (Entry &other) const {
		return (parent == other.parent && i == other.i);
	}
	Entry() {
	}
	Entry(Entry &e) : parent(e.parent), i(e.i) {
	}
};

struct Element {
	int state; // use StateId as this is usually InputStateId but in one case OutputStateId.
	Entry* string;
	LatticeWeight weight;
	bool operator != (Element &other) const {
		return (state != other.state || string != other.string ||
			(weight.value1_ != other.weight.value1_ || weight.value2_ != other.weight.value2_));
	}
	// This operator is only intended for the priority_queue in the function
	// EpsilonClosure().
	bool operator > (const Element &other) const {
		return state > other.state;
	}
};

struct TempArc {
	int ilabel;
	Entry* string;  // Look it up in the StringRepository, it's a sequence of Labels.
	int nextstate;  // or kNoState for final weights.
	LatticeWeight weight;
};

struct OutputState {
	vector<Element> minimal_subset;
	vector<TempArc> arcs; //minimal_subset��cost��Сֵ arcs out of the state-- those that have been processed.
	// Note: the final-weight is included here with kNoStateId as the state id.  We
	// always process the final-weight regardless of the beam; when producing the
	// output we may have to ignore some of these.
	double forward_cost; // Represents minimal cost from start-state
	// to this state.  Used in prioritization of tasks, and pruning.
	// Note: we know this minimal cost from when we first create the OutputState;
	// this is because of the priority-queue we use, that ensures that the
	// "best" path into the state will be expanded first.
	OutputState(const vector<Element> &minimal_subset,
		double forward_cost) : minimal_subset(minimal_subset),
		forward_cost(forward_cost) { }
};

struct Task {
	int state; 				// State from which we're processing the transition.
	int label; 				// Label on the transition we're processing out of this state.
	vector<Element> subset; // Weighted subset of states (with strings)-- not normalized.
	double priority_cost; 	// Cost used in deciding priority of tasks.  Note:
	// we assume there is a ConvertToCost() function that converts the semiring to double.
};

struct TaskCompare {
	inline int operator() (const Task *t1, const Task *t2) {
		// view this like operator <, which is the default template parameter
		// to std::priority_queue.
		// returns true if t1 is worse than t2.
		return (t1->priority_cost > t2->priority_cost);
	}
};

struct DeterminizeLatticePrunedOptions {
	float delta; // A small offset used to measure equality of weights.
	int max_mem; // If >0, determinization will fail and return false
	// when the algorithm's (approximate) memory consumption crosses this threshold.
	int max_loop; // If >0, can be used to detect non-determinizable input
	// (a case that wouldn't be caught by max_mem).
	int max_states;
	int max_arcs;
	float retry_cutoff;
	DeterminizeLatticePrunedOptions() : delta(kDelta),
		max_mem(-1),
		max_loop(-1),
		max_states(-1),
		max_arcs(-1),
		retry_cutoff(0.5) {
	}
};

struct SubsetKey {
	size_t operator ()(const vector<Element> * subset) const {  // hashes only the state and string.
		size_t hash = 0, factor = 1;
		for (vector<Element>::const_iterator iter = subset->begin(); iter != subset->end(); ++iter) {
			hash *= factor;
			hash += iter->state + reinterpret_cast<size_t>(iter->string);
			factor *= 23531;  // these numbers are primes.
		}
		return hash;
	}
};

inline bool ApproxEqual(const LatticeWeight &w1,
	const LatticeWeight &w2,
	float delta = kDelta) {
	if (w1.value1_ == w2.value1_ && w1.value2_ == w2.value2_)
		return true;  // handles Zero().
	return (fabs((w1.value1_ + w1.value2_) - (w2.value1_ + w2.value2_)) <= delta);
}

struct SubsetEqual {
	bool operator ()(const vector<Element> * s1, const vector<Element> * s2) const {
		size_t sz = s1->size();
		if (sz != s2->size()) return false;
		vector<Element>::const_iterator iter1 = s1->begin(),
			iter1_end = s1->end(), iter2 = s2->begin();
		for (; iter1 < iter1_end; ++iter1, ++iter2) {
			if (iter1->state != iter2->state ||
				iter1->string != iter2->string ||
				!ApproxEqual(iter1->weight, iter2->weight, delta_)) return false;
		}
		return true;
	}
	float delta_;
	SubsetEqual(float delta) : delta_(delta) {}
	SubsetEqual() : delta_(kDelta) {}
};

struct EntryKey
{ 	// Hash function object.
	inline size_t operator()(Entry *entry) const {
		size_t prime = 49109;
		return static_cast<size_t>(entry->i) + prime * reinterpret_cast<size_t>(entry->parent);
	}
};

struct EntryEqual
{
	inline bool operator()(Entry *e1, Entry *e2) const {
		return (*e1 == *e2);
	}
};

struct LatticeStringRepository
{
	Entry *new_entry_; // We always have a pre-allocated Entry ready to use,
	// to avoid unnecessary news and deletes.
	unordered_set<Entry*, EntryKey, EntryEqual> set_;
};

struct LatticeDeterminizerPruned
{
	vector<OutputState*> output_states_; // All the info about the output states.

	int num_arcs_; 	// keep track of memory usage: number of arcs in output_states_[ ]->arcs
	int num_elems_; // keep track of memory usage: number of elems in output_states_ and
	// the keys of initial_hash_

	VectorFstOfLatticeArc *ifst_;
	std::vector<double> backward_costs_; // This vector stores, for every state in ifst_,
	// the minimal cost to the end-state (i.e. the sum of weights; they are guaranteed to
	// have "take-the-minimum" semantics).  We get the double from the ConvertToCost()
	// function on the lattice weights.

	double beam_;
	double cutoff_; // beam plus total-weight of input (and note, the weight is
	// guaranteed to be "tropical-like" so the sum does represent a min-cost.

	DeterminizeLatticePrunedOptions opts_;
	SubsetKey hasher_;  	// object that computes keys-- has no data members.
	SubsetEqual equal_;  	// object that compares subsets-- only data member is delta_.
	bool determinized_; 	// set to true when user called Determinize(); used to make
	// sure this object is used correctly.
	/*MinimalSubsetHash*/
	unordered_map<const vector<Element>*, int, SubsetKey, SubsetEqual>  minimal_hash_;  // hash from Subset to OutputStateId.  Subset is "minimal
	// representation" (only include final and states and states with
	// nonzero ilabel on arc out of them.  Owns the pointers
	// in its keys.
	/*InitialSubsetHash*/
	unordered_map<const vector<Element>*, Element, SubsetKey, SubsetEqual>initial_hash_;   // hash from Subset to Element, which
	// represents the OutputStateId together
	// with an extra weight and string.  Subset
	// is "initial representation".  The extra
	// weight and string is needed because after
	// we convert to minimal representation and
	// normalize, there may be an extra weight
	// and string.  Owns the pointers
	// in its keys.

	// This priority queue contains "Task"s to be processed; these correspond
	// to transitions out of determinized states.  We process these in priority
	// order according to the best weight of any path passing through these
	// determinized states... it's possible to work this out.
	std::priority_queue<Task*, vector<Task*>, TaskCompare> queue_;

	vector<pair<int, Element> > all_elems_tmp_; // temporary vector used in ProcessTransitions.

	enum IsymbolOrFinal { OSF_UNKNOWN = 0, OSF_NO = 1, OSF_YES = 2 };

	vector<char> isymbol_or_final_; // A kind of cache; it says whether
	// each state is (emitting or final) where emitting means it has at least one
	// non-epsilon output arc.  Only accessed by IsIsymbolOrFinal()

	LatticeStringRepository repository_;  // defines a compact and fast way of
};

bool Determinize(LatticeDeterminizerPruned * det, double *effective_beam);

void Output(LatticeDeterminizerPruned *det, VectorFstOfCompactLatticeArc  *ofst, bool destroy = true);

#endif
