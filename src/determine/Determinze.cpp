#include <queue>
#include <float.h>
#include <algorithm>
#include <functional>
#include <unordered_map>
#include "Determinze.h"
#include "DeterminizeLatticePruned.h"

inline bool PairComparator(const pair<int, Element> &p1, const pair<int, Element> &p2) {
	if (p1.first < p2.first)
        return true;
	else if (p1.first > p2.first)
        return false;
	else {
		return p1.second.state < p2.second.state;
	}
}

Entry *Successor(LatticeStringRepository *repository, Entry *parent, int i) {
	repository->new_entry_->parent = parent;
	repository->new_entry_->i = i;

	std::pair<typename unordered_set<Entry*, EntryKey, EntryEqual>::iterator, bool> pr = repository->set_.insert(repository->new_entry_);
	if (pr.second) {
		// Was successfully inserted (was not there).  We need to
		// replace the element we inserted, which resides on the
		// stack, with one from the heap.
		Entry *ans = repository->new_entry_;
		repository->new_entry_ = new Entry();
		return ans;
	}
	else {
		// Was not inserted because an equivalent Entry already existed.
		return *pr.first;
	}
}

bool IsIsymbolOrFinal(LatticeDeterminizerPruned *det, int state) { // returns true if this state
	// of the input FST either is final or has an osymbol on an arc out of it.
	// Uses the vector isymbol_or_final_ as a cache for this info.
	// KALDI_ASSERT(state >= 0);
	if (det->isymbol_or_final_.size() <= state)
		det->isymbol_or_final_.resize(state + 1, static_cast<char>(OSF_UNKNOWN));
	if (det->isymbol_or_final_[state] == static_cast<char>(OSF_NO))
		return false;
	else if (det->isymbol_or_final_[state] == static_cast<char>(OSF_YES))
		return true;
	// else work it out...
	det->isymbol_or_final_[state] = static_cast<char>(OSF_NO);
	LatticeWeight weight = det->ifst_->states_[state]->final_;
	if (weight.value1_ != FLT_MAX || weight.value2_ != FLT_MAX)
		det->isymbol_or_final_[state] = static_cast<char>(OSF_YES);

	vector<LatticeArc> *vectorLatticeArc = &(det->ifst_->states_[state]->arcs_);
	int arcnum = vectorLatticeArc->size();
	for (int i = 0; i<arcnum; i++) {
		LatticeArc arc = (*vectorLatticeArc)[i];
		if (arc.ilabel != 0 && (arc.weight.value1_ != FLT_MAX || arc.weight.value2_ != FLT_MAX)) {
			det->isymbol_or_final_[state] = static_cast<char>(OSF_YES);
			return true;
		}
	}
	return IsIsymbolOrFinal(det, state); // will only recurse once.
}

void ConvertToMinimal(LatticeDeterminizerPruned *det, vector<Element> *subset) {
	vector<Element>::iterator cur_in = subset->begin(),
		cur_out = subset->begin(), end = subset->end();
	while (cur_in != end) {
		if (IsIsymbolOrFinal(det, cur_in->state)) {  // keep it...
			*cur_out = *cur_in;
			cur_out++;
		}
		cur_in++;
	}
	subset->resize(cur_out - subset->begin());
}

inline size_t Size(Entry *entry) {
	size_t ans = 0;
	while (entry != NULL) {
		ans++;
		entry = entry->parent;
	}
	return ans;
}

//把Entry转换成vector
void ConvertToVector(Entry *entry, vector<int> *out) {
	size_t length = Size(entry);
	out->resize(length);
	vector<int>::iterator iter = out->end();
	while (entry != NULL) {
		--iter;
		*iter = entry->i;
		entry = entry->parent;
	}
}

inline int DeterminzeCompare(const LatticeWeight &a_w, Entry* a_str,
	const LatticeWeight &b_w, Entry* b_str) {
	int weight_comp = LatticeWeightCompare(a_w, b_w);
	if (weight_comp != 0) return weight_comp;
	// now comparing strings.
	if (a_str == b_str) return 0;
	vector<int> a_vec, b_vec;
	ConvertToVector(a_str, &a_vec);
	ConvertToVector(b_str, &b_vec);
	// First compare their lengths.
	int a_len = a_vec.size(), b_len = b_vec.size();
	// use opposite order on the string lengths (c.f. Compare in
	// lattice-weight.h)
	if (a_len > b_len)
		return -1;
	else if (a_len < b_len)
		return 1;
	for (int i = 0; i < a_len; i++) {
		if (a_vec[i] < b_vec[i])
			return -1;
		else if (a_vec[i] > b_vec[i])
			return 1;
	}
	return 0;
}

// Take a subset of Elements that is sorted on state, and
// merge any Elements that have the same state (taking the best
// (weight, string) pair in the semiring).
void MakeSubsetUnique(vector<Element> *subset) {
	typedef vector<Element>::iterator IterType;

	IterType cur_in = subset->begin(), cur_out = cur_in, end = subset->end();
	size_t num_out = 0;
	// Merge elements with same state-id
	while (cur_in != end) {
		// while we have more elements to process.
		// At this point, cur_out points to location of next place we want to put an element,
		// cur_in points to location of next element we want to process.
		if (cur_in != cur_out) *cur_out = *cur_in;
		cur_in++;
		while (cur_in != end && cur_in->state == cur_out->state) {
			if (DeterminzeCompare(cur_in->weight, cur_in->string,
				cur_out->weight, cur_out->string) == 1) {
				// if *cur_in > *cur_out in semiring, then take *cur_in.
				cur_out->string = cur_in->string;
				cur_out->weight = cur_in->weight;
			}
			cur_in++;
		}
		cur_out++;
		num_out++;
	}
	subset->resize(num_out);
}

void ReduceToCommonPrefix(Entry *a, vector<int> *b) {
	size_t a_size = Size(a), b_size = b->size();
	while (a_size> b_size) {
		a = a->parent;
		a_size--;
	}
	if (b_size > a_size)
		b_size = a_size;
	vector<int>::iterator b_begin = b->begin();
	while (a_size != 0) {
		if (a->i != *(b_begin + a_size - 1))
			b_size = a_size - 1;
		a = a->parent;
		a_size--;
	}
	if (b_size != b->size())
		b->resize(b_size);
}

int Compare(const LatticeWeight w1, const LatticeWeight w2)
{
	float f1 = w1.value1_ + w1.value2_;
	float f2 = w2.value1_ + w2.value2_;
	if (f1 < f2)
		return 1;
	else if (f1>f2)
		return -1;
	else if (w1.value1_ < w2.value1_)
		return 1;
	else if (w1.value1_ > w2.value1_)
		return -1;
	else
		return 0;
}

LatticeWeight Divide(const LatticeWeight w1, const LatticeWeight w2)
{
	float a = w1.value1_ - w2.value1_, b = w1.value2_ - w2.value2_;
	LatticeWeight re;
	re.value1_ = FLT_MAX;
	re.value2_ = FLT_MAX;
	if (a != a || b != b || a == -FLT_MAX || b == -FLT_MAX) {
		return re;
	}
	if (a == FLT_MAX || b == FLT_MAX)
		return re; // not a valid number if only one is infinite.
	re.value1_ = a;
	re.value2_ = b;
	return re;
}

Entry *RemovePrefix(LatticeDeterminizerPruned *det, Entry *a, size_t n) {
	if (n == 0)
		return a;

	vector<int> a_vec;
	ConvertToVector(a, &a_vec);

	Entry *ans = NULL;
	for (size_t i = n; i < a_vec.size(); i++)
		ans = Successor(&det->repository_, ans, a_vec[i]);
	return ans;
}

Entry *ConvertFromVector(LatticeDeterminizerPruned *det, const vector<int> &vec) {
	Entry *e = NULL;
	for (size_t i = 0; i < vec.size(); i++)
		e = Successor(&det->repository_, e, vec[i]);
	return e;
}

// This function computes epsilon closure of subset of states by following epsilon links.
// Called by InitialToStateId and Initialize.
// Has no side effects except on the string repository.  The "output_subset" is not
// necessarily normalized (in the sense of there being no common substring), unless
// input_subset was.
//正向遍历，如果是空输入就把Element存入cur_subset里（Element里的weight是） queue只是中间数据
//空ilabel的时候存在两条链相交的情况，相交的位置的weight取最小的weight存在当前节点但是在queue里就是再存一遍  也就是说相交的节点在cur_subset只存一次但是在queue里存了两次
void EpsilonClosure(LatticeDeterminizerPruned *det, vector<Element> *subset) {
	// at input, subset must have only one example of each StateId.  [will still
	// be so at output].  This function follows input-epsilons, and augments the
	// subset accordingly.
	unordered_map<int, Element> cur_subset;
	typedef unordered_map<int, Element>::iterator MapIter;
	{
		MapIter iter = cur_subset.end();
		for (size_t i = 0; i < subset->size(); i++)
		{
			std::pair<const int, Element> pr((*subset)[i].state, (*subset)[i]);
			iter = cur_subset.insert(iter, pr);
			// By providing iterator where we inserted last one, we make insertion more efficient since
			// input subset was already in sorted order.
		}
	}
	// find whether input fst is known to be sorted on input label. 
	bool sorted = true;

	std::priority_queue<Element, vector<Element>, greater<Element> > queue;
	for (vector<Element>::const_iterator iter = subset->begin();
		iter != subset->end();
		++iter) queue.push(*iter);
	bool replaced_elems = false; // relates to an optimization, see below.
	//int counter = 0; // not used
	// (e.g. input with negative-cost epsilon loops); useful in testing.
	while (queue.size() != 0) {
		Element elem = queue.top();
		queue.pop();

		// The next if-statement is a kind of optimization.  It's to prevent us
		// unnecessarily repeating the processing of a state.  "cur_subset" always
		// contains only one Element with a particular state.  The issue is that
		// whenever we modify the Element corresponding to that state in "cur_subset",
		// both the new (optimal) and old (less-optimal) Element will still be in
		// "queue".  The next if-statement stops us from wasting compute by
		// processing the old Element.
		if (replaced_elems && cur_subset[elem.state] != elem)
			continue;

		vector<LatticeArc> *vectorLatticeArc = &(det->ifst_->states_[elem.state]->arcs_);
		int arcnum = vectorLatticeArc->size();
		for (int i = 0; i<arcnum; i++)
		{
			LatticeArc arc = (*vectorLatticeArc)[i];
			if (sorted && arc.ilabel != 0) break;  // Break from the loop: due to sorting there will be no
			// more transitions with epsilons as input labels.
			if (arc.ilabel == 0
				&& (arc.weight.value1_ != FLT_MAX || arc.weight.value2_ != FLT_MAX)) {  // Epsilon transition.
				Element next_elem;
				next_elem.state = arc.nextstate;
				next_elem.weight.value1_ = elem.weight.value1_ + arc.weight.value1_;
				next_elem.weight.value2_ = elem.weight.value2_ + arc.weight.value2_;
				// next_elem.string is not set up yet... create it only
				// when we know we need it (this is an optimization) 

				unordered_map<int, Element>::iterator
					iter = cur_subset.find(next_elem.state);
				if (iter == cur_subset.end()) {
					// was no such StateId: insert and add to queue.
					next_elem.string = (arc.olabel == 0 ? elem.string :
						Successor(&det->repository_, elem.string, arc.olabel));
					cur_subset[next_elem.state] = next_elem;
					queue.push(next_elem);
				}
				else {
					//新的elem的state在cur_subset里已经存在
					// was not inserted because one already there.  In normal
					// determinization we'd add the weights.  Here, we find which one
					// has the better weight, and keep its corresponding string.
					int comp = LatticeWeightCompare(next_elem.weight, iter->second.weight);
					if (comp == 0) { // A tie on weights.  This should be a rare case;
						// we don't optimize for it.
						next_elem.string = (arc.olabel == 0 ? elem.string :
							Successor(&det->repository_, elem.string,
							arc.olabel));
						comp = DeterminzeCompare(next_elem.weight, next_elem.string, iter->second.weight, iter->second.string);
					}
					//新的elem的weight小，需要添加
					if (comp == 1) { // next_elem is better, so use its (weight, string)
						next_elem.string = (arc.olabel == 0 ? elem.string :
							Successor(&det->repository_, elem.string, arc.olabel));
						iter->second.string = next_elem.string;
						iter->second.weight = next_elem.weight;
						queue.push(next_elem);
						replaced_elems = true;
					}
					// else it is the same or worse, so use original one.
				}
			}
		}
	}

	{
		// copy cur_subset to subset.
		// sorted order is automatic.
		subset->clear();
		subset->reserve(cur_subset.size());
		MapIter iter = cur_subset.begin(), end = cur_subset.end();
		for (; iter != end; ++iter) subset->push_back(iter->second);//慢
	}
}

void NormalizeSubset(LatticeDeterminizerPruned *det, vector<Element> *elems,
	LatticeWeight *tot_weight,
	Entry **common_str) {
	if (elems->empty()) {
		// just set common_str, tot_weight
		// to defaults and return...
#ifdef PRINTF_LOG
	
		printf("Did not reach requested beam in determinize-lattice: empty subset");
#endif
		return;
	}
	size_t size = elems->size();
	vector<int> common_prefix;
	ConvertToVector((*elems)[0].string, &common_prefix);
	LatticeWeight weight = (*elems)[0].weight;

	//把elems的相同的前n个string截取放到common_prefix里
	for (size_t i = 1; i < size; i++) {
		//计算weight的最小值
		weight = Compare(weight, (*elems)[i].weight) >= 0 ? weight : (*elems)[i].weight;
		ReduceToCommonPrefix((*elems)[i].string, &common_prefix);
	}
	// we made sure to ignore arcs with zero
	// weights on them, so we shouldn't have zero here.
	size_t prefix_len = common_prefix.size();
	for (size_t i = 0; i < size; i++) {
		(*elems)[i].weight = Divide((*elems)[i].weight, weight);
		//把string里面第prefix_len节点之前的数据都删除
		(*elems)[i].string = RemovePrefix(det, (*elems)[i].string, prefix_len);
	}
	*common_str = ConvertFromVector(det, common_prefix);
	*tot_weight = weight;
}
void ProcessFinal(LatticeDeterminizerPruned *det, int output_state_id) {
	OutputState &state = *(det->output_states_[output_state_id]);
	const vector<Element> &minimal_subset = state.minimal_subset;
	// processes final-weights for this subset.  state.minimal_subset_ may be
	// empty if the graphs is not connected/trimmed, I think, do don't check
	// that it's nonempty.
	Entry* final_string = NULL;  // set it to keep the
	// compiler happy; if it doesn't get set in the loop, we won't use the value anyway.
	LatticeWeight final_weight;
	final_weight.value1_ = FLT_MAX;
	final_weight.value2_ = FLT_MAX;
	bool is_final = false;
	vector<Element>::const_iterator iter = minimal_subset.begin(), end = minimal_subset.end();
	for (; iter != end; ++iter) {
		const Element &elem = *iter;
		LatticeWeight this_final_weight;
		LatticeWeight weightTmp = det->ifst_->states_[elem.state]->final_;
		this_final_weight.value1_ = elem.weight.value1_ + weightTmp.value1_;
		this_final_weight.value2_ = elem.weight.value2_ + weightTmp.value2_;

		Entry* this_final_string = elem.string;
		if ((this_final_weight.value1_ != FLT_MAX || this_final_weight.value2_ != FLT_MAX) &&
			(!is_final || DeterminzeCompare(this_final_weight, this_final_string,
			final_weight, final_string) == 1)) { // the new
			// (weight, string) pair is more in semiring than our current one.
			is_final = true;
			final_weight = this_final_weight;
			final_string = this_final_string;
		}
	}
	if (is_final &&
		final_weight.value1_ + final_weight.value2_ + state.forward_cost <= det->cutoff_) {
		// store final weights in TempArc structure, just like a transition.
		// Note: we only store the final-weight if it's inside the pruning beam, hence
		// the stuff with Compare.
		TempArc temp_arc;
		temp_arc.ilabel = 0;
		temp_arc.nextstate = kNoStateId;  // special marker meaning "final weight".
		temp_arc.string = final_string;
		temp_arc.weight = final_weight;
		state.arcs.push_back(temp_arc);
		det->num_arcs_++;
	}
}

// ProcessTransitions processes emitting transitions (transitions with
// ilabels) out of this subset of states.  It actualy only creates records
// ("Task") that get added to the queue.  The transitions will be processed in
// priority order from Determinize().  This function soes not consider final
// states.  Partitions the emitting transitions up by ilabel (by sorting on
// ilabel), and for each unique ilabel, it creates a Task record that contains
// the information we need to process the transition.
//把output_states_[output_state_id]->minimal_subset里的ilabel相等的elem存到一个task里，不同ilabel的task存到queue_队列里
//task里的state存的是output_states_的索引（并不是fst的state），label是ilabel，subset是elem的队列，priority_cost是前向elem的weight+后向backward_costs_的最优weight
//取elem的nextstate中所有ilabel是非空的，并把ilabel相等的存成一个task
void ProcessTransitions(LatticeDeterminizerPruned *det, int output_state_id) {
	const vector<Element> &minimal_subset = det->output_states_[output_state_id]->minimal_subset;
	// it's possible that minimal_subset could be empty if there are
	// unreachable parts of the graph, so don't check that it's nonempty.
	vector<pair<int, Element> > &all_elems(det->all_elems_tmp_); // use class member
	// to avoid memory allocation/deallocation.
	{
		// Push back into "all_elems", elements corresponding to all
		// non-epsilon-input transitions out of all states in "minimal_subset".
		//把所有非空输入的串成elem保存到all_elems里。minimal_subset里存的都是空输入的elem
		vector<Element>::const_iterator iter = minimal_subset.begin(), end = minimal_subset.end();
		for (; iter != end; ++iter) {
			const Element &elem = *iter;
			vector<LatticeArc> *vectorLatticeArc = &(det->ifst_->states_[elem.state]->arcs_);
			int arcnum = vectorLatticeArc->size();
			for (int i = 0; i<arcnum; i++)
			{
				LatticeArc arc = (*vectorLatticeArc)[i];
				if (arc.ilabel != 0
					&& (arc.weight.value1_ != FLT_MAX || arc.weight.value2_ != FLT_MAX)) {  // Non-epsilon transition -- ignore epsilons here.
					pair<int, Element> this_pr;
					this_pr.first = arc.ilabel;
					Element &next_elem(this_pr.second);
					next_elem.state = arc.nextstate;
					next_elem.weight.value1_ = elem.weight.value1_ + arc.weight.value1_;
					next_elem.weight.value2_ = elem.weight.value2_ + arc.weight.value2_;
					if (arc.olabel == 0) // output epsilon
						next_elem.string = elem.string;
					else
						next_elem.string = Successor(&det->repository_, elem.string, arc.olabel);
					all_elems.push_back(this_pr);
				}
			}
		}
	}
	//PairComparator pc;
	//按ilabel排序
	std::sort(all_elems.begin(), all_elems.end(), PairComparator);
	// now sorted first on input label, then on state.
	typedef vector<pair<int, Element> >::const_iterator PairIter;
	PairIter cur = all_elems.begin(), end = all_elems.end();
	//把ilabel的值都相等的elem存到Task结构的subset里,priority_cost存的是ilabel都相等的elem计算正向elem的cost+反向backward_costs_的cost取最小值
	//backward_costs_存的是反向weight累加和的最小值，索引是elem.state
	while (cur != end) {
		// The old code (non-pruned) called ProcessTransition; here, instead,
		// we'll put the calls into a priority queue.
		Task *task = new Task;
		// Process ranges that share the same input symbol.
		int ilabel = cur->first;
		task->state = output_state_id;
		task->priority_cost = FLT_MAX;
		task->label = ilabel;
		while (cur != end && cur->first == ilabel) {
			task->subset.push_back(cur->second);
			const Element &element = cur->second;
			// Note: we'll later include the term "forward_cost" in the
			// priority_cost.
			task->priority_cost = min(task->priority_cost,
				element.weight.value1_ + element.weight.value2_ +
				det->backward_costs_[element.state]);
			cur++;
		}

		// After the command below, the "priority_cost" is a value comparable to
		// the total-weight of the input FST, like a total-path weight... of
		// course, it will typically be less (in the semiring) than that.
		// note: we represent it just as a double.
		task->priority_cost += det->output_states_[output_state_id]->forward_cost;
		//如果subset大于cutoff_则删除task，cutoff_是代价最小链的cost+beam得到的阈值
		if (task->priority_cost > det->cutoff_) {
			// This task would never get done as it's past the pruning cutoff.
			delete task;
		}
		else {
			//把subset中的state相同的elem选出来，比较weight的大小/*string里面的i的大小（也就是olabel）*/把小的赋值给大的（赋值包括string和weight）
			MakeSubsetUnique(&(task->subset)); // remove duplicate Elements with the same state.
			det->queue_.push(task); // Push the task onto the queue.  The queue keeps it      
			// in prioritized order, so we always process the one with the "best"
			// weight (highest in the semiring).

			{ // this is a check.
#ifdef PRINTF_LOG
				double best_cost = det->backward_costs_[det->ifst_->start_],
					tolerance = 0.01 + 1.0e-04 * abs(best_cost);
				if (task->priority_cost < best_cost - tolerance) {
					printf("Cost below best cost was encountered: %f %f", task->priority_cost, best_cost);
				}
#endif
			}
		}
	}
	all_elems.clear(); // as it's a reference to a class variable; we want it to stay
	// empty.
}


// Takes a minimal, normalized subset, and converts it to an OutputStateId.
// Involves a hash lookup, and possibly adding a new OutputStateId.
// If it creates a new OutputStateId, it creates a new record for it, works
// out its final-weight, and puts stuff on the queue relating to its
// transitions.
int MinimalToStateId(LatticeDeterminizerPruned *de, const vector<Element> &subset,
	const double forward_cost) {
	unordered_map<const vector<Element>*, int, SubsetKey, SubsetEqual>::const_iterator iter
		= de->minimal_hash_.find(&subset);

#ifdef PRINTF_LOG
	if (iter != de->minimal_hash_.end()) { // Found a matching subset.
		int state_id = iter->second;
		const OutputState &state = *(de->output_states_[state_id]);
		// Below is just a check that the algorithm is working...
		if (forward_cost < state.forward_cost - 0.1) {
			printf("New cost is less (check the difference is small) %f %f", forward_cost, state.forward_cost);
		}
	}
#endif

	int state_id = static_cast<int>(de->output_states_.size());
	OutputState *new_state = new OutputState(subset, forward_cost);
	de->minimal_hash_[&(new_state->minimal_subset)] = state_id;
	de->output_states_.push_back(new_state);
	de->num_elems_ += subset.size();
	// Note: in the previous algorithm, we pushed the new state-id onto the queue
	// at this point.  Here, the queue happens elsewhere, and we directly process
	// the state (which result in stuff getting added to the queue).
	ProcessFinal(de, state_id); // will work out the final-prob.
	//取elem的nextstate中所有ilabel是非空的，并把ilabel相等的存成一个task
	ProcessTransitions(de, state_id); // will process transitions and add stuff to the queue.
	return state_id;
}

int InitialToStateId(LatticeDeterminizerPruned *det, vector<Element> &subset_in,
	double forward_cost,
	LatticeWeight *remaining_weight,
	Entry **common_prefix) {
	unordered_map<const vector<Element>*, Element, SubsetKey, SubsetEqual>::const_iterator iter
		= det->initial_hash_.find(&subset_in);
	if (iter != det->initial_hash_.end()) { // Found a matching subset.
		const Element &elem = iter->second;
		*remaining_weight = elem.weight;
		*common_prefix = elem.string;
#ifdef PRINTF_LOG
		if (elem.weight.value1_ == FLT_MAX || elem.weight.value2_ == FLT_MAX)
			printf("Zero weight!");
#endif
		return elem.state;
	}
	// else no matching subset-- have to work it out.
	vector<Element> subset(subset_in);
	// Follow through epsilons.  Will add no duplicate states.  note: after
	// EpsilonClosure, it is the same as "canonical" subset, except not
	// normalized (actually we never compute the normalized canonical subset,
	// only the normalized minimal one).
	EpsilonClosure(det, &subset); // follow epsilons.
	ConvertToMinimal(det, &subset); //只保留arc的weight非MAX和final的states  remove all but emitting and final states.

	Element elem; // will be used to store remaining weight and string, and
	// OutputStateId, in initial_hash_;    
	NormalizeSubset(det, &subset, &elem.weight, &elem.string); // normalize subset; put
	// common string and weight in "elem".  The subset is now a minimal,
	// normalized subset.

	forward_cost += (double)elem.weight.value1_ + (double)elem.weight.value2_;
	//把subset加入到output_states_中，然后把subset的elem转换成task存到queue_里
	int ans = MinimalToStateId(det, subset, forward_cost);
	*remaining_weight = elem.weight;
	*common_prefix = elem.string;
#ifdef PRINTF_LOG
	if (elem.weight.value1_ == FLT_MAX || elem.weight.value2_ == FLT_MAX)
		printf("Zero weight!");
	//KALDI_WARN << "Zero weight!";
#endif
	// Before returning "ans", add the initial subset to the hash,
	// so that we can bypass the epsilon-closure etc., next time
	// we process the same initial subset.
	vector<Element> *initial_subset_ptr = new vector<Element>(subset_in);
	elem.state = ans;
	det->initial_hash_[initial_subset_ptr] = elem;
	det->num_elems_ += initial_subset_ptr->size(); // keep track of memory usage.
	return ans;
}

Entry *Concatenate(LatticeDeterminizerPruned *de, Entry *a, Entry *b) {
	if (a == NULL) return b;
	else if (b == NULL) return a;
	vector<int> v;
	ConvertToVector(b, &v);
	Entry *ans = a;
	for (size_t i = 0; i < v.size(); i++)
		ans = Successor(&de->repository_, ans, v[i]);
	return ans;
}

void ProcessTransition(LatticeDeterminizerPruned *det, int ostate_id, int ilabel, vector<Element> *subset) {

	double forward_cost = det->output_states_[ostate_id]->forward_cost;
	Entry *common_str;//当前帧截取的公共string
	LatticeWeight tot_weight;//当前帧的最小weight
	//把subset相同的前n个string截取存到common_str，取subset里elem的最小weight存到tot_weight
	NormalizeSubset(det, subset, &tot_weight, &common_str);
	forward_cost += (double)tot_weight.value1_ + (double)tot_weight.value2_;

	int nextstate;
	{
		LatticeWeight next_tot_weight;//下一帧的最小weight
		Entry *next_common_str;//下一帧截取的公共string
		nextstate = InitialToStateId(det, *subset,
			forward_cost,
			&next_tot_weight,
			&next_common_str);
		common_str = Concatenate(det, common_str, next_common_str);

		tot_weight.value1_ += next_tot_weight.value1_;
		tot_weight.value2_ += next_tot_weight.value2_;
	}

	// Now add an arc to the next state (would have been created if necessary by
	// InitialToStateId).
	TempArc temp_arc;
	temp_arc.ilabel = ilabel;
	temp_arc.nextstate = nextstate;
	temp_arc.string = common_str;
	temp_arc.weight = tot_weight;
	det->output_states_[ostate_id]->arcs.push_back(temp_arc);  // record the arc.
	det->num_arcs_++;
}

void FreeOutputStates(LatticeDeterminizerPruned *det) {
	for (size_t i = 0; i < det->output_states_.size(); i++)
		delete det->output_states_[i];
	vector<OutputState*> temp;
	temp.swap(det->output_states_);
}

void Destroy(LatticeDeterminizerPruned *det) {
	for (unordered_set<Entry*, EntryKey, EntryEqual>::iterator iter = det->repository_.set_.begin();
		iter != det->repository_.set_.end();
		++iter)
		delete *iter;
	unordered_set<Entry*, EntryKey, EntryEqual> tmp;
	tmp.swap(det->repository_.set_);
	if (det->repository_.new_entry_) {
		delete det->repository_.new_entry_;
		det->repository_.new_entry_ = NULL;
	}
}

void FreeMostMemory(LatticeDeterminizerPruned *det) {
	if (det->ifst_) {
		delete det->ifst_;
		det->ifst_ = NULL;
	}
	{ unordered_map<const vector<Element>*, int,
		SubsetKey, SubsetEqual> tmp; tmp.swap(det->minimal_hash_); }

	for (size_t i = 0; i <det->output_states_.size(); i++) {
		vector<Element> empty_subset;
		empty_subset.swap(det->output_states_[i]->minimal_subset);
	}

	for (unordered_map<const vector<Element>*, Element,
		SubsetKey, SubsetEqual>::iterator iter = det->initial_hash_.begin();
		iter != det->initial_hash_.end(); ++iter)
		delete iter->first;
	{ unordered_map<const vector<Element>*, Element,
		SubsetKey, SubsetEqual> tmp; tmp.swap(det->initial_hash_); }
	for (size_t i = 0; i < det->output_states_.size(); i++) {
		vector<Element> tmp;
		tmp.swap(det->output_states_[i]->minimal_subset);
	}
	{ vector<char> tmp;  tmp.swap(det->isymbol_or_final_); }
	{ 	// Free up the queue.  I'm not sure how to make sure all
		// the memory is really freed (no swap() function)... doesn't really
		// matter much though.
		while (!det->queue_.empty()) {
			Task *t = det->queue_.top();
			delete t;
			det->queue_.pop();
		}
	}
	{ vector<pair<int, Element> > tmp; tmp.swap(det->all_elems_tmp_); }
}

void Output(LatticeDeterminizerPruned *det, VectorFstOfCompactLatticeArc  *ofst, bool destroy) {
	int nStates = static_cast<int>(det->output_states_.size());
	if (destroy)
		FreeMostMemory(det);

	ofst->states_.clear();
	ofst->start_ = -1;
	if (nStates == 0) {
		return;
	}

	for (int s = 0; s < nStates; s++) {
		VectorStateOfCompactLatticeArc *vt = new VectorStateOfCompactLatticeArc();
		vt->final_.weight_.value1_ = FLT_MAX;
		vt->final_.weight_.value2_ = FLT_MAX;
		vt->niepsilons_ = 0;
		vt->noepsilons_ = 0;
		// Arcs represenation
		ofst->states_.push_back(vt);
	}
	ofst->start_ = 0;
	// now process transitions.
	for (int this_state_id = 0; this_state_id < nStates; this_state_id++) {
		OutputState &this_state = *(det->output_states_[this_state_id]);
		vector<TempArc> &this_vec(this_state.arcs);
		vector<TempArc>::const_iterator iter = this_vec.begin(), end = this_vec.end();

		for (; iter != end; ++iter) {
			const TempArc &temp_arc(*iter);
			CompactLatticeArc new_arc;
			vector<int> olabel_seq;
			ConvertToVector(temp_arc.string, &olabel_seq);
			CompactLatticeWeight weight;
			weight.weight_ = temp_arc.weight;
			weight.string_ = olabel_seq;
			if (temp_arc.nextstate == -1) {  // is really final weight.
				ofst->states_[this_state_id]->final_ = weight;
			}
			else {  // is really an arc.
				new_arc.nextstate = temp_arc.nextstate;
				new_arc.ilabel = temp_arc.ilabel;
				new_arc.olabel = temp_arc.ilabel;  	// acceptor.  input == output.
				new_arc.weight = weight;  			// includes string and weight.
				if (new_arc.ilabel == 0)
					++ofst->states_[this_state_id]->niepsilons_;
				if (new_arc.olabel == 0)
					++ofst->states_[this_state_id]->noepsilons_;
				ofst->states_[this_state_id]->arcs_.push_back(new_arc);
			}
		}
		// Free up memory.  Do this inside the loop as ofst is also allocating memory,
		// and we want to reduce the maximum amount ever allocated.
		if (destroy) {
			vector<TempArc> temp;
			temp.swap(this_vec);
		}
	}
	if (destroy) {
		FreeOutputStates(det);
		Destroy(det);
	}
}

//算法使用的是反向传递weight，迭代的取最小值，backward_costs_存的是反向weight累加和的最小值，传递到首节点时就是最优路径的权重和，然后加上beam_生成cutoff_的阈值
void ComputeBackwardWeight(LatticeDeterminizerPruned *det) {
	// Only handle the toplogically sorted case.
	det->backward_costs_.resize(det->ifst_->states_.size());
	for (int s = det->ifst_->states_.size() - 1; s >= 0; s--) {
		double &cost = det->backward_costs_[s];
		cost = det->ifst_->states_[s]->final_.value1_ + det->ifst_->states_[s]->final_.value2_;
		vector<LatticeArc> *vectorLatticeArc = &(det->ifst_->states_[s]->arcs_);
		int arcnum = vectorLatticeArc->size();
		for (int i = 0; i<arcnum; i++) {
			LatticeArc arc = (*vectorLatticeArc)[i];
			cost = min(cost,
				arc.weight.value1_ + arc.weight.value2_ + det->backward_costs_[arc.nextstate]);
		}
	}

	if (det->ifst_->start_ == -1) return; // we'll be returning
	// an empty FST.

	double best_cost = det->backward_costs_[det->ifst_->start_];
#ifdef PRINTF_LOG
	if (best_cost==FLT_MAX)
		printf("Total weight of input lattice is zero.");
#endif
	det->cutoff_ = best_cost + det->beam_;
}

void InitializeDeterminization(LatticeDeterminizerPruned *det) {
	// We insist that the input lattice be topologically sorted.  This is not a
	// fundamental limitation of the algorithm (which in principle should be
	// applicable to even cyclic FSTs), but it helps us more efficiently
	// compute the backward_costs_ array.  There may be some other reason we
	// require this, that escapes me at the moment.
	//算法使用的是反向传递weight，迭代的取最小值，传递到首节点时就是最优路径的权重和，然后加上beam_生成cutoff_的阈值
	ComputeBackwardWeight(det);
	int num_states = det->ifst_->states_.size();
	det->minimal_hash_.rehash(num_states / 2 + 3);
	det->initial_hash_.rehash(num_states / 2 + 3);

	int start_id = det->ifst_->start_;
	if (start_id != -1) {
		/* Create determinized-state corresponding to the start state....
		Unlike all the other states, we don't "normalize" the representation
		of this determinized-state before we put it into minimal_hash_.  This is actually
		what we want, as otherwise we'd have problems dealing with any extra weight
		and string and might have to create a "super-initial" state which would make
		the output nondeterministic.  Normalization is only needed to make the
		determinized output more minimal anyway, it's not needed for correctness.
		Note, we don't put anything in the initial_hash_.  The initial_hash_ is only
		a lookaside buffer anyway, so this isn't a problem-- it will get populated
		later if it needs to be.
		*/
		vector<Element> subset(1);
		subset[0].state = start_id;
		subset[0].weight.value1_ = 0.0;
		subset[0].weight.value2_ = 0.0;
		subset[0].string = NULL; // Id of empty sequence.
		//把空输入加入到cur_subset里，如果是两条链相交取weight最小值存到cur_subset里
		EpsilonClosure(det, &subset); // follow through epsilon-input links
		ConvertToMinimal(det, &subset); // remove all but final states and
		// states with input-labels on arcs out of them.
		// Weight::One() is the "forward-weight" of this determinized state...
		// i.e. the minimal cost from the start of the determinized FST to this
		// state [One() because it's the start state].
		OutputState *initial_state = new OutputState(subset, 0);

		det->output_states_.push_back(initial_state);
		det->num_elems_ += subset.size();
		int initial_state_id = 0;
		det->minimal_hash_[&(initial_state->minimal_subset)] = initial_state_id;
		//把output_states_[initial_state_id]里的elem.weight+elem的state的final_的最小值的elem取出来weight和string存到output_states_[initial_state_id]的arcs里
		ProcessFinal(det, initial_state_id);
		//把output_states_[output_state_id]->minimal_subset里的ilabel相等的elem存到一个task里，不同ilabel的task存到queue_队列里
		//task里的state存的是output_states_的索引（并不是fst的state），label是ilabel，subset是elem的队列，priority_cost是前向elem的weight+后向backward_costs_的最优weight
		ProcessTransitions(det, initial_state_id); // this will add tasks to
		// the queue, which we'll start processing in Determinize().
	}
}

void AddStrings(const vector<Element> &vec,
	vector<Entry*> *needed_strings) {
	for (std::vector<Element>::const_iterator iter = vec.begin();
		iter != vec.end(); ++iter)
		needed_strings->push_back(iter->string);
}

typedef unordered_set<Entry*, EntryKey, EntryEqual> SetType;
typedef unordered_map<const vector<Element>*, Element,
	SubsetKey, SubsetEqual> InitialSubsetHash;

void RebuildHelper(Entry *to_add, SetType *tmp_set) {
	while (true) {
		if (to_add == NULL) return;
		SetType::iterator iter = tmp_set->find(to_add);
		if (iter == tmp_set->end()) { // not in tmp_set.
			tmp_set->insert(to_add);
			to_add = to_add->parent; // and loop.
		}
		else {
			return;
		}
	}
}
// Rebuild will rebuild this object, guaranteeing only
// to preserve the Entry values that are in the vector pointed
// to (this list does not have to be unique).  The point of
// this is to save memory.
void Rebuild(LatticeStringRepository * repository,  std::vector<Entry*> &to_keep) {
	SetType tmp_set;
	for (std::vector<Entry*>::const_iterator
		iter  = to_keep.begin();
		iter != to_keep.end(); ++iter)
		RebuildHelper(*iter, &tmp_set);

	// Now delete all elems not in tmp_set.
	for (SetType::iterator iter = repository->set_.begin();
		iter != repository->set_.end(); ++iter) {
		if (tmp_set.count(*iter) == 0)
			delete (*iter); // delete the Entry; not needed.
	}
	repository->set_.swap(tmp_set);
}

void RebuildRepository(LatticeDeterminizerPruned * det) { // rebuild the string repository,    
	// freeing stuff we don't need.. we call this when memory usage
	// passes a supplied threshold.  We need to accumulate all the
	// strings we need the repository to "remember", then tell it
	// to clean the repository.
	std::vector<Entry *> needed_strings;
	for (size_t i = 0; i < det->output_states_.size(); i++) {
		AddStrings(det->output_states_[i]->minimal_subset, &needed_strings);
		for (size_t j = 0; j < det->output_states_[i]->arcs.size(); j++)
			needed_strings.push_back(det->output_states_[i]->arcs[j].string);
	}

	{ // the queue doesn't allow us access to the underlying vector,
		// so we have to resort to a temporary collection.
		std::vector<Task*> tasks;
		while (!det->queue_.empty()) {
			Task *task = det->queue_.top();
			det->queue_.pop();
			tasks.push_back(task);
			AddStrings(task->subset, &needed_strings);
		}
		for (size_t i = 0; i < tasks.size(); i++)
			det->queue_.push(tasks[i]);
	}

	// the following loop covers strings present in initial_hash_.
	for (InitialSubsetHash::const_iterator
		iter = det->initial_hash_.begin();
		iter != det->initial_hash_.end(); ++iter) {
		const vector<Element> &vec = *(iter->first);
		Element elem = iter->second;
		AddStrings(vec, &needed_strings);
		needed_strings.push_back(elem.string);
	}
	std::sort(needed_strings.begin(), needed_strings.end());
	needed_strings.erase(std::unique(needed_strings.begin(),
		needed_strings.end()),
		needed_strings.end()); // uniq the strings.
	//KALDI_LOG << "Rebuilding repository.";

	Rebuild(&det->repository_, needed_strings);
}

bool CheckMemoryUsage(LatticeDeterminizerPruned * det) {
	eesen::int32 repo_size = det->repository_.set_.size() * sizeof(Entry) * 2,
		arcs_size = det->num_arcs_ * sizeof(TempArc),
		elems_size = det->num_elems_ * sizeof(Element),
		total_size = repo_size + arcs_size + elems_size;
	if (det->opts_.max_mem > 0 && total_size > det->opts_.max_mem) { // We passed the memory threshold.
		// This is usually due to the repository getting large, so we
		// clean this out.
		RebuildRepository(det);
		eesen::int32 new_repo_size = det->repository_.set_.size() * sizeof(Entry) * 2,
			new_total_size = new_repo_size + arcs_size + elems_size;

		if (new_total_size > static_cast<eesen::int32>(det->opts_.max_mem * 0.8)) {
			// Rebuilding didn't help enough-- we need a margin to stop
			// having to rebuild too often.  We'll just return to the user at
			// this point, with a partial lattice that's pruned tighter than
			// the specified beam.  Here we figure out what the effective
			// beam was.
			double effective_beam = det->beam_;	// not used
			if (!det->queue_.empty()) { // Note: queue should probably not be empty; we're
				// just being paranoid here.
				Task *task = det->queue_.top();
				double total_weight = det->backward_costs_[det->ifst_->start_]; // best weight of FST.
				effective_beam = task->priority_cost - total_weight;
			}

#ifdef PRINTF_LOG
			else
				printf("Did not reach requested beam in determinize-lattice: size exceeds maximum %d", det->opts_.max_mem);
#endif
			return false;
		}
	}
	return true;
}

bool Determinize(LatticeDeterminizerPruned * det, double *effective_beam) {
	// This determinizes the input fst but leaves it in the "special format"
	// in "output_arcs_".  Must be called after Initialize().  To get the
	// output, call one of the Output routines.

	InitializeDeterminization(det); // some start-up tasks.
	while (!det->queue_.empty()) {
		Task *task = det->queue_.top();
		// Note: the queue contains only tasks that are "within the beam".
		// We also have to check whether we have reached one of the user-specified
		// maximums, of estimated memory, arcs, or states.  The condition for
		// ending is:
		// num-states is more than user specified, OR
		// num-arcs is more than user specified, OR
		// memory passed a user-specified threshold and cleanup failed
		//  to get it below that threshold.
		size_t num_states = det->output_states_.size();
		if ((det->opts_.max_states > 0 && num_states > det->opts_.max_states) ||
			(det->opts_.max_arcs > 0 && det->num_arcs_ > det->opts_.max_arcs) ||
			(num_states % 10 == 0 && !CheckMemoryUsage(det))) { // note: at some point
			// it was num_states % 100, not num_states % 10, but I encountered an example
			// where memory was exhausted before we reached state #100.
			break;
			// we terminate the determinization here-- whatever we already expanded is
			// what we'll return...  because we expanded stuff in order of total
			// (forward-backward) weight, the stuff we returned first is the most
			// important.
		}
		det->queue_.pop();
		ProcessTransition(det, task->state, task->label, &(task->subset));
		delete task;
	}
	det->determinized_ = true;
	if (effective_beam != NULL) {
		if (det->queue_.empty()) *effective_beam = det->beam_;
		else
			*effective_beam = det->queue_.top()->priority_cost -
			det->backward_costs_[det->ifst_->start_];
	}
	return (det->queue_.empty()); // return success if queue was empty, i.e. we processed
	// all tasks and did not break out of the loop early due to reaching a memory,
	// arc or state limit.
}
