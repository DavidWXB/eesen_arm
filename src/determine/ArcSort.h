#ifndef ARCSORT_H_
#define ARCSORT_H_

#include "../wfst/VectorFstOfLatticeArc.h"

void ArcSort(VectorFstOfLatticeArc *fst);

#endif
