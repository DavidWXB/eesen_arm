#include <vector>
#include <algorithm>
#include "Invert.h"
#include "../wfst/VectorFstOfLatticeArc.h"
#include "../wfst/VectorStateOflatticeArc.h"
#include "../wfst/VectorFstOfCompactLatticeArc.h"

using std::vector;

void ArcMap(VectorFstOfLatticeArc *fst) {
	for (uint s = 0; s < fst->states_.size(); ++s) {
		int jend = fst->states_[s]->arcs_.size();
		for (int j = 0; j < jend; j++) {
			LatticeArc arco = fst->states_[s]->arcs_[j];
			LatticeArc arc;
			arc.ilabel = arco.olabel;
			arc.olabel = arco.ilabel;
			arc.weight = arco.weight;
			arc.nextstate = arco.nextstate;

			if (arco.ilabel == 0)
				--fst->states_[s]->niepsilons_;
			if (arco.olabel == 0)
				--fst->states_[s]->noepsilons_;
			if (arc.ilabel == 0)
				++fst->states_[s]->niepsilons_;
			if (arc.olabel == 0)
				++fst->states_[s]->noepsilons_;
			fst->states_[s]->arcs_[j] = arc;
		}
	}
}

void Invert(VectorFstOfLatticeArc *fst) {
	for (uint s = 0; s < fst->states_.size(); ++s) {
		int jend = fst->states_[s]->arcs_.size();
		for (int j = 0; j < jend; j++) {
			LatticeArc arco = fst->states_[s]->arcs_[j];
			LatticeArc arc;
			arc.ilabel = arco.olabel;
			arc.olabel = arco.ilabel;
			arc.weight = arco.weight;
			arc.nextstate = arco.nextstate;

			if (arco.ilabel == 0)
				--fst->states_[s]->niepsilons_;
			if (arco.olabel == 0)
				--fst->states_[s]->noepsilons_;
			if (arc.ilabel == 0)
				++fst->states_[s]->niepsilons_;
			if (arc.olabel == 0)
				++fst->states_[s]->noepsilons_;
			fst->states_[s]->arcs_[j] = arc;
		}
	}
}
