#ifndef INVERT_H_
#define INVERT_H_

#include <vector>
#include <algorithm>
#include "../wfst/VectorFstOfLatticeArc.h"
#include "../wfst/VectorStateOflatticeArc.h"
#include "../wfst/VectorFstOfCompactLatticeArc.h"

void Invert(VectorFstOfLatticeArc *fst);

#endif
