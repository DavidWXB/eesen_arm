#include <float.h>
#include "Invert.h"
#include "ArcSort.h"
#include "Determinze.h"
#include "DeterminizeLatticePruned.h"
#include "../wfst/VectorFstOfLatticeArc.h"
#include "../wfst/VectorFstOfCompactLatticeArc.h"
#include "../getpath/SccVisitorLatticeArc.h"
#include "../getpath/SccVisitorCompactLatticeArc.h"

void LatticeDeterminizerPrunedInit(LatticeDeterminizerPruned *det,const VectorFstOfLatticeArc *ifst, double beam, DeterminizeLatticePrunedOptions opts)
{
	det->num_arcs_ = 0;
	det->num_elems_ = 0;
	det->ifst_ = new VectorFstOfLatticeArc();
	det->ifst_->start_ = ifst->start_;
	det->ifst_->properties_ = ifst->properties_;
	det->ifst_->states_.assign(ifst->states_.begin(), ifst->states_.end());
	det->beam_ = beam;
	det->opts_ = opts;
	det->equal_ = det->opts_.delta;
	det->determinized_ = false;
	det->repository_.new_entry_ = new Entry();
}

bool PruneLattice(float beam, VectorFstOfLatticeArc *lat) {
	// We assume states before "start" are not reachable, since
	// the lattice is topologically sorted.
	eesen::int32 start = lat->start_;
	eesen::int32 num_states = lat->states_.size();
	if (num_states == 0) return false;
	std::vector<double> forward_cost(num_states,
		std::numeric_limits<double>::infinity()); // viterbi forward.
	forward_cost[start] = 0.0; // lattice can't have cycles so couldn't be
	// less than this.
	double best_final_cost = FLT_MAX;
	// Update the forward probs.
	// Thanks to Jing Zheng for finding a bug here.
	for (eesen::int32 state = 0; state < num_states; state++) {
		double this_forward_cost = forward_cost[state];

		vector<LatticeArc> *vectorLatticeArc = &(lat->states_[state]->arcs_);
		int arcnum = vectorLatticeArc->size();
		for (int i = 0; i<arcnum; i++)
		{
			LatticeArc arc = (*vectorLatticeArc)[i];
			int nextstate = arc.nextstate;
			double next_forward_cost = this_forward_cost +
				arc.weight.value1_ + arc.weight.value2_;
			if (forward_cost[nextstate] > next_forward_cost)
				forward_cost[nextstate] = next_forward_cost;
		}
		LatticeWeight final_weight = lat->states_[state]->final_;
		double this_final_cost = this_forward_cost +
			final_weight.value1_ + final_weight.value2_;
		if (this_final_cost < best_final_cost)
			best_final_cost = this_final_cost;
	}

	VectorStateOfLatticeArc *vt = new VectorStateOfLatticeArc();
	vt->final_.value1_ = FLT_MAX;
	vt->final_.value2_ = FLT_MAX;
	vt->niepsilons_ = 0;
	vt->noepsilons_ = 0;

	// Arcs represenation
	lat->states_.push_back(vt);
	eesen::int32 bad_state = lat->states_.size() - 1;
	double cutoff = best_final_cost + beam;

	// Go backwards updating the backward probs (which share memory with the
	// forward probs), and pruning arcs and deleting final-probs.  We prune arcs
	// by making them point to the non-final state "bad_state".  We'll then use
	// Trim() to remove unnecessary arcs and states.  [this is just easier than
	// doing it ourselves.]
	std::vector<double> &backward_cost(forward_cost);
	for (eesen::int32 state = num_states - 1; state >= 0; state--) {
		double this_forward_cost = forward_cost[state];
		LatticeWeight final_weight = lat->states_[state]->final_;
		double this_backward_cost = final_weight.value1_ + final_weight.value2_;
		if (this_backward_cost + this_forward_cost > cutoff
			&& this_backward_cost != FLT_MAX)
		{
			lat->states_[state]->final_.value1_ = FLT_MAX;
			lat->states_[state]->final_.value2_ = FLT_MAX;
		}

		VectorStateOfLatticeArc *vectorState = lat->states_[state];
		vector<LatticeArc> *vectorLatticeArc = &(vectorState->arcs_);
		int arcnum = vectorLatticeArc->size();
		for (int i = 0; i<arcnum; i++)
		{
			LatticeArc arc = (*vectorLatticeArc)[i];

			int nextstate = arc.nextstate;
			double arc_cost = arc.weight.value1_ + arc.weight.value2_,
				arc_backward_cost = arc_cost + backward_cost[nextstate],
				this_fb_cost = this_forward_cost + arc_backward_cost;
			if (arc_backward_cost < this_backward_cost)
				this_backward_cost = arc_backward_cost;
			if (this_fb_cost > cutoff) { // Prune the arc.
				arc.nextstate = bad_state;

				//AddArc
				if (arc.ilabel == 0)
				{
					++(vectorState->niepsilons_);
				}
				if (arc.olabel == 0)
				{
					++(vectorState->noepsilons_);
				}
				vectorState->arcs_.push_back(arc);

			}
		}
		backward_cost[state] = this_backward_cost;
	}
	ConnectForLattice(lat);
	return (lat->states_.size() > 0);
}

bool DeterminizeLatticePruned(
	const VectorFstOfLatticeArc&ifst,
	double beam,
	VectorFstOfCompactLatticeArc*ofst,
	DeterminizeLatticePrunedOptions opts) {
	eesen::int32 max_num_iters = 10;  // avoid the potential for infinite loops if retrying.
	VectorFstOfLatticeArc temp_fst;

	for (eesen::int32 iter = 0; iter < max_num_iters; iter++) {
		LatticeDeterminizerPruned det;
		LatticeDeterminizerPrunedInit(&det, iter == 0 ? &ifst : &temp_fst,beam,opts); 
		double effective_beam;
		bool ans = Determinize(&det, &effective_beam);
		// if it returns false it will typically still produce reasonable output,
		// just with a narrower beam than "beam".  If the user specifies an infinite
		// beam we don't do this beam-narrowing.
		if (effective_beam >= beam * opts.retry_cutoff ||
			beam == FLT_MAX || iter + 1 == max_num_iters) {
			Output(&det,ofst);
			return ans;
		}
		else {
			// The code below to set "beam" is a heuristic.
			// If effective_beam is very small, we want to reduce by a lot.
			// But never change the beam by more than a factor of two.
			if (effective_beam < 0.0) effective_beam = 0.0;
			double new_beam = beam * sqrt(effective_beam / beam);
			if (new_beam < 0.5 * beam) new_beam = 0.5 * beam;
			beam = new_beam;
			if (iter == 0) temp_fst = ifst;
			PruneLattice(beam, &temp_fst);
		}
	}
	return false;
}

bool DeterminizeLatticePhonePruned(
	VectorFstOfLatticeArc *ifst,
	double beam,
	VectorFstOfCompactLatticeArc *ofst,
	DeterminizeLatticePhonePrunedOptions opts) {
	// Returning status.
	bool ans = true;

	// Determinization options.
	DeterminizeLatticePrunedOptions det_opts;
	det_opts.delta = opts.delta;
	det_opts.max_mem = opts.max_mem;

	// If --word-determinize is true, do the determinization on word lattices.
	if (opts.word_determinize) {
		ans = DeterminizeLatticePruned(
			*ifst, beam, ofst, det_opts) && ans;
	}

	return ans;
}

bool DeterminizeLatticePhonePrunedWrapper(
	VectorFstOfLatticeArc *ifst,
	double beam,
	VectorFstOfCompactLatticeArc *ofst,
	DeterminizeLatticePhonePrunedOptions opts)
{
	bool ans = true;
	Invert(ifst);
	ArcSort(ifst);
	ans = DeterminizeLatticePhonePruned(ifst, beam, ofst, opts);
	ConnectForCompactLattice(ofst);

	return ans;
}
