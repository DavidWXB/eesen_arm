#include "../wfst/VectorStateOflatticeArc.h"
#include "../wfst/VectorFstOfCompactLatticeArc.h"
#include "../wfst/VectorFstOfLatticeArc.h"
#include "DeterminizeLatticePruned.h"
#include <vector>
#include <algorithm>
#include <unordered_map>
using std::vector;

bool comp_(const LatticeArc a, const LatticeArc b) {
	return a.ilabel < b.ilabel;
}

void ArcSort(VectorFstOfLatticeArc *fst) {
	for (uint s = 0; s < fst->states_.size(); ++s) {
		std::sort(fst->states_[s]->arcs_.begin(), fst->states_[s]->arcs_.end(), comp_);
	}
}

