#ifndef SCC_VISITOR_COMPACT_H
#define SCC_VISITOR_COMPACT_H

#include <list>
#include <vector>
#include "../wfst/VectorFstOfLatticeArc.h"
#include "../wfst/VectorFstOfCompactLatticeArc.h"

using namespace std;

struct TopOrderVisitor
{
	vector<int> *order_;
	bool *acyclic_;
	vector<int> *finish_;  // states in finishing-time order
};

struct SccVisitorCompactLatticeArc
{
	vector<int> *scc_;        	// State's scc number
	vector<bool> *access_;      // State's accessibility
	vector<bool> *coaccess_;    // State's coaccessibility
	eesen::uint64 *props_;
	const VectorFstOfCompactLatticeArc *fst_;
	int start_;
	int nstates_;             	// State count
	int nscc_;                	// SCC count
	bool coaccess_internal_;
	vector<int> *dfnumber_;   	// state discovery times
	vector<int> *lowlink_;    	// lowlink[s] == dfnumber[s] => SCC root
	vector<bool> *onstack_;     // is a state on the SCC stack
	vector<int> *scc_stack_;  	// SCC stack (w/ random access)
};

struct DfsStateLatticeArc
{
	int state_id;       // Fst state ...
	int i;
};

struct LinkOfStateLatticeArc {
	char buf[sizeof(DfsStateLatticeArc)];
	LinkOfStateLatticeArc *next;
};

struct MemoryArenaOfStateLatticeArc
{
	size_t block_size_;     // default block size in bytes
	size_t block_pos_;      // current position in block in bytes
	list<char *> blocks_;   // list of allocated blocks
};

struct MemoryPoolOfDfsStateLatticeArc
{
	MemoryArenaOfStateLatticeArc mem_arena_;
	LinkOfStateLatticeArc *free_list_;
};

void DfsStateLatticeArcDestroy(DfsStateLatticeArc *dfsStateLatticeArc, MemoryPoolOfDfsStateLatticeArc *memoryPool);
void SccVisitorCompactLatticeArcInitVisit(VectorFstOfLatticeArc *fst);
void SccVisitorLatticeArcFinishState();
void SccVisitorLatticeArcInitState();
bool SccVisitorLatticeArcBackArc();
bool SccVisitorLatticeArcForwardOrCrossArc();
void DfsVisit(VectorFstOfLatticeArc &fst, vector<int> &order_);
void DfsVisit2(VectorFstOfLatticeArc &fst, TopOrderVisitor *visitor);
void DfsVisit3(VectorFstOfCompactLatticeArc &fst, SccVisitorCompactLatticeArc *visitor);
void ConnectForCompactLattice(VectorFstOfCompactLatticeArc *fst);

#endif
