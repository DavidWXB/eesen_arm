#ifndef GET_BEST_PATH_H_
#define GET_BEST_PATH_H_

#include "../wfst/VectorFstOfLatticeArc.h"
#include "../decode/LatticeFasterDecoder.h"
#include "../wfst/VectorFstOfCompactLatticeArc.h"

#define OPT_FOR_GETBESTPATH

typedef struct VectorHasher {  // hashing function for vector<Int>.
	size_t operator()(const std::vector<int> &x) const {
		size_t ans = 0;
		for (uint i=0; i < x.size(); i++) {
			ans *= kPrime;
			ans += x[i];
		}
		return ans;
	}
	VectorHasher() {
	}
private:
	static const int kPrime = 7853;
} VectorHasherT;

void CompactLatticeShortestPath(const VectorFstOfCompactLatticeArc &clat, VectorFstOfCompactLatticeArc *shortest_path);

void ScaleLattice(double scale[2][2], VectorFstOfCompactLatticeArc *clat);

bool GetLinearSymbolSequence(VectorFstOfLatticeArc &clat, std::vector<int> *isymbols_out, std::vector<int> *osymbols_out, LatticeWeight *weight);

int LatticeGetBestPath(VectorFstOfCompactLatticeArc clat, std::string key, SymbolTable *word_syms, string str);

void StateSort(VectorFstOfLatticeArc *ifst, std::vector<int>order);

bool TopSort(VectorFstOfLatticeArc *ifst);

//ת��lattice����
void ConvertLattice(VectorFstOfLatticeArc &ifst, VectorFstOfCompactLatticeArc *ofst);

void ConvertLattice2(VectorFstOfCompactLatticeArc &ifst,  VectorFstOfLatticeArc*ofst);

bool ReachedFinal(LatticeFasterDecoder * decoder);

bool GetRawLattice(LatticeFasterDecoder * decoder, VectorFstOfLatticeArc *lat, bool use_final_probs);

void GetBestPathMain(LatticeFasterDecoder * decoder, double acoustic_scale, SymbolTable *word_syms, string str);

void GetBestPathMainForToken(LatticeFasterDecoder * decoder, double acoustic_scale, SymbolTable *word_syms, string str);

#endif
