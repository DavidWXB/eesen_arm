#include <vector>
#include <float.h>
#include <algorithm>
#include <unordered_map>
#include "GetBestPathMain.h"
#include "SccVisitorLatticeArc.h"
#include "SccVisitorCompactLatticeArc.h"
#include "../test/test.h"
#include "../wfst/VectorFstOfLatticeArc.h"
#include "../wfst/VectorStateOflatticeArc.h"
#include "../wfst/VectorFstOfCompactLatticeArc.h"
#include "../determine/Invert.h"
#include "../determine/ArcSort.h"
#include "../determine/DeterminizeLatticePruned.h"

using namespace std;
using namespace eesen;

#ifdef OPT_TEST
//自定义排序函数  
bool SortByM1(const float &v1, const float &v2)//注意：本函数的参数的类型一定要与vector中元素的类型一致  
{
	return v1 < v2;//升序排列  
}
float gtest[10000];
float gtest2[10000];
int   gtestnum = 0;
#endif

#ifdef OPT_FOR_GETBESTPATH
int TokenGetBestPath(LatticeFasterDecoder * decoder, SymbolTable *word_syms, std::string str, double acoustic_scale)
{
	for (uint i = 0; i < decoder->active_toks_.size(); i++)
	{
		TokenList list = decoder->active_toks_[i];
		Token *p_tok = list.toks;
		while (p_tok != NULL)
		{
			p_tok->tot_cost = FLT_MAX;

			p_tok->last = NULL;

#ifdef OPT_TEST
			p_tok->num = i;
			p_tok->extra_cost = FLT_MAX;
#endif
			p_tok = p_tok->next;
		}
	}

	float minCost = FLT_MAX;
	Token * minCostToken = NULL;

	unordered_map<Token*, float> final_costs_local;

	//sunhao 10-18  可以删除
	const unordered_map<Token*, float> &final_costs =
		(decoder->decoding_finalized_ ? decoder->final_costs_ : final_costs_local);
	if (!decoder->decoding_finalized_)
		ComputeFinalCosts(decoder, &final_costs_local, NULL, NULL);

	int32 num_frames = decoder->active_toks_.size() - 1;
	const int32 bucket_count = decoder->num_toks_ / 2 + 3;
	unordered_map<Token*, int> tok_map(bucket_count);

	std::vector<Token*> token_list;
	for (int32 f = 0; f <= num_frames; f++) {
		if (decoder->active_toks_[f].toks == NULL) {
			return false;
		}
		//保证节点的index是递增的，当大index的节点指向小index的节点时，把小的index节点放到最后面赋值为最大的index，原来的节点保存改为NULL
		//这里的目的是为后面最优路径做准备，因为最优路径是从小到大计算cost直至最后一帧，所以是不能有倒序的指向的
		TopSortTokens(decoder->active_toks_[f].toks, &token_list);

		//起始点的cost一定要置0
		if (f == 0) {
			token_list[0]->tot_cost = 0.0;

#ifdef OPT_TEST
			//token_list[0]->extra_cost = 0.0;
#endif
		}

		//正向计算cost
		{
			for (uint i = 0; i < token_list.size(); i++) {
				Token *p_tok = token_list[i];
				if (p_tok==NULL) {
					continue;
				}
				ForwardLink *p_link = p_tok->links;
				while (p_link != NULL)
				{
					float cost_offset = 0.0;
					if (p_link->ilabel != 0) {  // emitting..
						cost_offset = decoder->cost_offsets_[f];
					}

					float cost = p_tok->tot_cost + p_link->graph_cost*10.0 + (p_link->acoustic_cost - cost_offset) / acoustic_scale;// p_link->acoustic_cost + p_link->graph_cost;

#ifdef OPT_TEST
					static Token *toktmp;
					if (cost - (-57.6052246)<0.001&&cost - (-57.6052246)>-0.001) {
						p_tok->extra_cost = 10.0;
						toktmp = p_link->next_tok;
					}
					p_link->next_tok->extra_cost = min(p_tok->extra_cost, p_link->next_tok->extra_cost);
#endif

					if (cost < p_link->next_tok->tot_cost) {
						p_link->next_tok->tot_cost = cost;
						p_link->next_tok->last = p_tok;
					}
					p_link = p_link->next;
				}
				p_tok = p_tok->next;
			}

			if (f == num_frames) {
				for (uint i = 0; i < token_list.size(); i++) {
					Token *p_tok = token_list[i];
					if (p_tok == NULL) {
						continue;
					}
					//最后一帧计算最优cost和token
					const unordered_map<Token*, float> &final_costs = decoder->final_costs_;
					unordered_map<Token*, float>::const_iterator iter = final_costs.find(p_tok);
					if (iter != final_costs.end()) {
						float cost = p_tok->tot_cost + iter->second*10.0;
						if (cost < minCost) {
							minCost = cost;
							minCostToken = p_tok;
						}

#ifdef OPT_TEST
						if (cost<FLT_MAX) {
							gtest[gtestnum] = cost;
							gtestnum++;
						}
#endif
					}
				}
			}
		}
	}

#ifdef OPT_TEST
	vector<float> varr(gtest, gtest + gtestnum);
	std::sort(varr.begin(), varr.end(), SortByM1);
#endif

	if (minCostToken == NULL) {
		return 0;
	}

	//反向得到最优token链
	vector<Token *> tok_list;
	while (minCostToken != NULL) {
		tok_list.push_back(minCostToken);
		minCostToken = minCostToken->last;
	}
	std::reverse(tok_list.begin(), tok_list.end());

	//得到最优链路的olabel
	vector<int32> olabel_seq;
	for (uint i = 0; i < tok_list.size() - 1; i++) {
		Token *tok = tok_list[i];
		ForwardLink *link = tok->links;
		while (link != NULL) {
			if (link->next_tok == tok_list[i + 1]) {
				if (link->olabel != 0) {
					olabel_seq.push_back(link->olabel);
				}
				break;
			}
			link = link->next;
		}
	}

	//查表得到结果
	if (word_syms != NULL) {
		printf("key: ");
		for (size_t i = 0; i < olabel_seq.size(); i++) {
			std::string s = FindOfSymbolTable(word_syms, olabel_seq[i]);
			if (s == "")
				printf("\nnot in symbol table\n");
			printf("%s,", s.c_str());
			printf("olabel_seq[%d]=%d\n",i, olabel_seq[i]);
		}
		printf("\n");
	}

	return 1;
}
#endif

void CompactLatticeShortestPath(const VectorFstOfCompactLatticeArc &clat, VectorFstOfCompactLatticeArc *shortest_path)
{
	shortest_path->states_.clear();
	shortest_path->start_ = -1;
	if (clat.start_ == -1) return;
	vector<std::pair<double, int> > best_cost_and_pred(clat.states_.size() + 1);
	int superfinal = clat.states_.size();
	for (int s = 0; s <= superfinal; s++) {
		best_cost_and_pred[s].first = FLT_MAX;
		best_cost_and_pred[s].second = -1;
	}
	best_cost_and_pred[0].first = 0;
	for (uint s = 0; s < clat.states_.size(); s++) {
		double my_cost = best_cost_and_pred[s].first;
		//计算每个state的前向cost，放到best_cost_and_pred里
		int i = 0;
		int iend = clat.states_[s]->arcs_.size();
		for (; i < iend;i++) {
			const CompactLatticeArc &arc = clat.states_[s]->arcs_[i];
			double arc_cost =(double) arc.weight.weight_.value1_ + (double) arc.weight.weight_.value2_;
			double next_cost = my_cost + arc_cost;

			if (next_cost < best_cost_and_pred[arc.nextstate].first) {
				best_cost_and_pred[arc.nextstate].first = next_cost;
				best_cost_and_pred[arc.nextstate].second = s;
			}
		}
		double final_cost = (double)clat.states_[s]->final_.weight_.value1_ + (double)clat.states_[s]->final_.weight_.value2_;
	    double tot_final = my_cost + final_cost;
		//找到代价最小的cost
		if (tot_final < best_cost_and_pred[superfinal].first) {
			best_cost_and_pred[superfinal].first = tot_final;
			best_cost_and_pred[superfinal].second = s;
		}
	}

	std::vector<int> states; // states on best path.
	int cur_state = superfinal;
	//取出best路径的所有states
	while (cur_state != 0) {
		int prev_state = best_cost_and_pred[cur_state].second;
		if (prev_state == -1) {
			return; // return empty best-path.
		}
		states.push_back(prev_state);
		cur_state = prev_state;
	}
	std::reverse(states.begin(), states.end());
	//把取出states的arcs，放到shortest_path里
	for (size_t i = 0; i < states.size(); i++) {
		VectorStateOfCompactLatticeArc *tt=new VectorStateOfCompactLatticeArc();
		tt->final_.weight_.value1_ = FLT_MAX;
		tt->final_.weight_.value2_ = FLT_MAX;
		tt->niepsilons_ = 0;
		tt->noepsilons_ = 0;
		shortest_path->states_.push_back(tt);
	}

	for (int s = 0; s< (int)states.size(); s++) {
		if (s == 0)
			shortest_path->start_ = s;
		if (s + 1 < (int)states.size()) { // transition to next state.
			bool have_arc = false;
			CompactLatticeArc cur_arc;
			int i = 0;
			int iend = clat.states_[states[s]]->arcs_.size();

			for(; i < iend; i++) {
				const CompactLatticeArc &arc = clat.states_[states[s]]->arcs_[i];
				if (arc.nextstate == states[s + 1]) {
						if (!have_arc ||
							(double)arc.weight.weight_.value1_ + (double)arc.weight.weight_.value2_ < (double)cur_arc.weight.weight_.value1_ + (double)cur_arc.weight.weight_.value2_) {
						cur_arc = arc;
						have_arc = true;
					}
				}
			}

			CompactLatticeArc arct;
			arct.ilabel = cur_arc.ilabel;
			arct.olabel = cur_arc.olabel;
			arct.weight = cur_arc.weight;
			arct.nextstate = s + 1;
			shortest_path->states_[s]->arcs_.push_back(arct);
		}
		else { // final-prob.
			shortest_path->states_[s]->final_ = clat.states_[states[s]]->final_;
		}
	}
}

void ScaleLattice(double scale[2][2], VectorFstOfCompactLatticeArc *fst)
{
	double ans2[2][2] = { 0 };
	ans2[0][0] = ans2[1][1] = 1.0;

	if (scale == ans2) // nothing to do.
		return;
	int num_states = fst->states_.size();
	for (int s = 0; s < num_states; s++) {
		int i = 0;
		int endi = fst->states_[s]->arcs_.size();
		for (; i < endi;) {
			CompactLatticeArc arc = fst->states_[s]->arcs_[i];
			if (arc.weight.weight_.value1_ == FLT_MAX) {
				arc.weight.weight_.value1_ = FLT_MAX;
				arc.weight.weight_.value2_ = FLT_MAX;
			}
			else {
				arc.weight.weight_.value1_ = scale[0][0] * arc.weight.weight_.value1_ + scale[0][1] * arc.weight.weight_.value2_;
				arc.weight.weight_.value2_ = scale[1][0] * arc.weight.weight_.value1_ + scale[1][1] * arc.weight.weight_.value2_;
			}
			//可以去掉，arc的ilabel和olabel没有改变
			if (fst->states_[s]->arcs_[i].ilabel == 0)
				--fst->states_[s]->niepsilons_;
			if (fst->states_[s]->arcs_[i].olabel == 0)
				--fst->states_[s]->noepsilons_;
			if (arc.ilabel == 0)
				++fst->states_[s]->niepsilons_;
			if (arc.olabel == 0)
				++fst->states_[s]->noepsilons_;

			fst->states_[s]->arcs_[i] = arc;
			i++;
		}
		CompactLatticeWeight final_weight = fst->states_[s]->final_;

		if (final_weight.weight_.value1_ != FLT_MAX || final_weight.weight_.value2_ != FLT_MAX) {
			if (final_weight.weight_.value1_ == FLT_MAX) {
				final_weight.weight_.value1_ = FLT_MAX;
				final_weight.weight_.value2_ = FLT_MAX;
			}
			else {
				final_weight.weight_.value1_ = scale[0][0] * final_weight.weight_.value1_ + scale[0][1] * final_weight.weight_.value2_;
				final_weight.weight_.value2_ = scale[1][0] * final_weight.weight_.value1_ + scale[1][1] * final_weight.weight_.value2_;
			}
			fst->states_[s]->final_ = final_weight;
		}
	}
}

bool GetLinearSymbolSequence(VectorFstOfLatticeArc &fst, std::vector<int32> *isymbols_out, std::vector<int32> *osymbols_out, LatticeWeight *tot_weight_out)
{
	LatticeWeight tot_weight;
	tot_weight.value1_ = 0;
	tot_weight.value2_ = 0;
	LatticeWeight zero;
	zero.value1_ = FLT_MAX;
	zero.value2_ = FLT_MAX;
	vector<int32> ilabel_seq;
	vector<int32> olabel_seq;

	int cur_state = fst.start_;
	if (cur_state == -1) {  // empty sequence.
		if (isymbols_out != NULL) isymbols_out->clear();
		if (osymbols_out != NULL) osymbols_out->clear();
		if (tot_weight_out != NULL) *tot_weight_out = zero;
		return true;
	}
	while (1) {
		LatticeWeight w = fst.states_[cur_state]->final_;
		if (w.value1_!=FLT_MAX && w.value2_!=FLT_MAX)  {

			tot_weight.value1_ = (float)w.value1_ + (float)tot_weight.value1_;
			tot_weight.value2_ = (float)w.value2_ + (float)tot_weight.value2_;

			if (fst.states_[cur_state]->arcs_.size() != 0) return false;
			if (isymbols_out != NULL) *isymbols_out = ilabel_seq;
			if (osymbols_out != NULL) *osymbols_out = olabel_seq;
			if (tot_weight_out != NULL) *tot_weight_out = tot_weight;
			return true;
		}
		else {
			if (fst.states_[cur_state]->arcs_.size() != 1)
				return false;
			
			const LatticeArc &arc = fst.states_[cur_state]->arcs_[0];
			tot_weight.value1_ = (float)arc.weight.value1_ + (float)tot_weight.value1_;
			tot_weight.value2_ = (float)arc.weight.value2_ + (float)tot_weight.value2_;
			if (arc.ilabel != 0) ilabel_seq.push_back(arc.ilabel);
			if (arc.olabel != 0) olabel_seq.push_back(arc.olabel);
			cur_state = arc.nextstate;
		}
	}
}

int LatticeGetBestPath(VectorFstOfCompactLatticeArc fst, std::string key, SymbolTable *word_syms, string str)
{
	float acoustic_scale = 1.0;
	float lm_scale = 10.0;

	int32 n_done = 0, n_fail = 0;
	int64 n_frame = 0;
	LatticeWeight tot_weight;
	tot_weight.value1_ = 0;
	tot_weight.value2_ = 0;

	double scale[2][2] = { 0 };
	scale[0][0] = lm_scale;
	scale[1][1] = acoustic_scale;

	ScaleLattice(scale, &fst);
	VectorFstOfCompactLatticeArc clat_best_path;
	CompactLatticeShortestPath(fst, &clat_best_path);
	VectorFstOfLatticeArc best_path;
	ConvertLattice2(clat_best_path, &best_path);

	if (best_path.start_ == -1) {
		n_fail++;
	}
	else {
		std::vector<int32> alignment;
		std::vector<int32> words;
		LatticeWeight weight;
		GetLinearSymbolSequence(best_path, &alignment, &words, &weight);
		if (word_syms != NULL) {
			printf("key: ");
			for (size_t i = 0; i < words.size(); i++) {
				std::string s = FindOfSymbolTable(word_syms, words[i]);				
				if (s == "")
					printf("\nnot in symbol table\n");
				printf("%s,",s.c_str());
			}
			printf("\n");
		}
		n_done++;
		n_frame += alignment.size();
		tot_weight.value1_ = (float)tot_weight.value1_ + (float)weight.value1_;
		tot_weight.value2_ = (float)tot_weight.value2_ + (float)weight.value2_;
	}
	VectorFstOfLatticeArcDeleteStates(&best_path);
	DestroyVectorFstOfCompactLatticeArc(&clat_best_path);
	return 1;
}

void StateSort(VectorFstOfLatticeArc *fst, std::vector<int>order)
{
	if (order.size() != fst->states_.size()) return;
	if (fst->start_ == -1) return;
	vector<bool> done(order.size(), false);
	vector<LatticeArc> arcsa, arcsb;
	vector<LatticeArc> *arcs1 = &arcsa, *arcs2 = &arcsb;
	fst->start_ = order[fst->start_];

	int i = 0;
	int endindex = fst->states_.size();
	for (; i < endindex;i++) {
		
		int s1 = i, s2;
		if (done[s1]) {
			continue;
		}

		LatticeWeight final1 = fst->states_[s1]->final_, final2 ;
		final2.value1_ = FLT_MAX;
		final2.value2_ = FLT_MAX;
		arcs1->clear();
		int j =0;
		int jend = fst->states_[s1]->arcs_.size();
		for (; j < jend;) {
			LatticeArc t = fst->states_[s1]->arcs_[j];
			arcs1->push_back(t);
			j++;
		}

		for (; !done[s1]; s1 = s2, final1 = final2, swap(arcs1, arcs2)) {

			s2 = order[s1];

			if (!done[s2]) {
				final2 = fst->states_[s2]->final_;
				arcs2->clear();
				int j2 = 0;
				int jend2 = fst->states_[s2]->arcs_.size();
				for (; j2 < jend2;) {
					LatticeArc t = fst->states_[s2]->arcs_[j2];
					arcs2->push_back(t);
					j2++;
				}
			}

			fst->states_[s2]->final_ = final1;
			fst->states_[s2]->arcs_.clear();
			fst->states_[s2]->niepsilons_ = 0;
			fst->states_[s2]->noepsilons_ = 0;

			for (size_t n = 0; n < arcs1->size(); ++n) {
				LatticeArc arc = (*arcs1)[n];
				arc.nextstate = order[arc.nextstate];
				if (arc.ilabel == 0)
					++fst->states_[s2]->niepsilons_;
				if (arc.olabel == 0)
					++fst->states_[s2]->noepsilons_;
				fst->states_[s2]->arcs_.push_back(arc);
			}
			done[s1] = true;
		}
	}
}

bool TopSort(VectorFstOfLatticeArc *fst)
{
	vector<int> order;
	bool acyclic=true;

	TopOrderVisitor top_order_visitor;
	top_order_visitor.order_ = &order;
	top_order_visitor.acyclic_ = &acyclic;
	top_order_visitor.finish_ = new vector<int>;

	DfsVisit2(*fst, &top_order_visitor);
	if (acyclic) {
		StateSort(fst, order);
	}
	return acyclic;
}

void GetStateProperties(const VectorFstOfLatticeArc &fst, int max_state, vector<unsigned char> *props)
{
	props->clear();
	if (fst.start_ < 0) return;
	props->resize(max_state + 1, 0);
	(*props)[fst.start_] |= 0x2;
	for (int s = 0; s <= max_state; s++) {
		unsigned char &s_info = (*props)[s];
		DfsStateLatticeArc *r = (DfsStateLatticeArc*)malloc(sizeof(DfsStateLatticeArc));
		r->state_id = s;
		r->i = 0;
		VectorStateOfLatticeArc *aiter = fst.states_[s];
		for (uint i = 0; r->i < aiter->arcs_.size() && i<aiter->arcs_.size(); i++) {
			const LatticeArc &arc = aiter->arcs_[i];
			if (arc.ilabel != 0) s_info |= 0x80;	//有ilabel
			if (arc.olabel != 0) s_info |= 0x40;	//有olabel
			int nexts = arc.nextstate;
			unsigned char &nexts_info = (*props)[nexts];
			if (s_info & 0x10) s_info |= 0x20;		//至少有两个arcs
			s_info |= 0x10;							//有一个arc
			if (nexts_info & 0x4) nexts_info |= 0x8;//至少有两个前节点相连
			nexts_info |= 0x4;						//有一个前节点相连
			r->i++;
		}
		free(r);
		
		if (fst.states_[s]->final_.value1_ != FLT_MAX || fst.states_[s]->final_.value2_ != FLT_MAX)
			s_info |= 0x1;
	}
	
}
//转换lattice类型
//把ifst的数据按深度排序放到ofst里，并删除前后只有一个相邻节点且无olabel的节点
void ConvertLattice(VectorFstOfLatticeArc &ifst, VectorFstOfCompactLatticeArc *ofst)
{
	VectorFstOfLatticeArc *ffst = new VectorFstOfLatticeArc();
	ffst->start_ = -1;
	ffst->states_.clear();
	
	if (ifst.start_ < 0) return;
	vector<int> order;
	//把states按深度优先排序出来存到order里,并去除不相连的states，把相连的states统计到order里
	DfsVisit(ifst, order);
	//找到最大元素
	int max_state = *(std::max_element(order.begin(), order.end()));
	vector<unsigned char> state_properties;
	GetStateProperties(ifst, max_state, &state_properties);
	vector<bool> remove(max_state + 1);
	//0x4 只有一个前节点相连 0x10 只有一个arc 0x80 有ilabel 0x40 有olabel
	//把只有一个前节点相连且只有一个arc且无olabel的节点存到remove里
	for (int i = 0; i <= max_state; i++)
		remove[i] = (state_properties[i] == (0x4 | 0x10)
		|| state_properties[i] == (0x4 | 0x10 | 0x80));
	vector<int> state_mapping(max_state + 1, -1);
	typedef unordered_map<vector<int32>, int32, VectorHasherT> SymbolMapType;
	SymbolMapType symbol_mapping;
	vector<vector<int32> > labels;
	int symbol_counter = 0;
	{
		vector<int32> eps;
		symbol_mapping[eps] = symbol_counter++;
	}

	vector<int32> this_sym;
	for (size_t i = 0; i < order.size(); i++) {
		int state = order[i];

		if (!remove[state]) {
			int &new_state = state_mapping[state];
			if (new_state == -1) {
				VectorStateOfLatticeArc *newstate = new VectorStateOfLatticeArc();
				newstate->niepsilons_ = 0;
				newstate->noepsilons_ = 0;
				newstate->final_.value1_ = FLT_MAX;
				newstate->final_.value2_ = FLT_MAX;
				ffst->states_.push_back(newstate);
				new_state = ffst->states_.size() - 1;
			}
			DfsStateLatticeArc *r = new DfsStateLatticeArc();
			r->state_id = state;
			r->i = 0;
			VectorStateOfLatticeArc *aiter = ifst.states_[state];
			for (uint i = 0; r->i < aiter->arcs_.size() && i < aiter->arcs_.size(); r->i++, i++) {
				 LatticeArc arc = aiter->arcs_[i];
				if (arc.ilabel == 0) this_sym.clear();
				else {
					this_sym.resize(1);
					this_sym[0] = arc.ilabel;
				}
				//把arc后面需要删除的节点的weight都累加在一起 
				while (remove[arc.nextstate]) {
					VectorStateOfLatticeArc *aiter2 = ifst.states_[arc.nextstate];
					const LatticeArc  &nextarc = aiter2->arcs_[0];
					arc.weight.value1_ = arc.weight.value1_+ nextarc.weight.value1_;
					arc.weight.value2_ = arc.weight.value2_ + nextarc.weight.value2_;
					if (nextarc.ilabel != 0) this_sym.push_back(nextarc.ilabel);
					arc.nextstate = nextarc.nextstate;
				}
				int &new_nextstate = state_mapping[arc.nextstate];
				if (new_nextstate == -1) {
					VectorStateOfLatticeArc *newstate2 = new VectorStateOfLatticeArc();
					newstate2->niepsilons_ = 0;
					newstate2->noepsilons_ = 0;
					newstate2->final_.value1_ = FLT_MAX;
					newstate2->final_.value2_ = FLT_MAX;
					ffst->states_.push_back(newstate2);
					new_nextstate = ffst->states_.size() - 1;
				}
				arc.nextstate = new_nextstate;
				if (symbol_mapping.count(this_sym) != 0)
					arc.ilabel = symbol_mapping[this_sym];
				else
					arc.ilabel = symbol_mapping[this_sym] = symbol_counter++;
			
				if (arc.ilabel == 0)
					++ffst->states_[new_state]->niepsilons_;
				if (arc.olabel == 0)
					++ffst->states_[new_state]->noepsilons_;
				ffst->states_[new_state]->arcs_.push_back(arc);
			}
			if (ifst.states_[state]->final_.value1_ != FLT_MAX && ifst.states_[state]->final_.value2_ != FLT_MAX) {
				LatticeWeight final = ifst.states_[state]->final_;
				ffst->states_[new_state]->final_ = final;
			}
			delete(r);
		}
	}
	
	ffst->start_ = state_mapping[ifst.start_];
	// Now output the symbol sequences.
	labels.resize(symbol_counter);
	for (SymbolMapType::const_iterator iter = symbol_mapping.begin();
		 iter != symbol_mapping.end(); ++iter) {
		 labels[iter->second] = iter->first;
	}
	TopSort(ffst);

	ofst->states_.clear();
	ofst->start_ = -1;
	int num_states =ffst->states_.size() ;

	for (int s = 0; s < num_states; s++) {
		VectorStateOfCompactLatticeArc *vt=new VectorStateOfCompactLatticeArc();
		vt->final_.weight_.value1_ = FLT_MAX;
		vt->final_.weight_.value2_ = FLT_MAX;
		vt->niepsilons_ = 0;
		vt->noepsilons_ = 0;
		ofst->states_.push_back(vt);
	}
	ofst->start_ = ffst->start_;
	for (int s = 0; s < num_states; s++) {
		LatticeWeight final_weight = ffst->states_[s]->final_;
		if (final_weight.value1_ != FLT_MAX || final_weight.value2_ != FLT_MAX) {
			CompactLatticeWeight final_compact_weight;
			vector<int32> second;
			final_compact_weight.weight_ = final_weight;
			final_compact_weight.string_ = second;
			ofst->states_[s]->final_ = final_compact_weight;
		}
		int j = 0;
		int jend = ffst->states_[s]->arcs_.size();
		for (; j < jend;) {
			LatticeArc arc = ffst->states_[s]->arcs_[j];
			CompactLatticeArc compact_arc;
			compact_arc.ilabel = arc.olabel;
			compact_arc.olabel = arc.olabel;
			compact_arc.nextstate = arc.nextstate;
			CompactLatticeWeight tt;
			tt.weight_ = arc.weight;
			tt.string_ = labels[arc.ilabel];
			compact_arc.weight = tt;

			if (compact_arc.ilabel == 0)
				++ofst->states_[s]->niepsilons_;
			if (compact_arc.olabel == 0)
				++ofst->states_[s]->noepsilons_;
			ofst->states_[s]->arcs_.push_back(compact_arc);
			j++;
		}
	}
	VectorFstOfLatticeArcDeleteStates(ffst);
	delete(ffst);
}

void ConvertLattice2(VectorFstOfCompactLatticeArc &ifst, VectorFstOfLatticeArc*ofst)
{
	ofst->states_.clear();
	ofst->start_ = -1;
	int num_states = ifst.states_.size();
	for (int s = 0; s < num_states; s++) {
		VectorStateOfLatticeArc *vt = new VectorStateOfLatticeArc();
		vt->final_.value1_ = FLT_MAX;
		vt->final_.value2_ = FLT_MAX;
		vt->niepsilons_ = 0;
		vt->noepsilons_ = 0;
		ofst->states_.push_back(vt);
	}
	LatticeWeight one;
	one.value1_ = 0;
	one.value2_ = 0;
	ofst->start_ = ifst.start_;
	for (int s = 0; s < num_states; s++) {
		CompactLatticeWeight final_weight = ifst.states_[s]->final_;
		if(final_weight.weight_.value1_ != FLT_MAX || final_weight.weight_.value2_ != FLT_MAX ) {
			int cur_state = s;
			size_t string_length = final_weight.string_.size();
			for (size_t n = 0; n < string_length; n++) {
				VectorStateOfLatticeArc *vt = new VectorStateOfLatticeArc();
				vt->final_.value1_ = FLT_MAX;
				vt->final_.value2_ = FLT_MAX;
				vt->niepsilons_ = 0;
				vt->noepsilons_ = 0;
				ofst->states_.push_back(vt);
				int next_state = ofst->states_.size() - 1;
				int ilabel = 0;
				LatticeArc arc;
				LatticeWeight weight = (n == 0 ? final_weight.weight_ : one);
				arc.ilabel = ilabel;
				arc.olabel = final_weight.string_[n];
				arc.weight = weight;
				arc.nextstate = next_state;
				if (true)
					std::swap(arc.ilabel, arc.olabel);
				ofst->states_[cur_state]->arcs_.push_back(arc);
				cur_state = next_state;
			}
			ofst->states_[cur_state]->final_ = string_length > 0 ? one : final_weight.weight_;
		}
		for (uint i = 0; i < ifst.states_[s]->arcs_.size();i++) {
			const CompactLatticeArc &arc = ifst.states_[s]->arcs_[i];
			size_t string_length = arc.weight.string_.size();
			int cur_state = s;
			// for all but the last element in the string--
			// add a temporary state.
			for (size_t n = 0; n + 1 < string_length; n++) {
				VectorStateOfLatticeArc *vt = new VectorStateOfLatticeArc();
				vt->final_.value1_ = FLT_MAX;
				vt->final_.value2_ = FLT_MAX;
				vt->niepsilons_ = 0;
				vt->noepsilons_ = 0;				
				ofst->states_.push_back(vt);
				
				int next_state = ofst->states_.size() - 1;
				int ilabel = (n == 0 ? arc.ilabel : 0),
					olabel =(int)(arc.weight.string_[n]);
				LatticeWeight weight = (n == 0 ? arc.weight.weight_ : one);
				LatticeArc new_arc;
				new_arc.ilabel = ilabel;
				new_arc.olabel = olabel;
				new_arc.weight = weight;
				new_arc.nextstate = arc.nextstate;
				if (true)
					std::swap(new_arc.ilabel, new_arc.olabel);
				ofst->states_[cur_state]->arcs_.push_back(new_arc);
				cur_state = next_state;
			}
			int ilabel = (string_length <= 1 ? arc.ilabel : 0),
				olabel = (string_length > 0 ? arc.weight.string_[string_length - 1] : 0);
			
			LatticeWeight weight = (string_length <= 1 ? arc.weight.weight_ :one);
			LatticeArc new_arc;
			new_arc.ilabel = ilabel;
			new_arc.olabel = olabel;
			new_arc.weight = weight;
			new_arc.nextstate = arc.nextstate;
			if (true)
				std::swap(new_arc.ilabel, new_arc.olabel);
			ofst->states_[cur_state]->arcs_.push_back(new_arc);
		}
	}
}

bool GetRawLattice(LatticeFasterDecoder * decoder, VectorFstOfLatticeArc *lat, bool use_final_probs)
{
	unordered_map<Token*, float> final_costs_local;

	//sunhao 10-18  可以删除
	const unordered_map<Token*, float> &final_costs =
		(decoder->decoding_finalized_ ? decoder->final_costs_ : final_costs_local);
	if (!decoder->decoding_finalized_ && use_final_probs)
		ComputeFinalCosts(decoder,&final_costs_local, NULL, NULL);
	lat->states_.clear();
	lat->start_ = -1;
	
	int32 num_frames = decoder->active_toks_.size() - 1;
	const int32 bucket_count = decoder->num_toks_ / 2 + 3;
	unordered_map<Token*, int> tok_map(bucket_count);
	std::vector<Token*> token_list;
	for (int32 f = 0; f <= num_frames; f++) {
		if (decoder->active_toks_[f].toks == NULL) {
			return false;
		}
		//保证节点的index是递增的，当大index的节点指向小index的节点时，把小的index节点放到最后面赋值为最大的index，原来的节点保存改为NULL
		//这里的目的是为后面最优路径做准备，因为最优路径是从小到大计算cost直至最后一帧，所以是不能有倒序的指向的
		TopSortTokens(decoder->active_toks_[f].toks, &token_list);
#ifdef TEST_
		TokenListWrite(token_list);
#endif		
		for (size_t i = 0; i < token_list.size(); i++)
			if (token_list[i] != NULL) {
				VectorStateOfLatticeArc *newstate = new VectorStateOfLatticeArc();
				newstate->final_.value1_ = (float)FLT_MAX;
				newstate->final_.value2_ = (float)FLT_MAX;
				newstate->niepsilons_ = 0;
				newstate->noepsilons_ = 0;
				lat->states_.push_back(newstate);
				tok_map[token_list[i]] = lat->states_.size() - 1;
			}
	}
	lat->start_ = 0; 

	//遍历每一帧的每个token的每个link，把link转换成Arc保存到ofst里
	//注意：由于上面的TopSortTokens函数，每一帧的空转移在当前帧内被放到了后面，Arc的state也是在当前帧的后面
	for (int32 f = 0; f <= num_frames; f++) {
		for (Token *tok = decoder->active_toks_[f].toks; tok != NULL; tok = tok->next) {
			int cur_state = tok_map[tok];
			for (ForwardLink *l = tok->links;
				l != NULL;
				l = l->next) {
				unordered_map<Token*, int>::const_iterator iter =
					tok_map.find(l->next_tok);
				int nextstate = iter->second;
				float cost_offset = 0.0;
				if (l->ilabel != 0) {  // emitting..
					cost_offset = decoder->cost_offsets_[f];
				}
				//LatticeArc arc
				
				LatticeArc arc;
				arc.ilabel = l->ilabel;
				arc.olabel = l->olabel;
				LatticeWeight weight;
				weight.value1_ = l->graph_cost;
				weight.value2_ = l->acoustic_cost - cost_offset;
				arc.weight = weight;
				arc.nextstate = nextstate;
				VectorStateOfLatticeArc *curr = lat->states_[cur_state];

				if (arc.ilabel == 0)
					++curr->niepsilons_;
				if (arc.olabel == 0)
					++curr->noepsilons_;				
				curr->arcs_.push_back(arc);
			}
			if (f == num_frames) {
				if (use_final_probs && !final_costs.empty()) {
					unordered_map<Token*, float>::const_iterator iter = final_costs.find(tok);
					if (iter != final_costs.end()) {
						LatticeWeight lw;
						lw.value1_ =iter->second ;
						lw.value2_ = 0;
						//LatticeWeight final = lat->states_[cur_state]->final_;	// not used
						lat->states_[cur_state]->final_ = lw;
					}
				}
				else {
					LatticeWeight lw;
					lw.value1_ = 0;
					lw.value2_ = 0;
					//LatticeWeight final = lat->states_[cur_state]->final_;		// not used
					lat->states_[cur_state]->final_ = lw;
				}
			}
		}
	}
	return lat->states_.size();
}

#ifdef OPT_FOR_GETBESTPATH
void GetBestPathMainForToken(LatticeFasterDecoder * decoder, double acoustic_scale, SymbolTable *word_syms, string str)
{
	TokenGetBestPath(decoder, word_syms, str, acoustic_scale);
}
#endif

void GetBestPathMain(LatticeFasterDecoder * decoder, double acoustic_scale,SymbolTable *word_syms, string str)
{
	VectorFstOfLatticeArc lat;
	VectorFstOfCompactLatticeArc clat;

	GetRawLattice(decoder, &lat,true);

	ConvertLattice(lat, &clat);

	double scale[2][2] = { 0 };
	scale[0][0] = 1.0;
	scale[1][1] = 1.0 / acoustic_scale;

	ScaleLattice(scale, &clat);
	LatticeGetBestPath(clat, "key", word_syms, str);
	VectorFstOfLatticeArcDeleteStates(&lat);
	DestroyVectorFstOfCompactLatticeArc(&clat);
}
