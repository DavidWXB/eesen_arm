#include <stack>
#include <vector>
#include <float.h>
#include <algorithm>
#include "SccVisitorCompactLatticeArc.h"

using std::vector;

void DfsStateCompactLatticeArcDestroy(DfsStateLatticeArc *dfsStateLatticeArc, MemoryPoolOfDfsStateLatticeArc *memoryPool)
{
}

void SccVisitorCompactLatticeArcInitVisit(VectorFstOfCompactLatticeArc &fst, SccVisitorCompactLatticeArc &visitor)
{
	if (visitor.scc_)
		visitor.scc_->clear();
	if (visitor.access_)
		visitor.access_->clear();
	if (visitor.coaccess_) {
		visitor.coaccess_->clear();
		visitor.coaccess_internal_ = false;
	}
	else {
		visitor.coaccess_ = new vector<bool>;
		visitor.coaccess_internal_ = true;
	}
	visitor.fst_ = &fst;
	visitor.start_ = fst.start_;
	visitor.nstates_ = 0;
	visitor.nscc_ = 0;
	visitor.dfnumber_ = new vector<int>;
	visitor.lowlink_  = new vector<int>;
	visitor.onstack_  = new vector<bool>;
	visitor.scc_stack_ = new vector<int>;
}

void SccVisitorCompactLatticeArcFinishState(VectorFstOfCompactLatticeArc &fst, SccVisitorCompactLatticeArc &visitor, int s, int p)
{
	if (fst.states_[s]->final_.weight_.value1_ != FLT_MAX || fst.states_[s]->final_.weight_.value2_ != FLT_MAX )
		(*visitor.coaccess_)[s] = true;
	if ((*visitor.dfnumber_)[s] == (*visitor.lowlink_)[s]) {  // root of new SCC
		bool scc_coaccess = false;
		size_t i = visitor.scc_stack_->size();
		int t;
		do {
			t = (*visitor.scc_stack_)[--i];
			if ((*visitor.coaccess_)[t])
				scc_coaccess = true;
		} while (s != t);
		do {
			t = visitor.scc_stack_->back();
			if (visitor.scc_)
				(*visitor.scc_)[t] = visitor.nscc_;
			if (scc_coaccess)
				(*visitor.coaccess_)[t] = true;
			(*visitor.onstack_)[t] = false;
			visitor.scc_stack_->pop_back();
		} while (s != t);

		++visitor.nscc_;
	}
	if (p != -1) {
		if ((*visitor.coaccess_)[s])
			(*visitor.coaccess_)[p] = true;
		if ((*visitor.lowlink_)[s] < (*visitor.lowlink_)[p])
			(*visitor.lowlink_)[p] = (*visitor.lowlink_)[s];
	}

}
void SccVisitorCompactLatticeArcInitState(SccVisitorCompactLatticeArc &visitor, int s, int root)
{
	visitor.scc_stack_->push_back(s);
	while (visitor.dfnumber_->size() <= s) {
		if (visitor.scc_)
			visitor.scc_->push_back(-1);
		if (visitor.access_)
			visitor.access_->push_back(false);
		visitor.coaccess_->push_back(false);
		visitor.dfnumber_->push_back(-1);
		visitor.lowlink_->push_back(-1);
		visitor.onstack_->push_back(false);
	}
	(*visitor.dfnumber_)[s] = visitor.nstates_;
	(*visitor.lowlink_)[s] = visitor.nstates_;
	(*visitor.onstack_)[s] = true;
	if (root == visitor.start_) {
		if (visitor.access_)
			(*visitor.access_)[s] = true;
	}
	else {
		if (visitor.access_)
			(*visitor.access_)[s] = false;
	}
	++visitor.nstates_;
}
bool SccVisitorCompactLatticeArcBackArc(SccVisitorCompactLatticeArc &visitor, CompactLatticeArc arc, int s)
{
	int t = arc.nextstate;
	if ((*visitor.dfnumber_)[t] < (*visitor.lowlink_)[s])
		(*visitor.lowlink_)[s] = (*visitor.dfnumber_)[t];
	if ((*visitor.coaccess_)[t])
		(*visitor.coaccess_)[s] = true;
	return true;
}

bool SccVisitorCompactLatticeArcForwardOrCrossArc(SccVisitorCompactLatticeArc &visitor, CompactLatticeArc arc, int s)
{
	int t = arc.nextstate;
	if ((*visitor.dfnumber_)[t] < (*visitor.dfnumber_)[s] &&
		(*visitor.onstack_)[t] && (*visitor.dfnumber_)[t] < (*visitor.lowlink_)[s])
		(*visitor.lowlink_)[s] = (*visitor.dfnumber_)[t];
	if ((*visitor.coaccess_)[t])
		(*visitor.coaccess_)[s] = true;
	return true;
}
//把states按深度优先排序出来存到order里
void DfsVisit(VectorFstOfLatticeArc &fst, vector<int> &order_)
{
	int start = fst.start_;
	if (fst.start_ == -1) {
		return;
	}
	vector<char> state_color;                	// Fst state DFS status
	stack<DfsStateLatticeArc*> state_stack;     // DFS execution stack
	MemoryPoolOfDfsStateLatticeArc state_pool;
	int nstates = start + 1;             // # of known states in general case
	bool expanded = false;
	fst.states_.size();
	nstates = fst.states_.size();
	expanded = true;
	state_color.resize(nstates, 0);
	bool dfs = true;
	for (int root = start; dfs && root < nstates;) {
		state_color[root] = 1;
		DfsStateLatticeArc *r = new DfsStateLatticeArc();
		r->state_id = root;
		r->i = 0;
		state_stack.push(r);
		order_.push_back(root);
		while (!state_stack.empty()) {
			DfsStateLatticeArc *dfs_state = state_stack.top(); 
			int s = dfs_state->state_id;
			if (s >= state_color.size()) {
				nstates = s + 1;
				state_color.resize(nstates, 1);
			}
			VectorStateOfLatticeArc *aiter = fst.states_[s];
			if (!dfs || dfs_state->i >= aiter->arcs_.size()) {
				state_color[s] = 2;// kDfsBlack;
				delete(state_stack.top());
				state_stack.pop();
				if (!state_stack.empty()) {
					DfsStateLatticeArc *parent_state = state_stack.top();
					//int p = parent_state->state_id;	// not used
					parent_state->i++;
				}
				else {
				}
				continue;
				}
			const LatticeArc &arc = aiter->arcs_[dfs_state->i];
				if (arc.nextstate >= state_color.size()) {
					nstates = arc.nextstate + 1;
					state_color.resize(nstates, 1);//kDfsWhite
				}

				int next_color = state_color[arc.nextstate];
				switch (next_color) 
				{
					default:
					case 0:
					{
						if (!dfs) break;
						state_color[arc.nextstate] = 1;
						DfsStateLatticeArc *r = new DfsStateLatticeArc();
						r->state_id = arc.nextstate;
						r->i = 0;
						state_stack.push(r);
						dfs = true;
						order_.push_back(arc.nextstate);
						break;
					}
					case 1:
					{
						dfs_state->i++;
						break;
					}
					case 2:
					{
						dfs_state->i++;
						break;
					}
				}
			}//while
		// Find next tree root
		for (root = root == start ? 0 : root + 1;
			root < nstates && state_color[root] != 0;// kDfsWhite;
			++root) {
		}
		if (!expanded && root == nstates) {
		}

	}
}

void FinishState(TopOrderVisitor &visitor, int state)
{
	visitor.finish_->push_back(state);
}
bool BackArc(TopOrderVisitor &visitor)
{
	visitor.acyclic_ = NULL;//false;
	return true;
}
void DfsVisit2(VectorFstOfLatticeArc &fst, TopOrderVisitor *visitor)
{
	int start = fst.start_;
	if (fst.start_ == -1)//kNoStateId
	{
		//visitor->FinishVisit();
		return;
	}
	vector<char> state_color;                	// Fst state DFS status
	stack<DfsStateLatticeArc*> state_stack;     // DFS execution stack
	MemoryPoolOfDfsStateLatticeArc state_pool;

	int nstates = start + 1;             // # of known states in general case
	bool expanded = false;
	fst.states_.size();
	nstates = fst.states_.size();
	expanded = true;
	state_color.resize(nstates, 0);//kDfsWhite
	bool dfs = true;
	for (int root = start; dfs && root < nstates;) {
		state_color[root] = 1;// kDfsGrey;
		DfsStateLatticeArc *r = new DfsStateLatticeArc();
		r->state_id = root;
		r->i = 0;
		state_stack.push(r);

		while (!state_stack.empty()) {
			DfsStateLatticeArc *dfs_state = state_stack.top();
			int s = dfs_state->state_id;
			if (s >= state_color.size()) {
				nstates = s + 1;
				state_color.resize(nstates, 1);
			}
			VectorStateOfLatticeArc *aiter = fst.states_[s];
			if (!dfs || dfs_state->i >= aiter->arcs_.size()) {
				state_color[s] = 2;// kDfsBlack;
				delete(state_stack.top());
				state_stack.pop();
				if (!state_stack.empty()) {
					DfsStateLatticeArc *parent_state = state_stack.top();
					//int p = parent_state->state_id;	// not used
					visitor->finish_->push_back(s);
					parent_state->i++;
				}
				else {
					visitor->finish_->push_back(s);
				}
				continue;
			}
			const LatticeArc &arc = aiter->arcs_[dfs_state->i];
			if (arc.nextstate >= state_color.size()) {
				nstates = arc.nextstate + 1;
				state_color.resize(nstates, 1);//kDfsWhite
			}
			/*if (!filter(arc)) {
			aiter.Next();
			continue;
			}*/
			int next_color = state_color[arc.nextstate];
			switch (next_color) {
			default:
			case 0:
			{
				if (!dfs) break;
				state_color[arc.nextstate] = 1;
				DfsStateLatticeArc *r = new DfsStateLatticeArc();
				r->state_id = arc.nextstate;
				r->i = 0;
				state_stack.push(r);
				dfs = true;
				break;
			}
			case 1:
			{
				*visitor->acyclic_ = false;
				dfs_state->i++;
				break;
			}
			case 2:
			{
				dfs_state->i++;
				break;
			}
			}
		}//while

		// Find next tree root
		for (root = root == start ? 0 : root + 1;
			root < nstates && state_color[root] != 0;// kDfsWhite;
			++root) {
		}
		if (!expanded && root == nstates) {
		}

	}
	if (*visitor->acyclic_)
	{
		visitor->order_->clear();
		for (uint s = 0; s < visitor->finish_->size(); ++s)
			visitor->order_->push_back(-1);
		for (uint s = 0; s < visitor->finish_->size(); ++s)
		{
			(*visitor->order_)[(*visitor->finish_)[visitor->finish_->size() - s - 1]] = s;
		}
			
	}
	delete visitor->finish_;
}
void DfsVisit3ForCompactLattice(VectorFstOfCompactLatticeArc &fst, SccVisitorCompactLatticeArc *visitor)
{
	SccVisitorCompactLatticeArcInitVisit(fst, *visitor);
	int start = fst.start_;
	if (fst.start_ == -1)//kNoStateId
	{
		return;
	}
	vector<char> state_color;                // Fst state DFS status
	stack<DfsStateLatticeArc*> state_stack;      // DFS execution stack
	MemoryPoolOfDfsStateLatticeArc state_pool;
	
	int nstates = start + 1;             // # of known states in general case
	bool expanded = false;
	fst.states_.size();
	nstates = fst.states_.size();
	expanded = true;
	state_color.resize(nstates, 0);//kDfsWhite
	bool dfs = true;
	
	for (int root = start; dfs && root < nstates;) {
		state_color[root] = 1;// kDfsGrey;
		DfsStateLatticeArc *r = new DfsStateLatticeArc();
		r->state_id = root;
		r->i = 0;
		state_stack.push(r);
		SccVisitorCompactLatticeArcInitState(*visitor, root, root);

		while (!state_stack.empty()) {
			DfsStateLatticeArc *dfs_state = state_stack.top();
			int s = dfs_state->state_id;
			if (s >= state_color.size()) {
				nstates = s + 1;
				state_color.resize(nstates, 1);
			}
			VectorStateOfCompactLatticeArc *aiter = fst.states_[s];
			if (!dfs || dfs_state->i >= aiter->arcs_.size()) {
				state_color[s] = 2;// kDfsBlack;
				delete(state_stack.top());
				state_stack.pop();
				if (!state_stack.empty()) {
					DfsStateLatticeArc *parent_state = state_stack.top();
					int p = parent_state->state_id;
					SccVisitorCompactLatticeArcFinishState(fst, *visitor, s, p);
					parent_state->i++;
				}
				else {
					SccVisitorCompactLatticeArcFinishState(fst, *visitor, s, -1);
				}
				continue;
			}
			const CompactLatticeArc &arc = aiter->arcs_[dfs_state->i];
			if (arc.nextstate >= state_color.size()) {
				nstates = arc.nextstate + 1;
				state_color.resize(nstates, 1);//kDfsWhite
			}
			int next_color = state_color[arc.nextstate];
			switch (next_color) {
				default:
				case 0: {
					if (!dfs)
						break;
					state_color[arc.nextstate] = 1;
					DfsStateLatticeArc *r = new DfsStateLatticeArc();
					r->state_id = arc.nextstate;
					r->i = 0;
					state_stack.push(r);
					SccVisitorCompactLatticeArcInitState(*visitor, arc.nextstate, root);
					break;
				}
				case 1: {
					dfs = SccVisitorCompactLatticeArcBackArc(*visitor, arc, s);
					dfs_state->i++;
					break;
				}
				case 2: {
					dfs = SccVisitorCompactLatticeArcForwardOrCrossArc(*visitor, arc, s);
					dfs_state->i++;
					break;
				}
			}
		}//while

		// Find next tree root
		for (root = root == start ? 0 : root + 1;
			root < nstates && state_color[root] != 0;// kDfsWhite;
			++root) {
		}
		if (!expanded && root == nstates) {
		}
	}
	if (visitor->scc_)
		for (uint i = 0; i < visitor->scc_->size(); ++i)
			(*visitor->scc_)[i] = visitor->nscc_ - 1 - (*visitor->scc_)[i];
	if (visitor->coaccess_internal_)
		delete visitor->coaccess_;
	delete visitor->dfnumber_;
	delete visitor->lowlink_;
	delete visitor->onstack_;
	delete visitor->scc_stack_;
}
void DeleteStates(VectorFstOfCompactLatticeArc &fst, vector<int> dstates)
{
	vector<int> newid(fst.states_.size(), 0);
	for (size_t i = 0; i < dstates.size(); ++i)
		newid[dstates[i]] = -1;
	int nstates = 0;
	for (uint s = 0; s < fst.states_.size(); ++s) {
		if (newid[s] != -1) {
			newid[s] = nstates;
			if (s != nstates)
				fst.states_[nstates] = fst.states_[s];
			++nstates;
		}
		else {
		}
	}
	fst.states_.resize(nstates);
	for (uint s = 0; s < fst.states_.size(); ++s) {
		CompactLatticeArc *arcs = fst.states_[s]->arcs_.data();
		size_t narcs = 0;
		size_t nieps = fst.states_[s]->niepsilons_;
		size_t noeps = fst.states_[s]->noepsilons_;
		for (size_t i = 0; i < fst.states_[s]->arcs_.size(); ++i) {
			int t = newid[arcs[i].nextstate];
			if (t != -1) {
				arcs[i].nextstate = t;
				if (i != narcs)
					arcs[narcs] = arcs[i];
				++narcs;
			}
			else {
				if (arcs[i].ilabel == 0)
					--nieps;
				if (arcs[i].olabel == 0)
					--noeps;
			}
		}

		int n = fst.states_[s]->arcs_.size()-narcs;
		for (int size = 0; size < n; size++) {
			fst.states_[s]->arcs_.pop_back();
		}
		fst.states_[s]->niepsilons_ = nieps;
		fst.states_[s]->noepsilons_=noeps;
	}

	if (fst.start_ != -1)
		fst.start_ = newid[fst.start_];
}
void ConnectForCompactLattice(VectorFstOfCompactLatticeArc *fst)
{
	vector<bool> access;
	vector<bool> coaccess;
	eesen::uint64 props = 0;
	SccVisitorCompactLatticeArc scc_visitor;
	scc_visitor.scc_= 0;
	scc_visitor.access_ =&access;
	scc_visitor.coaccess_ = &coaccess;
	scc_visitor.props_ = &props;
	DfsVisit3ForCompactLattice(*fst, &scc_visitor);
	vector<int> dstates;
	for (uint s = 0; s < access.size(); ++s) {
		if (!access[s] || !coaccess[s])
			dstates.push_back(s);
	}
	DeleteStates(*fst, dstates);
}
