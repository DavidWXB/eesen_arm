#ifndef SCCVISITORLATTICEARC_H_
#define SCCVISITORLATTICEARC_H_

#include <list>
#include <vector>
#include "../wfst/VectorFstOfLatticeArc.h"

struct SccVisitorLatticeArc
{
	vector<int> *scc_;         // State's scc number
	vector<bool> *access_;     // State's accessibility
	vector<bool> *coaccess_;   // State's coaccessibility
	eesen::uint64 *props_;
	const VectorFstOfLatticeArc *fst_;
	int start_;
	int nstates_;             // State count
	int nscc_;                // SCC count
	bool coaccess_internal_;
	vector<int> *dfnumber_;   // state discovery times
	vector<int> *lowlink_;    // lowlink[s] == dfnumber[s] => SCC root
	vector<bool> *onstack_;   // is a state on the SCC stack
	vector<int> *scc_stack_;  // SCC stack (w/ random access)
};

void ConnectForLattice(VectorFstOfLatticeArc *fst);

#endif
