#ifndef _LSTM_LAYER_C
#define _LSTM_LAYER_C

#include "parameter_list.h"

void lstm_layer(double W_ix[cell][cell * 2], double W_ih[cell][cell], double W_ic[cell],
	double W_fx[cell][cell * 2], double W_fh[cell][cell], double W_fc[cell], double W_cx[cell][cell * 2],  
	double W_ch[cell][cell], double W_ox[cell][cell * 2], double W_oh[cell][cell], double W_oc[cell],
	double h_pre[cell], double c_pre[cell], double h_cur[cell], double c_cur[cell], 
	double b_i[cell], double b_f[cell], double b_c[cell], double b_o[cell], 
	double x[cell * 2], boolUse input_layer, double *max);

#endif
