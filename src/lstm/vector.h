#ifndef VECTOR_H_H_
#define VECTOR_H_H_

void vector_dot(double a[], double b[], double sum[]);
void vector_add(double a[], double b[], double c[], double d[], double sum[]);
void vector_add_2(double a[], double b[],  double sum[]);
void vector_sigmoid(double a[]);
void vector_tanh(double a[]);
void vector_concat(double a[], double b[], double c[]);
void vector_add_2(double a[], double b[], double sum[]);

#endif
