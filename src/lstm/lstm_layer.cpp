#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "vector.h"
#include "matrix.h"
#include "parameter_list.h"

void lstm_layer(double W_ix[][cell * 2], double W_ih[cell][cell], double W_ic[cell], 
	double W_fx[cell][cell * 2], double W_fh[cell][cell], double W_fc[cell], double W_cx[cell][cell * 2],  
	double W_ch[cell][cell], double W_ox[cell][cell * 2], double W_oh[cell][cell], double W_oc[cell],
	double h_pre[cell], double c_pre[cell], double h_cur[cell], double c_cur[cell], 
	double b_i[cell], double b_f[cell], double b_c[cell], double b_o[cell], 
	double x[cell * 2], boolUse input_layer, double *max) {

		// 存储三种门的结果
		double i[cell] = {0};
		double f[cell] = {0};
		double o[cell] = {0};

		// 每一层与输入是120或640维
		int x_dim = input_layer ? fbank : cell * 2;

		// 输出 暂存矩阵乘的结果
		double tmp_x[cell] = {0};
		double tmp_h[cell] = {0};
		double tmp_c[cell] = {0};

		memset(tmp_x, 0, cell * sizeof(double));
		memset(tmp_h, 0, cell * sizeof(double));
		memset(tmp_c, 0, cell * sizeof(double));

		matrix_multiply_x(W_ix, x, tmp_x, x_dim);
		matrix_multiply(W_ih, h_pre, tmp_h, cell);

		vector_dot(W_ic, c_pre, tmp_c);
		vector_add(tmp_x, tmp_h, tmp_c, b_i, i);
		vector_sigmoid(i);	// re-implement

		// 最好按eesene源码方式实现
		memset(tmp_x, 0, cell * sizeof(double));
		memset(tmp_h, 0, cell * sizeof(double));
		memset(tmp_c, 0, cell * sizeof(double));

		matrix_multiply_x(W_fx, x, tmp_x, x_dim);
		matrix_multiply(W_fh, h_pre, tmp_h, cell);

		vector_dot(W_fc, c_pre, tmp_c);
		vector_add(tmp_x, tmp_h, tmp_c, b_f, f);
		vector_sigmoid(f);	// re-implement

		// c 输出结果c
		// 多次调用tanh函数浪费时间
		// 顺序问题？是否位于输出门之前, 答 是的
		memset(tmp_x, 0, cell * sizeof(double));
		memset(tmp_h, 0, cell * sizeof(double));
		memset(tmp_c, 0, cell * sizeof(double));

		matrix_multiply_x(W_cx, x, tmp_x, x_dim);
		matrix_multiply(W_ch, h_pre, tmp_h, cell);

		memset(tmp_c, 0, cell * sizeof(double));
		vector_add(tmp_x, tmp_h, tmp_c, b_c, tmp_x);
		vector_tanh(tmp_x);	// re-implement

		vector_dot(i, tmp_x, tmp_x);
		vector_dot(f, c_pre, tmp_c);

		memset(tmp_h, 0, cell * sizeof(double));
		vector_add(tmp_x, tmp_h, tmp_c, tmp_h, c_cur);

		// o
		memset(tmp_x, 0, cell * sizeof(double));
		memset(tmp_h, 0, cell * sizeof(double));
		memset(tmp_c, 0, cell * sizeof(double));

		matrix_multiply_x(W_ox, x, tmp_x, x_dim);
		matrix_multiply(W_oh, h_pre, tmp_h, cell);

		// 特殊与当前时刻有关
		vector_dot(W_oc, c_cur, tmp_c);
		vector_add(tmp_x, tmp_h, tmp_c, b_o, o);
		vector_sigmoid(o);	// re-implement

		// h 输出结果h
		memset(tmp_x, 0, cell * sizeof(double));
		memset(tmp_h, 0, cell * sizeof(double));
		memset(tmp_c, 0, cell * sizeof(double));

		memcpy(tmp_c, c_cur, sizeof(double) * cell);
		for (int n = 0; n < cell; n++)	// new added
			if (abs(tmp_c[n]) >= 500)
				max++;

		vector_tanh(tmp_c);				// re-implement
		vector_dot(o, tmp_c, h_cur);
}
