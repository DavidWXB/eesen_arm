#ifndef _MATRIX_H
#define _MATRIX_H

#include "parameter_list.h"

void matrix_multiply(double a[][cell], double b[], double sum[], int matrix_col);
void matrix_multiply_x(double a[][cell * 2], double b[], double sum[], int matrix_col);

#endif
