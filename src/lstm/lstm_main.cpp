/************************************************************************/
/*
   v0.3 修正参数列表的元素类型，添加readfuns.h
   v0.4 修改网络结构
   v0.5 所有的float改成double,进行debug，数据匹配一致
   v0.6 以e为底，数据与服务器结果基本匹配，差异可能是 由 float与double类型引起的
          注意：修改完函数定义后，如果遇到很奇怪的提示，声明也需要相应作了修改
*/
/************************************************************************/

#include <math.h>
#include <stdio.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#include "vector.h"
#include "lstm_main.h"
#include "lstm_layer.h"
#include "parameter_list.h"
#include "externglobalvaris.h"
#include "../utils/matrix.h"

#define ALIGN 	   0x8
#define ALIGN_MASK 0xFFFFFFF8

// global variable declare
extern eesen::Matrix<eesen::BaseFloat> fbank_output;
extern double features_0[TT][cell * 2];

// global variable definition
double prior[phone];
double total_prob = 0;

typedef struct cell_1 {
	double arr[cell];
} cell_1;

typedef struct cell2 {
	double arr[cell*2];
} cell_2;

void read_label_counts_prior_arm()
{
	long label_cnt[phone] = {
			918406,6792,3199,4276,4796,360,18315,8363,9414,23738,359,8861,4498,2197,
			2263,491,9401,4375,14643,2155,15,3811,1593,3071,10193,9,5515,9661,1168,3765,
			230,19443,9283,51934,73,936,2651,169,2081,75,7601,1207,4519,6807,15618,503,
			2352,8682,1842,27,11438,29294,11403,25906,26896,25471,46116,919,18801,10171,
			10171,21832,1,10305,7500,20156,10773,5243,13694,597,16633,8866,16707,17002,
			284,1,1468,432,5406,1374,184,2710,17303,2620,1112,29,1044,60,7295,28478,258,
			9758,3351,1793,1632,4357,8,11436,8185,7664,18655,367,25795,14838,21600,6556
	};

	long sum =  0;
	for (int i = 0; i < phone; i++) {
		sum += label_cnt[i];
	}

	for (int i = 0; i < phone; i++) {
		prior[i] = ((double)label_cnt[i] / (double)sum);
	}

	for (int i = 0; i < phone; i++) {
		prior[i] = log(prior[i]);
	}
}

int LstmMain(Matrix_Float *mat) {
	int time;
	int i, j;
	double max = 0.0;

	double out[phone];
	double prob[phone];

	// get info from fbank
	const int frames = fbank_output.NumRows();
	cell_2 *feats2lstm  = new cell_2[frames];

	for(i = 0; i < frames; i++)
		for(j = 0; j < 2*cell; j++)
			feats2lstm[i].arr[j] = (j < fbank_output.NumCols()) ? fbank_output.Data()[i*fbank_output.Stride()+j]
			                                                    : 0.0;
			/*
			feats2lstm[i].arr[j] = features_0[i][j];
			*/

	fbank_output.~Matrix();

	// memory pool, save heap space
	cell_1 *h_fp         = new cell_1[frames+2];
	cell_1 *c_fp         = new cell_1[frames+2];
	cell_1 *h_bp         = new cell_1[frames+2];
	cell_1 *c_bp         = new cell_1[frames+2];
	cell_2 *concat_fp_bp = new cell_2[frames+2];
	memset(h_fp,         0, (frames + 2) * cell * sizeof(double));
	memset(h_bp,         0, (frames + 2) * cell * sizeof(double));
	memset(c_fp,         0, (frames + 2) * cell * sizeof(double));
	memset(c_bp,         0, (frames + 2) * cell * sizeof(double));
	memset(concat_fp_bp, 0, (frames + 2) * cell * 2 * sizeof(double));

	// not read from file, cmp done
	read_label_counts_prior_arm();

	// layer1前向
	printf("lstm layer1 forward...\n");
	for (time = 1; time <= frames; time++) {
		lstm_layer(W_ix_1, W_ih_1, W_ic_1,
			       W_fx_1, W_fh_1, W_fc_1,
			       W_cx_1, W_ch_1,
			       W_ox_1, W_oh_1, W_oc_1,
			       h_fp[time - 1].arr, c_fp[time - 1].arr, h_fp[time].arr, c_fp[time].arr,
			       b_i_1, b_f_1, b_c_1, b_o_1,
			       feats2lstm[time-1].arr/*features_1[time-1]*/, true, &max);
	}

	// layer1后向
	printf("lstm layer1 backward...\n");
	for (time = frames; time >= 1; time--) {
		lstm_layer(W_ix_1_r, W_ih_1_r, W_ic_1_r,
                   W_fx_1_r, W_fh_1_r, W_fc_1_r,
                   W_cx_1_r, W_ch_1_r,
                   W_ox_1_r, W_oh_1_r, W_oc_1_r,
			       // 前两个是输入，前一时刻的结果 后面的是当前的输出，暂存
			       h_bp[time + 1].arr, c_bp[time + 1].arr, h_bp[time].arr, c_bp[time].arr,
			       b_i_1_r, b_f_1_r, b_c_1_r, b_o_1_r,
			       feats2lstm[time-1].arr/*features_1[time-1]*/, true, &max);
	}

	for (time = 0; time <= frames + 1; time++) {
		vector_concat(h_fp[time].arr, h_bp[time].arr, concat_fp_bp[time].arr);
	}


	// layer2 前向
	printf("lstm layer2 forward...\n");
	for (time = 1; time <= frames; time++) {
	    lstm_layer(W_ix_2, W_ih_2, W_ic_2,
                   W_fx_2, W_fh_2, W_fc_2,
                   W_cx_2, W_ch_2,
                   W_ox_2, W_oh_2, W_oc_2,
	               h_fp[time - 1].arr, c_fp[time - 1].arr, h_fp[time].arr, c_fp[time].arr,
	               b_i_2, b_f_2, b_c_2, b_o_2,
                   concat_fp_bp[time].arr, false, &max);
	}

	// layer2 后向
	printf("lstm layer2 backward...\n");
	for (time = frames; time >= 1; time--) {
		lstm_layer(W_ix_2_r, W_ih_2_r, W_ic_2_r,
                   W_fx_2_r, W_fh_2_r, W_fc_2_r,
                   W_cx_2_r, W_ch_2_r,
                   W_ox_2_r, W_oh_2_r, W_oc_2_r,
			       // 前两个是输入，前一时刻的结果 后面的是当前的输出，暂存 
			       h_bp[time + 1].arr, c_bp[time + 1].arr, h_bp[time].arr, c_bp[time].arr,
			       b_i_2_r, b_f_2_r, b_c_2_r, b_o_2_r,
                   concat_fp_bp[time].arr, false, &max);
	}

	for (time = 0; time <= frames + 1; time++) {
		vector_concat(h_fp[time].arr, h_bp[time].arr, concat_fp_bp[time].arr);
	}

	/*
	memset(h_fp, 0, (frames + 2) * cell * sizeof(double));
	memset(h_bp, 0, (frames + 2) * cell * sizeof(double));
	memset(c_fp, 0, (frames + 2) * cell * sizeof(double));
	memset(c_bp, 0, (frames + 2) * cell * sizeof(double));

	// layer3
	printf("lstm layer3 forward...\n");
	for (time = 1; time <= frames; time++) {
	    lstm_layer(W_ix_3, W_ih_3, W_ic_3,
                   W_fx_3, W_fh_3, W_fc_3, 
                   W_cx_3, W_ch_3,
                   W_ox_3, W_oh_3, W_oc_3,
	               h_fp[time - 1].arr, c_fp[time - 1].arr, h_fp[time].arr, c_fp[time].arr,
	               b_i_3, b_f_3, b_c_3, b_o_3,
                   concat_fp_bp[time].arr, false, &max);
	}

	printf("lstm layer3 backward...\n");
	for (time = frames; time >= 1; time--) {
		lstm_layer(W_ix_3_r, W_ih_3_r, W_ic_3_r, 
                   W_fx_3_r, W_fh_3_r, W_fc_3_r,
                   W_cx_3_r, W_ch_3_r,
                   W_ox_3_r, W_oh_3_r, W_oc_3_r,
			       // 前两个是输入，前一时刻的结果 后面的是当前的输出，暂存
			       h_bp[time + 1].arr, c_bp[time + 1].arr, h_bp[time].arr, c_bp[time].arr,
			       b_i_3_r, b_f_3_r, b_c_3_r, b_o_3_r,
                   concat_fp_bp[time].arr, false, &max);
	}
	memset(concat_fp_bp, 0, (frames + 2) * cell * 2* sizeof(double));

	for (time = 0; time <= frames + 1; time++) {
		vector_concat(h_fp[time].arr, h_bp[time].arr, concat_fp_bp[time].arr);
	}


	// layer4
	printf("lstm layer4 forward...\n");
	for (time = 1; time <= frames; time++) {
		lstm_layer(W_ix_4, W_ih_4, W_ic_4,
                   W_fx_4, W_fh_4, W_fc_4,
                   W_cx_4, W_ch_4,
                   W_ox_4, W_oh_4, W_oc_4,
			       h_fp[time - 1].arr, c_fp[time - 1].arr, h_fp[time].arr, c_fp[time].arr,
			       b_i_4, b_f_4, b_c_4, b_o_4,
                   concat_fp_bp[time].arr, false, &max);
	}

	printf("lstm layer4 backward...\n");
	for (time = frames; time >= 1; time--) {
		lstm_layer(W_ix_4_r, W_ih_4_r, W_ic_4_r,
                   W_fx_4_r, W_fh_4_r, W_fc_4_r,
                   W_cx_4_r, W_ch_4_r,
                   W_ox_4_r, W_oh_4_r, W_oc_4_r,
			       // 前两个是输入，前一时刻的结果 后面的是当前的输出，暂存
			       h_bp[time + 1].arr, c_bp[time + 1].arr, h_bp[time].arr, c_bp[time].arr,
			       b_i_4_r, b_f_4_r, b_c_4_r, b_o_4_r,
                   concat_fp_bp[time].arr, false, &max);
	}

	for (time = 0; time <= frames + 1; time++) {
		vector_concat(h_fp[time].arr, h_bp[time].arr, concat_fp_bp[time].arr);
	}
	*/

	mat->data_ = (float *)malloc(frames*phone*sizeof(float));
	mat->num_cols_ = phone;
	mat->num_rows_ = frames;
	mat->stride_   = phone;

	// 输出层
	printf("lstm layer output...\n");
	for (time = 1; time <= frames; time++) {
		for ( i = 0; i < phone; i++) {
			// attention need to reset out[] as 0
			out[i] = 0;
			for ( j = 0; j < cell * 2; j++) {
				out[i] += W[i][j] * concat_fp_bp[time].arr[j];
			}
			out[i] += b_aff[i];
		}

		// softmax
		total_prob = 0;
		for ( i = 0; i < phone; i ++) {
			prob[i] = exp(out[i]);
			total_prob += prob[i];
		}
		for ( i = 0; i < phone; i ++) {
			prob[i] = prob[i] / total_prob;
		}

		// 以e为底取对数
		for (i = 0; i < phone; i ++) {
			prob[i] = log(prob[i]);
		}

		for (i = 0; i < phone; i ++) {
			prob[i] = prob[i] - prior[i];
			*(mat->data_ + (time - 1)*mat->stride_ + i) = (float)prob[i];
		}
	}

	// release memory
	delete [] h_fp;
	delete [] c_fp;
	delete [] h_bp;
	delete [] c_bp;
	delete [] concat_fp_bp;

	return 0;
}
