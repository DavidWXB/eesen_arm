#ifndef _GLOBALVARIS_H
#define _GLOBALVARIS_H

#include "parameter_list.h"

//features
extern double features_1[TT][cell * 2];

//layer_1
extern double W_ix_1[cell][cell * 2];   //W_ix是输入门和输入向量之间权重矩阵（i术入门，x最初的输入向量） 320*320
extern double W_ih_1[cell][cell];       //输入门和隐含层之间的权重  320 320
extern double W_ic_1[cell];             //输入门和Cell
extern double W_fx_1[cell][cell * 2];   //遗忘门和输入向量
extern double W_fh_1[cell][cell];       //遗忘门和隐含层
extern double W_fc_1[cell];             //遗忘门和Cell
extern double W_cx_1[cell][cell * 2];   //Cell和输入向量
extern double W_ch_1[cell][cell];       //Cell和隐含层
extern double W_ox_1[cell][cell * 2];   //输出门和输入向量
extern double W_oh_1[cell][cell];       //输出门和隐含层
extern double W_oc_1[cell];             //输出门和cell
extern double b_i_1[cell];              //输入门的偏置
extern double b_f_1[cell];              //遗忘门的偏置
extern double b_c_1[cell];              //Cell的偏置
extern double b_o_1[cell];              //输出门的偏置

//layer_2
extern double W_ix_2[cell][cell * 2];
extern double W_ih_2[cell][cell];
extern double W_ic_2[cell];
extern double W_fx_2[cell][cell * 2];
extern double W_fh_2[cell][cell];
extern double W_fc_2[cell];
extern double W_cx_2[cell][cell * 2];
extern double W_ch_2[cell][cell];	
extern double W_ox_2[cell][cell * 2];
extern double W_oh_2[cell][cell];
extern double W_oc_2[cell];
extern double b_i_2[cell];
extern double b_f_2[cell];
extern double b_c_2[cell];
extern double b_o_2[cell];

//layer_3
extern double W_ix_3[cell][cell * 2];
extern double W_ih_3[cell][cell];
extern double W_ic_3[cell];
extern double W_fx_3[cell][cell * 2];
extern double W_fh_3[cell][cell];
extern double W_fc_3[cell] ;
extern double W_cx_3[cell][cell * 2];
extern double W_ch_3[cell][cell];
extern double W_ox_3[cell][cell * 2];
extern double W_oh_3[cell][cell];
extern double W_oc_3[cell];
extern double b_i_3[cell];
extern double b_f_3[cell];
extern double b_c_3[cell];
extern double b_o_3[cell];

//layer_4
extern double W_ix_4[cell][cell * 2];
extern double W_ih_4[cell][cell];
extern double W_ic_4[cell];
extern double W_fx_4[cell][cell * 2];
extern double W_fh_4[cell][cell];
extern double W_fc_4[cell];
extern double W_cx_4[cell][cell * 2];
extern double W_ch_4[cell][cell];
extern double W_ox_4[cell][cell * 2];
extern double W_oh_4[cell][cell];
extern double W_oc_4[cell];
extern double b_i_4[cell];
extern double b_f_4[cell];
extern double b_c_4[cell];
extern double b_o_4[cell];

//layer_1_return
extern double W_ix_1_r[cell][cell * 2];
extern double W_ih_1_r[cell][cell];
extern double W_ic_1_r[cell];
extern double W_fx_1_r[cell][cell * 2];
extern double W_fh_1_r[cell][cell];
extern double W_fc_1_r[cell];
extern double W_cx_1_r[cell][cell * 2];
extern double W_ch_1_r[cell][cell];
extern double W_ox_1_r[cell][cell * 2];
extern double W_oh_1_r[cell][cell];
extern double W_oc_1_r[cell];
extern double b_i_1_r[cell];
extern double b_f_1_r[cell];
extern double b_c_1_r[cell];
extern double b_o_1_r[cell];

//layer_2_return
extern double W_ix_2_r[cell][cell * 2];
extern double W_ih_2_r[cell][cell];
extern double W_ic_2_r[cell];
extern double W_fx_2_r[cell][cell * 2];
extern double W_fh_2_r[cell][cell];
extern double W_fc_2_r[cell];
extern double W_cx_2_r[cell][cell * 2];
extern double W_ch_2_r[cell][cell];
extern double W_ox_2_r[cell][cell * 2];
extern double W_oh_2_r[cell][cell];
extern double W_oc_2_r[cell];
extern double b_i_2_r[cell];
extern double b_f_2_r[cell];
extern double b_c_2_r[cell];
extern double b_o_2_r[cell];

//layer_3_r_return
extern double W_ix_3_r[cell][cell * 2];
extern double W_ih_3_r[cell][cell];
extern double W_ic_3_r[cell];
extern double W_fx_3_r[cell][cell * 2];
extern double W_fh_3_r[cell][cell];
extern double W_fc_3_r[cell];
extern double W_cx_3_r[cell][cell * 2];
extern double W_ch_3_r[cell][cell];
extern double W_ox_3_r[cell][cell * 2];
extern double W_oh_3_r[cell][cell];
extern double W_oc_3_r[cell];
extern double b_i_3_r[cell];
extern double b_f_3_r[cell];
extern double b_c_3_r[cell];
extern double b_o_3_r[cell];

//layer_4_return
extern double W_ix_4_r[cell][cell * 2];
extern double W_ih_4_r[cell][cell];
extern double W_ic_4_r[cell];
extern double W_fx_4_r[cell][cell * 2];
extern double W_fh_4_r[cell][cell];
extern double W_fc_4_r[cell];
extern double W_cx_4_r[cell][cell * 2];
extern double W_ch_4_r[cell][cell];
extern double W_ox_4_r[cell][cell * 2];
extern double W_oh_4_r[cell][cell];
extern double W_oc_4_r[cell];
extern double b_i_4_r[cell];
extern double b_f_4_r[cell];
extern double b_c_4_r[cell];
extern double b_o_4_r[cell];

//full
extern double W[phone][cell * 2];
extern double b_aff[phone];

#endif
