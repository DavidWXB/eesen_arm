#include <math.h>
#include <stdio.h>
#include "parameter_list.h"

void vector_dot(double a[], double b[], double sum[]) {
	for (int i = 0; i < cell; i ++) {
		sum[i] = a[i] * b[i];
	}
}

void vector_add(double a[], double b[], double c[], double d[], double sum[]) {
	for (int i = 0; i < cell; i ++) {
		sum[i] = a[i] + b[i] + c[i] + d[i];
	}
}

void vector_add_2(double a[], double b[], double sum[]) {
	for (int i = 0; i < cell; i ++) {
		sum[i] = a[i] + b[i] ;
	}
}

/*
double fp_sigmoid2(double x){
	double temp = x;
	double rst;
	if (x < 0) {
		temp = -x;
	}
	if (temp >= 5) {
		rst = 1;
	}
	else if (temp >= 3.5&& temp < 5) {
		rst = temp*0.015625 + 0.921875;
	}
	else if (temp >= 2.375 && temp < 3.5) {
		rst = temp*0.046875 + 0.8125;
	}
	else if (temp >= 1 && temp <2.375) {
		rst = temp*0.125 + 0.625;
	}
	else if (temp >= 0 && temp < 1) {
		rst = temp*0.25 + 0.5;
	}
	if (x < 0) {
		rst = 1 - rst;
	}
	return rst;
}
double fp_sigmoid(double x){
	double temp = x;
	double rst;
	if (x < 0) {
		temp = -x;
	}
	if (temp >= 7) {
		rst = 1;
	}
	else if (temp >= 6 && temp < 7) {
		rst = temp*0.00390625 + 0.96875;
	}
	else if (temp >=5 && temp < 6) {
		rst = temp*0.0078125 + 0.9453125;
	}
	else if (temp >= 4 && temp < 5) {
		rst = temp*0.015625+ 0.90625;
	}
	else if (temp >= 3&& temp <4) {
		rst = temp*0.03125 + 0.84375;
	}
	else if (temp >= 2.0 && temp < 3.0) {
		rst = temp*0.0625 + 0.75;
	}
	else if (temp >= 1 && temp <2.0) {
		rst = temp*0.125 + 0.625;
	}
	else if (temp >= 0 && temp < 1) {
		rst = temp*0.25 + 0.5;
	}
	if (x < 0) {
		rst = 1 - rst;
	}
	return rst;
}
*/
void vector_sigmoid(double a[]) {
	for (int i = 0; i < cell; i ++) {
		//a[i] = fp_sigmoid2(a[i]);
		a[i] = 1/(1+exp(-a[i]));
	}
}

/*
inline double fp_tanh(double x) {
	double rst;
	double d_x = 2 * x;
	double rst_sigmoid = 2*fp_sigmoid2(d_x);
	rst = rst_sigmoid - 1;
	return rst;
}
*/
void vector_tanh(double a[]) {
	for (int i = 0; i < cell; i ++) {
		//a[i] = fp_tanh(a[i]);
		if(a[i] > 500.0)
			a[i] = 1.0;
		else if(a[i] < -500.0)
			a[i] = -1.0;
		else
			a[i] = (exp(a[i]) - exp(-a[i])) / (exp(a[i]) + exp(-a[i]));
	}
}

// 向量拼接concatenation ,两个320维向量拼成一个640维向量
void vector_concat(double a[], double b[], double c[])
{
	for (int i = 0; i < (cell << 1); i++) {
		if (i < cell)
			c[i] = a[i];
		else
			c[i] = b[i - cell];
	}
}
