#ifndef LSTM_MAIN_H_
#define LSTM_MAIN_H_

#include "../decode/DecodableInterface.h"

int LstmMain(Matrix_Float *mat);
void read_label_counts_prior_arm();

#endif
