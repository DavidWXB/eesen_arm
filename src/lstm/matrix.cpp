#include <stdio.h>
#include "matrix.h"

// 矩阵乘
// 矩阵a[][]*一维向量b[]，得到一维向量sum[],权重矩阵在左侧，后面乘一维向量
// 320维
void matrix_multiply(double a[][cell], double b[], double sum[], int matrix_col) {
	for (int i = 0; i < cell; i ++) {
		for (int j = 0; j < matrix_col; j ++) {
			sum[i] += a[i][j] * b[j];
		}
	}
}
// 与输入有关的的矩阵乘，640维
void matrix_multiply_x(double a[][cell * 2], double b[], double sum[], int matrix_col) {
	for (int i = 0; i < cell; i ++) {
		for (int j = 0; j < matrix_col; j ++) {
			sum[i] += a[i][j] * b[j];
		}
	}
}
