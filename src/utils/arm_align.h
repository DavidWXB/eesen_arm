#ifndef ARM_ALIGN_H
#define ARM_ALIGN_H

#include <assert.h>
#include <stdlib.h>

inline void* arm_align_malloc(size_t size, size_t alignment)
{
	// check alignment is pow of 2
	assert(!(alignment & (alignment-1)));

	// calc. the max offset
	size_t offset = sizeof(void*) + (--alignment);

	// malloc
	char *p = (char*)(malloc(offset + size));
	if(!p)
		return NULL;
		
	// minus alignment
	void *r = (void*)((size_t)(p + offset) & (~alignment));
		
	((void**)r)[-1] = p;
	return (void*)r;
}

inline void arm_align_free(void *p)
{
	free(((void**)p)[-1]);
}

#endif
