#include <fstream>
#include "FSTFile.h"
#include "FstHeader.h"

using namespace std;

void ReadFstKaldi(VectorFstOfStdArc *fst, string rxfilename)
{
	ifstream is_;
	const bool binary = true;
	is_.open(rxfilename.c_str(), (binary ? std::ios_base::in | std::ios_base::binary : std::ios_base::in));

	if (!is_) {
		return;
	}

	FstHeader fstheader;
	ReadOfFstHeader(&fstheader, is_, rxfilename, false);

	ReadOfVectorFstOfStdArc(fst, is_, fstheader);
	is_.close();
}

void ReadFstKaldi(VectorFstOfStdArc *fst, unsigned char **wfst, unsigned int &idx)
{
	FstHeader fstheader;
	ReadOfFstHeader(&fstheader, wfst[0], idx);
	ReadOfVectorFstOfStdArc(fst, wfst, idx, fstheader);
}
