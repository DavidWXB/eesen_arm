#include <float.h>
#include <stdlib.h>
#include "VectorFstOfStdArc.h"

#define FST_SIZE 0x92BD0

void VectorFstOfStdArcDeleteStates(VectorFstOfStdArc* vectorFstImpl)
{
}

void VectorFstOfStdArcDeleteArcs(VectorFstOfStdArc* vectorFstImpl, int stateId)
{
}

void ReadOfVectorFstOfStdArc(VectorFstOfStdArc *fst, std::ifstream &is, FstHeader fstheader)
{
	fst->properties_ = fstheader.properties_;
	fst->start_      = fstheader.start_;
	fst->states_.reserve(fstheader.numstates_);

	for (int s = 0; s < fstheader.numstates_; s++) {
		TropicalWeight final;
		if (!is.read(reinterpret_cast<char *>(&(final.value_)), sizeof(float))) {
			break;
		}
		if (final.value_>FLT_MAX) {
			final.value_ = FLT_MAX;
		}

		VectorStateOfStdArc *vectorstate = new VectorStateOfStdArc();
		vectorstate->final_.value_ = FLT_MAX;
		fst->states_.push_back(vectorstate);

		VectorStateOfStdArc *state = fst->states_[s];
		state->niepsilons_ = 0;
		state->noepsilons_ = 0;
		state->final_ = final;

		eesen::int64 narcs;
		is.read(reinterpret_cast<char *>(&narcs), sizeof(long long));
		state->arcs_.reserve(narcs);

		for (size_t j = 0; j < narcs; j++) {
			StdArc arc;
			is.read(reinterpret_cast<char *>(&(arc.ilabel)), sizeof(int));
			is.read(reinterpret_cast<char *>(&(arc.olabel)), sizeof(int));
			is.read(reinterpret_cast<char *>(&(arc.weight.value_)), sizeof(float));
			is.read(reinterpret_cast<char *>(&(arc.nextstate)), sizeof(int));

			if (!is) {
				free(fst);
				return;
			}

			//AddArc
			if (arc.ilabel == 0) {
				++(state->niepsilons_);
			}
			if (arc.olabel == 0) {
				++(state->noepsilons_);
			}
			state->arcs_.push_back(arc);
		}
	}
	return;
}

void DestroyVectorFstOfStdArc(VectorFstOfStdArc *fst)
{
	for (uint s = 0; s < fst->states_.size(); ++s) {
		fst->states_[s]->arcs_.clear();
		delete fst->states_[s];
	}
	fst->states_.clear();
	fst->start_ = -1;
}

// added at 2016-01-04
void ReadOfVectorFstOfStdArc(VectorFstOfStdArc *fst, unsigned char **wfst, unsigned int &idx, FstHeader fstheader)
{
	fst->properties_ = fstheader.properties_;
	fst->start_      = fstheader.start_;
	fst->states_.reserve(fstheader.numstates_);

	int fst_idx, ele_idx;
	for(int s = 0; s < fstheader.numstates_; s++) {
		TropicalWeight final;
		final.value_ = 0;
		uint *ptr = (uint*)(&final.value_);
		for(int f = 3; f >= 0; f--) {
			fst_idx = (idx+f)/FST_SIZE;
			ele_idx = (idx+f)%FST_SIZE;
			*ptr = ((*ptr)<<8) | wfst[fst_idx][ele_idx];
		}
		idx += 4;
		if(final.value_ > FLT_MAX)
			final.value_ = FLT_MAX;

		VectorStateOfStdArc *vectorstate = new VectorStateOfStdArc();
		vectorstate->final_.value_ = FLT_MAX;
		fst->states_.push_back(vectorstate);

		VectorStateOfStdArc *state = fst->states_[s];
		state->niepsilons_ = 0;
		state->noepsilons_ = 0;
		state->final_ = final;

		eesen::int64 narcs = 0;
		for(int f = 7; f >= 0; f--) {
			fst_idx = (idx+f)/FST_SIZE;
			ele_idx = (idx+f)%FST_SIZE;
			narcs = (narcs<<8) | wfst[fst_idx][ele_idx];
		}
		idx += 8;
		state->arcs_.reserve(narcs);

		for(size_t j = 0; j < narcs; j++) {
			StdArc arc;
			for(int f = 3; f >= 0; f--) {
				fst_idx = (idx+f)/FST_SIZE;
				ele_idx = (idx+f)%FST_SIZE;
				arc.ilabel = (arc.ilabel<<8) | wfst[fst_idx][ele_idx];
			}
			idx += 4;

			for(int f = 3; f >= 0; f--) {
				fst_idx = (idx+f)/FST_SIZE;
				ele_idx = (idx+f)%FST_SIZE;
				arc.olabel = (arc.olabel<<8) | wfst[fst_idx][ele_idx];
			}
			idx += 4;

			int *weight = (int*)(&arc.weight.value_);
			for(int f = 3; f >= 0; f--) {
				fst_idx = (idx+f)/FST_SIZE;
				ele_idx = (idx+f)%FST_SIZE;
				(*weight) = ((*weight)<<8) | wfst[fst_idx][ele_idx];
			}
			idx += 4;

			for(int f = 3; f >= 0; f--) {
				fst_idx = (idx+f)/FST_SIZE;
				ele_idx = (idx+f)%FST_SIZE;
				arc.nextstate = (arc.nextstate<<8) | wfst[fst_idx][ele_idx];
			}
			idx += 4;

			// AddArc
			if(arc.ilabel == 0)
				++(state->niepsilons_);
			if(arc.olabel == 0)
				++(state->noepsilons_);
			state->arcs_.push_back(arc);
		}
	}
	return;
}
