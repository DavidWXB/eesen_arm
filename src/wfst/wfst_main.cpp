#include <string>
#include <vector>
#include <cstdlib>
#include "VectorFstOfStdArc.h"
#include "VectorFstOfLatticeArc.h"
#include "VectorFstOfCompactLatticeArc.h"
#include "FSTFile.h"
#include "wfst_main.h"
#include "../getpath/GetBestPathMain.h"
#include "../decode/DecodableInterface.h"
#include "../decode/LatticeFasterDecoder.h"
#include "../decode/LatticeFasterDecoderConfig.h"
#include "../test/test.h"

using namespace std;

// global variable declare
extern unsigned char fst0[];
extern unsigned char fst1[];
extern unsigned char fst2[];
extern unsigned char fst3[];
extern unsigned char fst4[];
extern unsigned char fst5[];
extern unsigned char fst6[];
extern unsigned char fst7[];
extern unsigned char fst8[];
extern unsigned char fst9[];
extern unsigned char fsta[];
extern unsigned char fstb[];
extern unsigned char fstc[];
extern unsigned char fstd[];
extern unsigned char fste[];
extern unsigned char fstf[];
extern const char *words[];
extern const char *words_cn[];

// global variable definition
VectorFstOfStdArc fst;
SymbolTable *word_syms;
LatticeFasterDecoderConfig config;
double acoustic_scale;
unsigned int g_idx_wfst = 0;

void wfstInit()
{
	unsigned char* wfst[] = {
			fst0, fst1, fst2, fst3, fst4, fst5, fst6, fst7,
			fst8, fst9, fsta, fstb, fstc, fstd, fste, fstf
	};

	printf("read tlg start...\n");
	ReadFstKaldi(&fst, wfst, g_idx_wfst);

	/*
    printf("start_ = %d, properties_ = %d\n", fst.start_, fst.properties_);
    for (uint a = 0; a < fst.states_.size(); a++) {
    	int *final = (int*)(&fst.states_[a]->final_.value_);
        printf("final_ = %d, niepsilons_ = %d, noepsilons_ = %d\n",
            *final,
            fst.states_[a]->niepsilons_,
            fst.states_[a]->noepsilons_);
        for (uint b = 0; b < fst.states_[a]->arcs_.size(); b++) {
        	int *weight = (int*)(&fst.states_[a]->arcs_[b].weight.value_);
            printf("%d, %d, %d, %d  ",
            fst.states_[a]->arcs_[b].ilabel,
            fst.states_[a]->arcs_[b].olabel,
            *weight,
            fst.states_[a]->arcs_[b].nextstate);
        }
        printf("\n");
    }
	*/

	printf("read symbol start...\n");
	word_syms = ReadTextOfSymbolTable(words_cn);

	/*
    for (uint i = 0; i < word_syms->symbols_.symbols_.size(); i++) {
        printf("%s\n\n", word_syms->symbols_.symbols_[i]);
    }
    */

	config.beam = 6.0;          // 17.0;
	config.max_active = 1000;   // 5000;
	config.min_active = 200;    // 200;
	config.lattice_beam = 8.0;
	config.prune_interval = 25;
	config.determinize_lattice = true;
	config.beam_delta = 0.5;
	config.hash_ratio = 2.0;
	config.prune_scale = 0.1;
	config.det_opts.delta   = 0.000976562500;
	config.det_opts.max_mem = 300000000;
	config.det_opts.phone_determinize = true;
	config.det_opts.word_determinize  = true;
	config.det_opts.minimize = false;

	acoustic_scale = 0.6;
}

void FstMain(Matrix_Float *mat, VectorFstOfStdArc *ifst, SymbolTable *word_syms, string str)
{
	printf("fst start...\n");
	LatticeFasterDecoder decode;
 	LatticeFasterDecoderInit(&decode, ifst, config);
	DecodableInterface decodable;
	DecodableInterfaceInit(&decodable, mat, acoustic_scale);

	printf("decode start...\n");
	Decode(&decode, &decodable);

	printf("get best path start...\n");
	bool re = ReachedFinal(&decode);
#ifdef OPT_FOR_GETBESTPATH
 	if (re)
        GetBestPathMainForToken(&decode, acoustic_scale, word_syms, str);
#else
	if (re)
        GetBestPathMain(&decode, acoustic_scale, word_syms, str);
#endif

	printf("eesen delete start...\n");
	EesenDelete(&decode, &decodable);
	printf("fst_main done\n");
}

void EesenMain(Matrix_Float *mat)
{
	string str;
	FstMain(mat, &fst, word_syms, str);
}

void EesenDelete(LatticeFasterDecoder *decode, DecodableInterface *decodable)
{
	printf("free data...\n");
	free(decodable->likes_.data_);
	decodable->likes_.num_cols_ = 0;
	decodable->likes_.num_rows_ = 0;
	decodable->likes_.stride_   = 0;

	printf("free token...\n");
	Elem_StateId_Token *elem = HashListClear(&decode->toks_);
	DeleteElems(&decode->toks_, elem);

	printf("free hash list...\n");
	DestroyHashList(&decode->toks_);

	printf("clear active tokens...\n");
	ClearActiveTokens(decode);

	//Ӳ������Ҫʵ��
	//printf("free fst...\n");
	//DestroyVectorFstOfStdArc(&fst);

	/* Note : word_syms pointer to global variable, can not be released. */
	//DestroySymbolTable(word_syms);
}
