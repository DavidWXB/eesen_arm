#ifndef FSTREADOPTIONS_H_
#define FSTREADOPTIONS_H_

#include <string>
#include "FstHeader.h"
#include "SymbolTable.h"

using namespace std;

//需要确认是否必须使用这个结构体
struct FstReadOptions
{
	enum FileReadMode {
		READ,
		MAP
	};

	string source;                	// Where you're reading from
	const FstHeader *header;      	// Pointer to Fst header. If non-zero, use
									// this info (don't read a stream header)
	const SymbolTable* isymbols;  	// Pointer to input symbols. If non-zero, use
									// this info (read and skip stream isymbols)
	const SymbolTable* osymbols;  	// Pointer to output symbols. If non-zero, use
									// this info (read and skip stream osymbols)
	FileReadMode mode;            	// Read or map files (advisory, if possible)
	bool read_isymbols;           	// Read isymbols, if any, default true
	bool read_osymbols;           	// Read osymbols, if any, default true
};

void FstReadOptionsInit(FstReadOptions* frOptions);

#endif
