#ifndef COMPACT_LATTICE_ARC_
#define COMPACT_LATTICE_ARC_

#include <vector>
#include "LatticeArc.h"
#include "../utils/kaldi-types.h"

struct CompactLatticeWeight
{
	LatticeWeight weight_;
	std::vector<eesen::int32> string_;
};

CompactLatticeWeight CompactLatticeWeightInit(float value1, float value2);

struct CompactLatticeArc
{
	int ilabel;
	int olabel;
	CompactLatticeWeight weight;
	int nextstate;
};

#endif
