#include "VectorFstOfLatticeArc.h"

void DestroyVectorStateOfLatticeArc(VectorStateOfLatticeArc *vectorState)
{
	if (vectorState == NULL) {
		return;
	}

	vectorState->arcs_.clear();
	delete vectorState;
}

void DestroyVectorFstOfLatticeArc(VectorFstOfLatticeArc* vectorFstImpl)
{
	for (uint s = 0; s < vectorFstImpl->states_.size(); ++s) {
		DestroyVectorStateOfLatticeArc(vectorFstImpl->states_[s]);
	}
	
	vectorFstImpl->states_.clear();
	vectorFstImpl->start_ = -1;
}

void VectorFstOfLatticeArcDeleteStates2(VectorFstOfLatticeArc* vectorFstImpl, vector<int>& dstates)
{
	vector<int> newid(vectorFstImpl->states_.size(), 0);
	for (size_t i = 0; i < dstates.size(); ++i)
		newid[dstates[i]] = -1;
	int nstates = 0;
	for (uint s = 0; s < vectorFstImpl->states_.size(); ++s) {
		if (newid[s] != -1) {
			newid[s] = nstates;
			if (s != nstates)
				vectorFstImpl->states_[nstates] = vectorFstImpl->states_[s];
			++nstates;
		}
		else {
			DestroyVectorStateOfLatticeArc(vectorFstImpl->states_[s]);//State::Destroy(states_[s], &state_alloc_);
		}
	}
	vectorFstImpl->states_.resize(nstates);
	for (uint s = 0; s < vectorFstImpl->states_.size(); ++s) {
		LatticeArc *arcs = &vectorFstImpl->states_[s]->arcs_[s];
		size_t narcs = 0;
		size_t nieps = vectorFstImpl->states_[s]->niepsilons_;
		size_t noeps = vectorFstImpl->states_[s]->noepsilons_;
		for (size_t i = 0; i < vectorFstImpl->states_[s]->arcs_.size(); ++i) {
			int t = newid[arcs[i].nextstate];
			if (t != -1) {
				arcs[i].nextstate = t;
				if (i != narcs)
					arcs[narcs] = arcs[i];
				++narcs;
			}
			else {
				if (arcs[i].ilabel == 0)
					--nieps;
				if (arcs[i].olabel == 0)
					--noeps;
			}
		}
		VectorFstOfLatticeArcDeleteArcs2(vectorFstImpl->states_[s], vectorFstImpl->states_[s]->arcs_.size() - narcs);
		vectorFstImpl->states_[s]->niepsilons_ = nieps;
		vectorFstImpl->states_[s]->noepsilons_ = noeps;
	}
	if (vectorFstImpl->start_ != -1) {
		vectorFstImpl->start_ = newid[vectorFstImpl->start_];
	}
}

void VectorStateOfLatticeArcDeleteArcs(VectorStateOfLatticeArc* vectorStateImpl)
{
	vectorStateImpl->niepsilons_ = 0;
	vectorStateImpl->noepsilons_ = 0;
	vectorStateImpl->arcs_.clear();
}

void VectorFstOfLatticeArcDeleteArcs2(VectorStateOfLatticeArc* vectorStateImpl, int n)
{
	for (size_t i = 0; i < n; ++i) {
		if (vectorStateImpl->arcs_.back().ilabel == 0)
			--vectorStateImpl->niepsilons_;
		if (vectorStateImpl->arcs_.back().olabel == 0)
			--vectorStateImpl->noepsilons_;
		vectorStateImpl->arcs_.pop_back();
	}
}

void VectorFstOfLatticeArcDeleteArcs(VectorFstOfLatticeArc* vectorFstImpl)
{
}

void VectorFstOfLatticeArcDeleteStates(VectorFstOfLatticeArc* vectorFstImpl)
{
	for (uint s = 0; s < vectorFstImpl->states_.size(); ++s) {
		DestroyVectorStateOfLatticeArc(vectorFstImpl->states_[s]);
	}
	vectorFstImpl->states_.clear();
	vectorFstImpl->start_ = -1;
}
