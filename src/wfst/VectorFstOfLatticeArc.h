#ifndef VECTOR_FST_OF_LATTICE_ARC_
#define VECTOR_FST_OF_LATTICE_ARC_

#include "FstHeader.h"
#include "SymbolTable.h"
#include "VectorStateOfLatticeArc.h"

struct VectorFstOfLatticeArc
{
	vector<VectorStateOfLatticeArc *> states_;      // States represenation.
	int start_;               						// initial state
	mutable eesen::uint64 properties_;           	// Property bits
};

float  VectorFstOfLatticeArcFinalValue(VectorFstOfLatticeArc *fst, int state);

int VectorFstOfLatticeArcStart(VectorFstOfLatticeArc* vectorFstImpl);

void VectorFstOfLatticeArcSetProperties(VectorFstOfLatticeArc *vectorFstOfLatticeArc, eesen::uint64 props, eesen::uint64 mask);

void VectorFstOfLatticeArcDeleteStates(VectorFstOfLatticeArc* vectorFstImpl);

void VectorFstOfLatticeArcDeleteArcs2(VectorStateOfLatticeArc* vectorStateImpl, int n);

void VectorFstOfLatticeArcDeleteStates2(VectorFstOfLatticeArc* vectorFstImpl, vector<int>& dstates);

void VectorStateOfLatticeArcDeleteArcs(VectorStateOfLatticeArc* vectorStateImpl);

#endif
