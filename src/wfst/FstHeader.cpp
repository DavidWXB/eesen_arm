#include <assert.h>
#include "FstHeader.h"
#include "../utils/kaldi-types.h"

inline istream &ReadType(istream &strm, string *s) {
	s->clear();
	int ns = 0;
	strm.read(reinterpret_cast<char*>(&ns), sizeof(ns));

	for (int i = 0; i < ns; ++i) {
		char c;
		strm.read(&c, 1);
		*s += c;
	}
	return strm;
}

bool ReadOfFstHeader(FstHeader *fsh, ifstream &strm, const string source, bool rewind)
{
	int magic_number = 0;
	strm.read(reinterpret_cast<char*>(&magic_number), sizeof(int));    // reda 4 bytes

	ReadType(strm, &(fsh->fsttype_));
	ReadType(strm, &(fsh->arctype_));

	strm.read(reinterpret_cast<char*>(&fsh->version_), 	  sizeof(int));
	strm.read(reinterpret_cast<char*>(&fsh->flags_), 	  sizeof(int));
	strm.read(reinterpret_cast<char*>(&fsh->properties_), sizeof(unsigned long long));
	strm.read(reinterpret_cast<char*>(&fsh->start_), 	  sizeof(long long));
	strm.read(reinterpret_cast<char*>(&fsh->numstates_),  sizeof(long long));
	strm.read(reinterpret_cast<char*>(&fsh->numarcs_), 	  sizeof(long long));

	return true;
}

// re-write
bool ReadOfFstHeader(FstHeader *fsh, unsigned char* fst, unsigned int &idx)
{
	// skip first 4 bytes
	fsh->fsttype_ = "vector";
	fsh->arctype_ = "standard";
	idx = 26;
	fsh->version_ = fst[idx] + (fst[idx+1]<<8) + (fst[idx+2]<<16) + (fst[idx+3]<<24); //0x2;
	idx += 4;
	fsh->flags_   = fst[idx] + (fst[idx+1]<<8) + (fst[idx+2]<<16) + (fst[idx+3]<<24); //0x0;
	idx += 4;
	fsh->properties_ = fst[idx] + (fst[idx+1]<<8) + (fst[idx+2]<<16) + (fst[idx+3]<<24) + //0x50000000003;
			(fst[idx+4]<<32) + (fst[idx+5]<<40) + (fst[idx+6]<<48) + (fst[idx+7]<<56);
	idx += 8;
	fsh->start_      = fst[idx] + (fst[idx+1]<<8) + (fst[idx+2]<<16) + (fst[idx+3]<<24) + //0x0;
			(fst[idx+4]<<32) + (fst[idx+5]<<40) + (fst[idx+6]<<48) + (fst[idx+7]<<56);
	idx += 8;
	fsh->numstates_  = fst[idx] + (fst[idx+1]<<8) + (fst[idx+2]<<16) + (fst[idx+3]<<24) + //0x36D18;
			(fst[idx+4]<<32) + (fst[idx+5]<<40) + (fst[idx+6]<<48) + (fst[idx+7]<<56);
	idx += 8;
	fsh->numarcs_    = fst[idx] + (fst[idx+1]<<8) + (fst[idx+2]<<16) + (fst[idx+3]<<24) + //0x0;
			(fst[idx+4]<<32) + (fst[idx+5]<<40) + (fst[idx+6]<<48) + (fst[idx+7]<<56);
	idx += 8;
	assert(idx == 66);

	return true;
}
