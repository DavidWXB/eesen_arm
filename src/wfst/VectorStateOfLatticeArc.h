#ifndef VECTOR_STATE_OF_LATTICE_ARC_
#define VECTOR_STATE_OF_LATTICE_ARC_

#include <vector>
#include <stddef.h>
#include "LatticeArc.h"

struct VectorStateOfLatticeArc
{
	LatticeWeight final_;               // Final weight
	size_t niepsilons_;             	// # of input epsilons
	size_t noepsilons_;             	// # of output epsilons
	std::vector<LatticeArc>arcs_;		//
};

#endif
