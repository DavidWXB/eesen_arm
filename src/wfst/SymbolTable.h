#ifndef SYMBOLTABLE_
#define SYMBOLTABLE_

#include <map>
#include <vector>
#include <string>
#include <functional>
#include "../utils/kaldi-types.h"

using namespace std;

struct DenseSymbolMap
{
	eesen::int64 empty_;
	vector<const char*> symbols_;
	std::hash<string>  str_hash_;
	vector<eesen::int64> buckets_;
	eesen::uint64 hash_mask_;
	int size_;
};

struct SymbolTable
{
	DenseSymbolMap symbols_;
};

void SymbolTableInit(SymbolTable *table, string name);

SymbolTable *ReadTextOfSymbolTable(string name);

SymbolTable *ReadOfSymbolTable(istream *strm);

string FindOfSymbolTable(SymbolTable *symtable, eesen::int64 key);

void DestroySymbolTable(SymbolTable* sTable);

// added by wxb, 2017-01-04
SymbolTable *ReadTextOfSymbolTable(const char *word[]);

#endif
