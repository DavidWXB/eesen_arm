#include <fstream>
#include <cstring>
#include "SymbolTable.h"
#include "../utils/kaldi-types.h"

using namespace std;

#define FST_FIELD_SEPARATOR "\t \n"
#define KLINELEN 			8096
#define WORDCOUNT 			7313	//30247

void SymbolTableInit(SymbolTable *table, string name)
{
	table->symbols_.empty_ = -1;
	table->symbols_.buckets_.resize(16);
	table->symbols_.hash_mask_ = table->symbols_.buckets_.size() - 1;

	for (uint i = 0; i < table->symbols_.buckets_.size(); i++) {
		table->symbols_.buckets_[i] = table->symbols_.empty_;
	}
}

void SplitToVector(char* full, const char* delim, vector<char*>* vec,
	bool omit_empty_strings) {
	char *p = full;
	while (p) {
		if ((p = strpbrk(full, delim)))
			p[0] = '\0';
		if (!omit_empty_strings || full[0] != '\0')
			vec->push_back(full);
		if (p)
			full = p + 1;
	}
}

const char* NewSymbol(const string& sym) {
	size_t num = sym.size() + 1;
	char *newstr = new char[num];
	memcpy(newstr, sym.c_str(), num);
	return newstr;
}

void DestroySymbolTable(SymbolTable* sTable)
{
	for (uint i = 0; i < sTable->symbols_.symbols_.size(); i++) {
		delete sTable->symbols_.symbols_[i];
	}
	delete sTable;
}

SymbolTable *ReadTextOfSymbolTable(string name)
{
	ifstream strm(name.c_str(), ifstream::in);
	if (!strm.good()) {
		return 0;
	}

	SymbolTable *impl = new SymbolTable();
	SymbolTableInit(impl, name);

	eesen::int64 nline = 0;
	char line[KLINELEN];
	while (!strm.getline(line, KLINELEN).fail()) {
		++nline;
		vector<char *> col;
		string separator = FST_FIELD_SEPARATOR;
		SplitToVector(line, separator.c_str(), &col, true);
		if (col.size() == 0)
			continue;
		if (col.size() != 2) {
			delete impl;
			return 0;
		}
		const char *symbol = col[0];

		//sunhao 16-10-13   
		impl->symbols_.symbols_.push_back(NewSymbol(symbol));
	}
	strm.close();
	return impl;
}

string FindOfSymbolTable(SymbolTable *symtable, eesen::int64 key)
{
	eesen::int64 idx = key;
	if (idx < 0 || idx >= symtable->symbols_.symbols_.size())
        return "";
	return string(symtable->symbols_.symbols_[idx], strlen(symtable->symbols_.symbols_[idx]));
}

// added by wxb, 2017-01-04
SymbolTable *ReadTextOfSymbolTable(const char *word[])
{
	SymbolTable *impl = new SymbolTable();
	SymbolTableInit(impl, "");

	for(int i = 0; i < WORDCOUNT; i++) {
		impl->symbols_.symbols_.push_back(word[i]);
	}

	return impl;
}
