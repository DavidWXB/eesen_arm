#ifndef FSTFILE_H_
#define FSTFILE_H_

#include "VectorFstOfStdArc.h"

using namespace std;

void ReadFstKaldi(VectorFstOfStdArc *fst, string rxfilename);
void ReadFstKaldi(VectorFstOfStdArc *fst, unsigned char **wfst, unsigned int &idx);

#endif
