#ifndef WFST_MAIN_H_
#define WFST_MAIN_H_

#include "../decode/DecodableInterface.h"
#include "../decode/LatticeFasterDecoder.h"

void wfstInit();
void EesenMain(Matrix_Float *mat);
void EesenDelete(LatticeFasterDecoder *decode, DecodableInterface *decodable);

#endif
