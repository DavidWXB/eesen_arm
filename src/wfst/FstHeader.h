#ifndef FSTHEADER_
#define FSTHEADER_

#include <string>
#include <fstream>
#include "../utils/kaldi-types.h"

using namespace std;

struct FstHeader {
	enum {
		HAS_ISYMBOLS = 0x1,         // Has input symbol table
		HAS_OSYMBOLS = 0x2,         // Has output symbol table
		IS_ALIGNED   = 0x4,         // Memory-aligned (where appropriate)
	} Flags;

	string fsttype_;                // E.g. "vector"
	string arctype_;                // E.g. "standard"
	eesen::int32 version_;          // Type version #
	eesen::int32 flags_;            // File format bits
	eesen::uint64 properties_;      // FST property bits
	eesen::int64 start_;            // Start state
	eesen::int64 numstates_;        // # of states
	eesen::int64 numarcs_;          // # of arcs
};

bool ReadOfFstHeader(FstHeader *fsh, unsigned char *fst, unsigned int &idx);
bool ReadOfFstHeader(FstHeader *fsh, ifstream &strm, const string source, bool rewind);

#endif
