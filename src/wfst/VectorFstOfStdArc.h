#ifndef VECTORFSTOFSTDARC_
#define VECTORFSTOFSTDARC_

#include "FstHeader.h"
#include "SymbolTable.h"
#include "FstReadOptions.h"
#include "VectorStateOfStdArc.h"
#include "../utils/kaldi-types.h"

struct VectorFstOfStdArc
{
	vector<VectorStateOfStdArc *> states_;      // States represenation.
	int start_;                                 // initial state
	mutable eesen::uint64 properties_;     		// Property bits
};

float VectorFstOfStdArcFinalValue(VectorFstOfStdArc *fst, int state);
void  VectorFstOfStdArcDeleteStates(VectorFstOfStdArc* vectorFstImpl);
void  VectorFstOfStdArcDeleteArcs(VectorFstOfStdArc* vectorFstImpl, int stateId);

//��FST����Ҫ����
void ReadOfVectorFstOfStdArc(VectorFstOfStdArc *fst, std::ifstream &ifs, FstHeader ropts);
bool ReadHeaderOfVectorFstOfStdArc(std::ifstream *ifs, FstReadOptions ropts, int kMinFileVersion, FstHeader *hdr);
void DestroyVectorFstOfStdArc(VectorFstOfStdArc *fst);

// adapt to arm
void ReadOfVectorFstOfStdArc(VectorFstOfStdArc *fst, unsigned char **wfst, unsigned int &idx, FstHeader fstheader);

#endif
