#ifndef VECTOR_FST_OF_COMPACT_LATTICE_
#define VECTOR_FST_OF_COMPACT_LATTICE_

#include <vector>
#include <string>
#include "FstHeader.h"
#include "SymbolTable.h"
#include "VectorStateOfCompactLatticeArc.h"

struct VectorFstOfCompactLatticeArc
{
	vector<VectorStateOfCompactLatticeArc *> states_;      	// States represenation.
	int start_;               								// initial state
	mutable eesen::uint64 properties_;           			// Property bits
};

void VectorFstOfCompactLatticeArcConnect(VectorFstOfCompactLatticeArc *vectorFstOfCompactLatticeArc);

void VectorFstOfCompactLatticeArcSetProperties(VectorFstOfCompactLatticeArc *vectorFstOfCompactLatticeArc, eesen::uint64 props, eesen::uint64 mask);

void VectorFstOfCompactLatticeArcDeleteArcs(VectorFstOfCompactLatticeArc* vectorFstImpl, int stateId);

void VectorFstOfCompactLatticeArcDeleteStates(VectorFstOfCompactLatticeArc* vectorFstImpl);

void VectorFstOfCompactLatticeArcDeleteStates2(VectorFstOfCompactLatticeArc* vectorFstImpl, vector<int>& dstates);

void VectorFstOfCompactLatticeArcDeleteArcs2(VectorStateOfCompactLatticeArc* vectorStateImpl, int n);

void DestroyVectorStateOfCompactLatticeArc(VectorStateOfCompactLatticeArc *vectorState);

void DestroyVectorFstOfCompactLatticeArc(VectorFstOfCompactLatticeArc* vectorFstImpl);

#endif
