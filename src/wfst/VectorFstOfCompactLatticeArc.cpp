#include "VectorFstOfCompactLatticeArc.h"

void DestroyVectorStateOfCompactLatticeArc(VectorStateOfCompactLatticeArc *vectorState)
{
	if (vectorState == NULL) {
		return;
	}

	int len = vectorState->arcs_.size();
	for (int i = 0; i < len; i++) {
		vectorState->arcs_[i].weight.string_.clear();
	}
	vectorState->arcs_.clear();
	delete(vectorState);
}

void DestroyVectorFstOfCompactLatticeArc(VectorFstOfCompactLatticeArc* vectorFstImpl)
{
	for (uint s = 0; s < vectorFstImpl->states_.size(); ++s) {
		DestroyVectorStateOfCompactLatticeArc(vectorFstImpl->states_[s]);
	}

	vectorFstImpl->states_.clear();
	vectorFstImpl->start_ = -1;
}

void VectorFstOfCompactLatticeArcDeleteStates2(VectorFstOfCompactLatticeArc* vectorFstImpl, vector<int>& dstates)
{
	vector<int> newid(vectorFstImpl->states_.size(), 0);
	for (size_t i = 0; i < dstates.size(); ++i)
		newid[dstates[i]] = -1;
	int nstates = 0;
	for (uint s = 0; s < vectorFstImpl->states_.size(); ++s) {
		if (newid[s] != -1) {
			newid[s] = nstates;
			if (s != nstates)
				vectorFstImpl->states_[nstates] = vectorFstImpl->states_[s];
			++nstates;
		}
		else {
			DestroyVectorStateOfCompactLatticeArc(vectorFstImpl->states_[s]);
		}
	}
	vectorFstImpl->states_.resize(nstates);
	for (uint s = 0; s < vectorFstImpl->states_.size(); ++s) {
		CompactLatticeArc *arcs = &vectorFstImpl->states_[s]->arcs_[s];
		size_t narcs = 0;
		size_t nieps = vectorFstImpl->states_[s]->niepsilons_;
		size_t noeps = vectorFstImpl->states_[s]->noepsilons_;
		for (size_t i = 0; i < vectorFstImpl->states_[s]->arcs_.size(); ++i) {
			int t = newid[arcs[i].nextstate];
			if (t != -1) {
				arcs[i].nextstate = t;
				if (i != narcs)
					arcs[narcs] = arcs[i];
				++narcs;
			}
			else {
				if (arcs[i].ilabel == 0)
					--nieps;
				if (arcs[i].olabel == 0)
					--noeps;
			}
		}
		VectorFstOfCompactLatticeArcDeleteArcs2(vectorFstImpl->states_[s], vectorFstImpl->states_[s]->arcs_.size() - narcs);
		vectorFstImpl->states_[s]->niepsilons_ = nieps;
		vectorFstImpl->states_[s]->noepsilons_ = noeps;
	}
	if (vectorFstImpl->start_ != -1) {
		vectorFstImpl->start_ = newid[vectorFstImpl->start_];
	}
}

void VectorFstOfCompactLatticeArcDeleteArcs(VectorFstOfCompactLatticeArc* vectorFstImpl, int stateId)
{
	vectorFstImpl->states_[stateId]->niepsilons_ = 0;
	vectorFstImpl->states_[stateId]->noepsilons_ = 0;
	vectorFstImpl->states_[stateId]->arcs_.clear();
}

void VectorFstOfCompactLatticeArcDeleteArcs2(VectorStateOfCompactLatticeArc* vectorStateImpl, int n)
{
	for (size_t i = 0; i < n; ++i) {
		if (vectorStateImpl->arcs_.back().ilabel == 0)
			--vectorStateImpl->niepsilons_;
		if (vectorStateImpl->arcs_.back().olabel == 0)
			--vectorStateImpl->noepsilons_;
		vectorStateImpl->arcs_.pop_back();
	}
}

void VectorFstOfCompactLatticeArcConnect(VectorFstOfCompactLatticeArc *vectorFstOfCompactLatticeArc)
{
}
