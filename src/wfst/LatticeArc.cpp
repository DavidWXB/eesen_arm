#include "LatticeArc.h"

LatticeWeight LatticeWeightInit(float value1, float value2)
{
	LatticeWeight tropicalWeight;
	tropicalWeight.value1_ = value1;
	tropicalWeight.value2_ = value2;
	return tropicalWeight;
}

int LatticeWeightCompare(const LatticeWeight &w1, const LatticeWeight &w2) {
	float f1 = w1.value1_ + w1.value2_,
		  f2 = w2.value1_ + w2.value2_;
	if (f1 < f2) {		// having smaller cost means you're larger
		return 1;
	}
	else if (f1 > f2) {	// in the semiring [higher probability]
		return -1;
	}
	// mathematically we should be comparing (w1.value1_-w1.value2_ < w2.value1_-w2.value2_)
	// in the next line, but add w1.value1_+w1.value2_ = w2.value1_+w2.value2_ to both sides and
	// divide by two, and we get the simpler equivalent form w1.value1_ < w2.value1_.
	else if (w1.value1_ < w2.value1_) {
		return 1;
	}
	else if (w1.value1_ > w2.value1_) {
		return -1;
	}
	else {
		return 0;
	}
}
