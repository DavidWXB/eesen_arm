#ifndef STDARC_
#define STDARC_

struct TropicalWeight
{
	float value_;
};

TropicalWeight TropicalWeightTime(TropicalWeight *weight1, TropicalWeight *weight2);
TropicalWeight TropicalWeightInit(float value);

struct StdArc
{
	int ilabel;
	int olabel;
	TropicalWeight weight;
	int nextstate;
};

#endif
