#ifndef LATTICEARC_
#define LATTICEARC_

struct LatticeWeight
{
	float value1_;
	float value2_;
};

LatticeWeight LatticeWeightInit(float value1, float value2);

struct LatticeArc
{
	int ilabel;
	int olabel;
	LatticeWeight weight;
	int nextstate;
};
LatticeWeight LatticeWeightTime(LatticeWeight *weight1, LatticeWeight *weight2);

int LatticeWeightCompare(const LatticeWeight &w1, const LatticeWeight &w2);

#endif
