#ifndef VECTOR_STATE_OF_COMPACT_LATTICE_ARC_
#define VECTOR_STATE_OF_COMPACT_LATTICE_ARC_

#include <stddef.h>
#include "CompactLatticeArc.h"

struct VectorStateOfCompactLatticeArc
{
	CompactLatticeWeight final_;                  	// Final weight
	size_t niepsilons_;             				// # of input epsilons
	size_t noepsilons_;             				// # of output epsilons
	std::vector<CompactLatticeArc> arcs_;  			// Arcs represenation
};

#endif
