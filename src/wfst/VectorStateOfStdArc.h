#ifndef VECTORSTATEOFSTDARC_
#define VECTORSTATEOFSTDARC_

#include <vector>
#include <stddef.h>
#include "StdArc.h"

struct VectorStateOfStdArc
{
	TropicalWeight final_;          // Final weight
	size_t niepsilons_;             // of input epsilons
	size_t noepsilons_;             // of output epsilons
	std::vector<StdArc> arcs_;      // Arcs represenation
};

#endif
