#include "StdArc.h"

TropicalWeight TropicalWeightInit(float value)
{
	TropicalWeight tropicalWeight;
	tropicalWeight.value_ = value;
	return tropicalWeight;
}

TropicalWeight TropicalWeightTime(TropicalWeight *weight1, TropicalWeight *weight2)
{
	TropicalWeight tropicalWeight;
	tropicalWeight.value_ = weight1->value_ + weight2->value_;
	return tropicalWeight;
}
