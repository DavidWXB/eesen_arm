#include "LatticeFasterDecoder.h"
#include "DecodableInterface.h"
#include "TokenList.h"
#include <algorithm>
#include <unordered_set>
#include "../wfst/VectorStateOfStdArc.h"
#include <time.h>
#include <float.h>
#include <limits.h>
#include <string.h>

void LatticeFasterDecoderInit(LatticeFasterDecoder * decoder, VectorFstOfStdArc *fst, LatticeFasterDecoderConfig config)
{
	decoder->toks_.list_head_  = NULL;
	decoder->toks_.bucket_list_tail_ = static_cast<size_t>(-1);  // invalid.
	decoder->toks_.hash_size_  = 0;
	decoder->toks_.freed_head_ = NULL;

	decoder->fst_ = *fst;
	decoder->config_ = config;
	decoder->num_toks_ = 0;
	HashListSetSize(&(decoder->toks_), 1000);
}

bool Decode(LatticeFasterDecoder * decoder,DecodableInterface *decodable)
{
	InitDecoding(decoder);
	while (decoder->active_toks_.size()-1 != decodable->likes_.num_rows_)
	{
		//每25帧都裁剪一次的目的是为了防止无效链的过快增长，减少复杂度  最终还会在全局裁剪一次保证裁剪的正确性
		if ((decoder->active_toks_.size() - 1) % decoder->config_.prune_interval == 0)
			PruneActiveTokens(decoder, decoder->config_.lattice_beam * decoder->config_.prune_scale);
		ProcessEmitting(decoder, decodable);  // Note: the value returned by
		ProcessNonemitting(decoder);
	}
	FinalizeDecoding(decoder);

	// Returns true if we have any kind of traceback available (not necessarily
	// to the end state; query ReachedFinal() for that).
	return !decoder->active_toks_.empty() && decoder->active_toks_.back().toks != NULL;
}

void InitDecoding(LatticeFasterDecoder * decoder)
{
	Elem_StateId_Token* ans = HashListClear(&(decoder->toks_));
	DeleteElems(&(decoder->toks_), ans);
	decoder->cost_offsets_.clear();
	ClearActiveTokens(decoder);

	decoder->num_toks_ = 0;
	decoder->decoding_finalized_ = false;
	decoder->final_costs_.clear();
	int start_state = decoder->fst_.start_;

	decoder->active_toks_.resize(1);
	decoder->active_toks_[decoder->active_toks_.size() - 1].must_prune_forward_links = true;
	decoder->active_toks_[decoder->active_toks_.size() - 1].must_prune_tokens = true;
	decoder->active_toks_[decoder->active_toks_.size() - 1].toks = NULL;

	Token *start_tok = (Token *)malloc(sizeof(Token));
	Token_Init(start_tok, 0.0, 0.0, NULL, NULL);

	decoder->active_toks_[0].toks = start_tok;
	HashListInsert(&(decoder->toks_), start_state, start_tok);
	decoder->num_toks_++;
	ProcessNonemitting(decoder);
}

void DestroyHashList(HashList_StateId_Token *toks_)
{
	// First test whether we had any memory leak within the
	// HashList, i.e. things for which the user did not call Delete().
	size_t num_in_list = 0, num_allocated = 0;
	for (Elem_StateId_Token *e = toks_->freed_head_; e != NULL; e = e->tail)
		num_in_list++;
	for (size_t i = 0; i < toks_->allocated_.size(); i++) {
		num_allocated += toks_->allocate_block_size_;
		free(toks_->allocated_[i]);
	}
	if (num_in_list != num_allocated) {
		//error
	}
}

void DeleteElems(HashList_StateId_Token *toks_, Elem_StateId_Token *elem)
{
	for (Elem_StateId_Token *e = elem, *e_tail; e != NULL; e = e_tail) {
		e_tail = e->tail;
		e->tail = toks_->freed_head_;
		toks_->freed_head_ = e;
	}
}

void ClearActiveTokens(LatticeFasterDecoder * decoder)
{
	for (size_t i = 0; i < decoder->active_toks_.size(); i++) {
		// Delete all tokens alive on this frame, and any forward links they may have.
		for (Token *tok = decoder->active_toks_[i].toks; tok != NULL;) {
			DeleteForwardLinks(tok);
			Token *next_tok = tok->next;
			delete tok;
			decoder->num_toks_--;
			tok = next_tok;
		}
	}
	decoder->active_toks_.clear();
}

void ProcessEmitting(LatticeFasterDecoder * decoder, DecodableInterface *decodable)
{
	int32 frame = decoder->active_toks_.size() - 1; // frame is the frame-index
	// (zero-based) used to get likelihoods from the decodable object.
	decoder->active_toks_.resize(decoder->active_toks_.size() + 1);
	decoder->active_toks_[decoder->active_toks_.size() - 1].must_prune_forward_links = true;
	decoder->active_toks_[decoder->active_toks_.size() - 1].must_prune_tokens = true;
	decoder->active_toks_[decoder->active_toks_.size() - 1].toks = NULL;

	//删除toks_里面的bucket 返回toks_里面的token的list
	Elem_StateId_Token *final_toks = HashListClear(&(decoder->toks_));
	// in simple-decoder.h.   Removes the Elems from
	// being indexed in the hash in toks_.
	Elem_StateId_Token *best_elem = NULL;
	float adaptive_beam;
	size_t tok_cnt;
	//计算阈值 adaptive_beam是满足条件的阈值 cur_cutoff是找tokenlist里代价最小的token加beam得到阈值等于上一帧最小min_tot+adaptive_beam，要求留下来的token个数在max_active和min_active范围内否则调整阈值 并统计token个数
	//注意:该阈值用于对上一帧所有的token选择时使用，不满足阈值的token不加link

	float cur_cutoff = GetCutoff(decoder, final_toks, &tok_cnt, &adaptive_beam, &best_elem);

	PossiblyResizeHash(decoder, tok_cnt);  // This makes sure the hash is always big enough.
	
	float next_cutoff = FLT_MAX;

	// pruning "online" before having seen all tokens
	float cost_offset = 0.0; // Used to keep probabilities in a good dynamic range.

	// First process the best token to get a hopefully
	// reasonably tight bound on the next cutoff.  The only
	// products of the next block are "next_cutoff" and "cost_offset".
	//取最优的token的fst预测link的bestweight(bestweight=音素weight+语言模型weight)计算阈值
	//注意:该阈值是为每个token在使用fst建立新的link时使用的，使用fst预测的token只有满足阈值的link才保留下来
	if (best_elem) {
		int state = best_elem->key;
		Token *tok = best_elem->val;
		cost_offset = -tok->tot_cost;

		vector<StdArc> *vectorStdArc = &(decoder->fst_.states_[state]->arcs_);
		int arcnum = decoder->fst_.states_[state]->arcs_.size();
		for (int i = 0; i<arcnum; i++) {
			StdArc arc = (*vectorStdArc)[i];
			if (arc.ilabel != 0) {  // propagate..
				float logLikelihood = DecodableInterfaceLogLikelihood(decodable, frame, arc.ilabel);
				arc.weight.value_ = arc.weight.value_ + (cost_offset - logLikelihood);
				float new_weight = arc.weight.value_ + tok->tot_cost;
				if (new_weight + adaptive_beam < next_cutoff)
					next_cutoff = new_weight + adaptive_beam;
			}
		} // for all arcs
	}

	// Store the offset on the acoustic likelihoods that we're applying.
	// Could just do cost_offsets_.push_back(cost_offset), but we
	// do it this way as it's more robust to future code changes.
	//cost_offsets_存储每一帧最优的token
	decoder->cost_offsets_.resize(frame + 1, 0.0);
	decoder->cost_offsets_[frame] = cost_offset;

	// the tokens are now owned here, in final_toks, and the hash is empty.
	// 'owned' is a complex thing here; the point is we need to call DeleteElem
	// on each elem 'e' to let toks_ know we're done with them.
	//遍历上一帧的token，用fst_预测token的所有可能的集合保存到arc
	//去除掉上一帧中不满足阈值的token，保留下来的token使用fst预测，并选择满足阈值的link建立连接
	//tot_cost计算方法是取当前token的tot_cost值减去当前帧的代价最小的tot_cost值再加上音素分数和语言模型分数
	for (Elem_StateId_Token *e = final_toks, *e_tail; e != NULL; e = e_tail) {
		// loop this way because we delete "e" as we go.
		int state = e->key;
		Token *tok = e->val;

		if (tok->tot_cost <= cur_cutoff) {

			vector<StdArc> *vectorStdArc = &(decoder->fst_.states_[state]->arcs_);
			int arcnum = decoder->fst_.states_[state]->arcs_.size();
			for (int i = 0; i < arcnum; i++) {

				StdArc arc = (*vectorStdArc)[i];
				if (arc.ilabel != 0) {  // propagate..
					float logLikelihood = DecodableInterfaceLogLikelihood(decodable, frame, arc.ilabel);
					float ac_cost = cost_offset - logLikelihood;

					float graph_cost = arc.weight.value_;
					float cur_cost = tok->tot_cost;
					float tot_cost = cur_cost + ac_cost + graph_cost;
					if (tot_cost > next_cutoff) continue;
					else if (tot_cost + decoder->config_.beam < next_cutoff)
						next_cutoff = tot_cost + decoder->config_.beam; // prune by best current token
					// Note: the frame indexes into active_toks_ are one-based,
					// hence the + 1.
					Token *next_tok = FindOrAddToken(decoder, arc.nextstate,
						frame + 1, tot_cost, NULL);
					// NULL: no change indicator needed
					
					// Add ForwardLink from tok to next_tok (put on head of list tok->links)
					tok->links = ForwardLink_Init(next_tok, arc.ilabel, arc.olabel, graph_cost, ac_cost, tok->links);
				}
			} // for all arcs
		}
		e_tail = e->tail;
		HashListDelete(&(decoder->toks_), e);
	}
}

void ProcessNonemitting(LatticeFasterDecoder * decoder)
{
	int32 frame = static_cast<int32>(decoder->active_toks_.size()) - 2;
	// Note: "frame" is the time-index we just processed, or -1 if
	// we are processing the nonemitting transitions before the
	// first frame (called from InitDecoding()).

	// Processes nonemitting arcs for one frame.  Propagates within toks_.
	// Note-- this queue structure is is not very optimal as
	// it may cause us to process states unnecessarily (e.g. more than once),
	// but in the baseline code, turning this vector into a set to fix this
	// problem did not improve overall speed.

	float best_cost = FLT_MAX;
	for (const Elem_StateId_Token *e = decoder->toks_.list_head_; e != NULL; e = e->tail) {
		decoder->queue_.push_back(e->key);
		// for pruning with current best token
		best_cost = min(best_cost, (e->val->tot_cost));
	}
	if (decoder->queue_.empty()) {
		return;
	}
	float cutoff = best_cost + decoder->config_.beam;

	while (!decoder->queue_.empty()) {
		int state = decoder->queue_.back();
		decoder->queue_.pop_back();

		Token *tok = HashListFind(&(decoder->toks_),state)->val;  // would segfault if state not in toks_ but this can't happen.
		float cur_cost = tok->tot_cost;
		if (cur_cost > cutoff) // Don't bother processing successors.
			continue;

		DeleteForwardLinks(tok); // necessary when re-visiting
		tok->links = NULL;
		vector<StdArc> *vectorStdArc = &(decoder->fst_.states_[state]->arcs_);
		int arcnum = decoder->fst_.states_[state]->arcs_.size();
		for (int i=0; i<arcnum; i++) {
			StdArc arc = (*vectorStdArc)[i];
			if (arc.ilabel == 0) {  // propagate nonemitting only...
				float graph_cost = arc.weight.value_,
					tot_cost = cur_cost + graph_cost;
				if (tot_cost < cutoff) {
					bool changed;
					Token *new_tok = FindOrAddToken(decoder, arc.nextstate, frame + 1, tot_cost, &changed);
					tok->links = ForwardLink_Init(new_tok, 0, arc.olabel, graph_cost, 0, tok->links);

					// "changed" tells us whether the new token has a different
					// cost from before, or is new [if so, add into queue].
					if (changed)
						decoder->queue_.push_back(arc.nextstate);
				}
			}
		} // for all arcs
	} // while queue not empty
}

void FinalizeDecoding(LatticeFasterDecoder * decoder)
{
	int32 final_frame_plus_one = decoder->active_toks_.size()-1;
	// int32 num_toks_begin = decoder->num_toks_;				// not used
	// PruneForwardLinksFinal() prunes final frame (with final-probs), and
	// sets decoding_finalized_.
	PruneForwardLinksFinal(decoder);
	for (int32 f = final_frame_plus_one - 1; f >= 0; f--) {
		bool b1, b2; 			// values not used.
		float dontcare = 0.0; 	// delta of zero means we must always update
		PruneForwardLinks(decoder, f, &b1, &b2, dontcare);
		PruneTokensForFrame(decoder, f + 1);
	}
	PruneTokensForFrame(decoder, 0);
}

void ComputeFinalCosts(LatticeFasterDecoder * decoder, unordered_map<Token*, float> *final_costs, float *final_relative_cost, float *final_best_cost)
{
	if (final_costs != NULL)
		final_costs->clear();
	const Elem_StateId_Token *final_toks = decoder->toks_.list_head_;
	float infinity = FLT_MAX;
	float best_cost = infinity,
		best_cost_with_final = infinity;
	while (final_toks != NULL) {
		int state = final_toks->key;
		Token *tok = final_toks->val;
		const Elem_StateId_Token *next = final_toks->tail;
		float final_cost = decoder->fst_.states_[state]->final_.value_;
		float cost = tok->tot_cost,
			cost_with_final = cost + final_cost;
		best_cost = min(cost, best_cost);
		best_cost_with_final = min(cost_with_final, best_cost_with_final);
		if (final_costs != NULL && final_cost != infinity)
			(*final_costs)[tok] = final_cost;
		final_toks = next;
	}
	if (final_relative_cost != NULL) {
		if (best_cost == infinity && best_cost_with_final == infinity) {
			// Likely this will only happen if there are no tokens surviving.
			// This seems the least bad way to handle it.
			*final_relative_cost = infinity;
		}
		else {
			*final_relative_cost = best_cost_with_final - best_cost;
		}
	}
	if (final_best_cost != NULL) {
		if (best_cost_with_final != infinity) { // final-state exists.
			*final_best_cost = best_cost_with_final;
		}
		else { // no final-state exists.
			*final_best_cost = best_cost;
		}
	}
}

void PruneForwardLinksFinal(LatticeFasterDecoder * decoder)
{
	int32 frame_plus_one = decoder->active_toks_.size() - 1;

	typedef unordered_map<Token*, float>::const_iterator IterType;
	ComputeFinalCosts(decoder, &decoder->final_costs_, &decoder->final_relative_cost_, &decoder->final_best_cost_);
	decoder->decoding_finalized_ = true;
	// We call DeleteElems() as a nicety, not because it's really necessary;
	// otherwise there would be a time, after calling PruneTokensForFrame() on the
	// final frame, when toks_.GetList() or toks_.Clear() would contain pointers
	// to nonexistent tokens.

	DeleteElems(&decoder->toks_, HashListClear(&(decoder->toks_)));

	// Now go through tokens on this frame, pruning forward links...  may have to
	// iterate a few times until there is no more change, because the list is not
	// in topological order.  This is a modified version of the code in
	// PruneForwardLinks, but here we also take account of the final-probs.
	bool changed = true;
	float delta = 1.0e-05;
	while (changed) {
		changed = false;
		for (Token *tok = decoder->active_toks_[frame_plus_one].toks;
			tok != NULL; tok = tok->next) {
			ForwardLink *link, *prev_link = NULL;
			// will recompute tok_extra_cost.  It has a term in it that corresponds
			// to the "final-prob", so instead of initializing tok_extra_cost to infinity
			// below we set it to the difference between the (score+final_prob) of this token,
			// and the best such (score+final_prob).
			float final_cost;
			if (decoder->final_costs_.empty()) {
				final_cost = 0.0;
			}
			else {
				IterType iter = decoder->final_costs_.find(tok);
				if (iter != decoder->final_costs_.end())
					final_cost = iter->second;
				else
					final_cost = FLT_MAX;
			}
			float tok_extra_cost = tok->tot_cost + final_cost - decoder->final_best_cost_;
			// tok_extra_cost will be a "min" over either directly being final, or
			// being indirectly final through other links, and the loop below may
			// decrease its value:
			for (link = tok->links; link != NULL;) {
				// See if we need to excise this link...
				Token *next_tok = link->next_tok;
				float link_extra_cost = next_tok->extra_cost +
					((tok->tot_cost + link->acoustic_cost + link->graph_cost)
					- next_tok->tot_cost);
				if (link_extra_cost > decoder->config_.lattice_beam) {  // excise link
					ForwardLink *next_link = link->next;
					if (prev_link != NULL) prev_link->next = next_link;
					else tok->links = next_link;
					delete link;
					link = next_link; // advance link but leave prev_link the same.
				}
				else { // keep the link and update the tok_extra_cost if needed.
					if (link_extra_cost < 0.0) { // this is just a precaution.
						link_extra_cost = 0.0;
					}
					if (link_extra_cost < tok_extra_cost)
						tok_extra_cost = link_extra_cost;
					prev_link = link;
					link = link->next;
				}
			}
			// prune away tokens worse than lattice_beam above best path.  This step
			// was not necessary in the non-final case because then, this case
			// showed up as having no forward links.  Here, the tok_extra_cost has
			// an extra component relating to the final-prob.
			if (tok_extra_cost > decoder->config_.lattice_beam)
				tok_extra_cost = FLT_MAX;
			// to be pruned in PruneTokensForFrame

			if (!ApproxEqual(tok->extra_cost, tok_extra_cost, delta))
				changed = true;
			tok->extra_cost = tok_extra_cost; // will be +infinity or <= lattice_beam_.
		}
	} // while changed
}

void PruneActiveTokens(LatticeFasterDecoder * decoder, float delta)
{
	int32 cur_frame_plus_one = decoder->active_toks_.size()-1;
	// int32 num_toks_begin = decoder->num_toks_;			// not used
	// The index "f" below represents a "frame plus one", i.e. you'd have to subtract
	// one to get the corresponding index for the decodable object.
	for (int32 f = cur_frame_plus_one - 1; f >= 0; f--) {
		// Reason why we need to prune forward links in this situation:
		// (1) we have never pruned them (new TokenList)
		// (2) we have not yet pruned the forward links to the next f,
		// after any of those tokens have changed their extra_cost.
		if (decoder->active_toks_[f].must_prune_forward_links) {
			bool extra_costs_changed = false, links_pruned = false;
			//extra_costs_changed用来判断extra_cost是否有变化需要向前传递
			//links_pruned用来判断是否有link被删除
			//extra_cost：出现路径会合时，当前路径的cost - 最优cost
			PruneForwardLinks(decoder, f, &extra_costs_changed, &links_pruned, delta);
			if (extra_costs_changed && f > 0) // any token has changed extra_cost
				decoder->active_toks_[f - 1].must_prune_forward_links = true;
			if (links_pruned) // any link was pruned
				decoder->active_toks_[f].must_prune_tokens = true;
			decoder->active_toks_[f].must_prune_forward_links = false; // job done
		}
		if (f + 1 < cur_frame_plus_one &&      // except for last f (no forward links)
			decoder->active_toks_[f + 1].must_prune_tokens) {
			PruneTokensForFrame(decoder, f + 1);
			decoder->active_toks_[f + 1].must_prune_tokens = false;
		}
	}
}

void PruneForwardLinks(LatticeFasterDecoder * decoder, int frame_plus_one, bool *extra_costs_changed,
	bool *links_pruned, float delta)
{
	*extra_costs_changed = false;
	*links_pruned = false;
	if (decoder->active_toks_[frame_plus_one].toks == NULL) {  // empty list; should not happen.
		return;
	}

	// We have to iterate until there is no more change, because the links
	// are not guaranteed to be in topological order.
	bool changed = true;  // difference new minus old extra cost >= delta ?
	//while(changed)的目的是帧内的extra_cost参差的的传递  当参差传递到帧内的首节点时tok->extra_cost不会再改变  也就是说此时changed=false
	while (changed) {
		changed = false;
		for (Token *tok = decoder->active_toks_[frame_plus_one].toks;
			tok != NULL; tok = tok->next) {
			ForwardLink *link, *prev_link = NULL;
			// will recompute tok_extra_cost for tok.
			float tok_extra_cost = FLT_MAX;// INF;// std::numeric_limits<float>::infinity();
			// tok_extra_cost is the best (min) of link_extra_cost of outgoing links
			for (link = tok->links; link != NULL;) {
				// See if we need to excise this link...
				Token *next_tok = link->next_tok;
				//extra_cost：出现路径会合时，当前路径的cost-最优cost
				float link_extra_cost = next_tok->extra_cost +
					((tok->tot_cost + link->acoustic_cost + link->graph_cost)
					- next_tok->tot_cost);  // difference in brackets is >= 0

				//KALDI_ASSERT(link_extra_cost == link_extra_cost);  // check for NaN
				//config_.lattice_beam：路径会合时，是否删除link的阈值
				if (link_extra_cost > decoder->config_.lattice_beam) {  // excise link
					ForwardLink *next_link = link->next;
					if (prev_link != NULL) prev_link->next = next_link;
					else tok->links = next_link;
					delete link;
					link = next_link;  // advance link but leave prev_link the same.
					*links_pruned = true;
				}
				else {   // keep the link and update the tok_extra_cost if needed.
					if (link_extra_cost < 0.0) {  // this is just a precaution.
						if (link_extra_cost < -0.01)
						link_extra_cost = 0.0;
					}
					if (link_extra_cost < tok_extra_cost)
						tok_extra_cost = link_extra_cost;
					prev_link = link;  // move to next link
					link = link->next;
				}
			}  // for all outgoing links
			//delta：大于delta的extra_cost向前传递
			//减去tok->extra_cost的目的是extra_cost参差在传递时(tok_extra_cost - tok->extra_cost)的差值是有变化的，如果传递完成了差值就等于0了，如果不减tok->extra_cost会导致死循环
			//delta的目的是参差在传递累加时最大参差必须大于delta，因为如果w1<0.5*beam,w2<0.5*beam,w3<0.5*beam,结果满足w1+w2+w3>beam的概率较低，其实最准确应该是delta=0
			if (fabs(tok_extra_cost - tok->extra_cost) > delta)
				changed = true;   // difference new minus old is bigger than delta
			tok->extra_cost = tok_extra_cost;
			// will be +infinity or <= lattice_beam_.
			// infinity indicates, that no forward link survived pruning
		}  // for all Token on active_toks_[frame]
		if (changed)
			*extra_costs_changed = true;
	} // while changed
}

void PruneTokensForFrame(LatticeFasterDecoder * decoder, int frame_plus_one)
{
	Token *toks = decoder->active_toks_[frame_plus_one].toks;
	if (toks == NULL)
		return;

	Token *tok, *next_tok, *prev_tok = NULL;
	for (tok = toks; tok != NULL; tok = next_tok) {
		next_tok = tok->next;
		if (tok->extra_cost == FLT_MAX) {
			// token is unreachable from end of graph; (no forward links survived)
			// excise tok from list and delete tok.
			if (prev_tok != NULL)
			{
				prev_tok->next = tok->next;
			}
			else
			{
				toks = tok->next;
				decoder->active_toks_[frame_plus_one].toks = toks;
			}
			delete tok;
			decoder->num_toks_--;
		}
		else {  // fetch next Token
			prev_tok = tok;
		}
	}
}

#define OPT_FOR_YINGJIAN
#ifdef OPT_FOR_YINGJIAN
float GetCutoff(LatticeFasterDecoder * decoder, Elem_StateId_Token *list_head, unsigned int *tok_count,
	float *adaptive_beam, Elem_StateId_Token **best_elem)
{
	float best_weight = FLT_MAX;
	size_t count = 0;
	//如果个数无限制(max_active和min_active为个数的最大最小值)，则best_weight取list里的最小值
	if (decoder->config_.max_active == INT_MAX && decoder->config_.min_active == 0) {
		for (Elem_StateId_Token *e = list_head; e != NULL; e = e->tail, count++) {
			float w = static_cast<float>(e->val->tot_cost);
			if (w < best_weight) {
				best_weight = w;
				if (best_elem)
					*best_elem = e;
			}
		}
		if (tok_count != NULL)
			*tok_count = count;
		if (adaptive_beam != NULL)
			*adaptive_beam = decoder->config_.beam;

		return best_weight + decoder->config_.beam;
	}
	else {
		//如果个数有限制,求list的weight最小值，把tot的值存在tmp_array_中
		//后面是保证保留下来的token的个数在min_active和max_active范围内，而计算阈值min_active_cutoff和max_active_cutoff
		int a_tmp[100];
		uint elemCount = 0;
		memset(a_tmp,0,sizeof(a_tmp));
		for (Elem_StateId_Token *e = list_head; e != NULL; e = e->tail, count++) {
			float w = e->val->tot_cost;
			int a = w*10+0.5;
			a=max(a, 0);
			a = min(a, 99);
			a_tmp[a]++;
			elemCount++;
			if (w < best_weight) {
				best_weight = w;
				if (best_elem)
					*best_elem = e;
			}
		}
		if (tok_count != NULL)
			*tok_count = count;

		float beam_cutoff = best_weight + decoder->config_.beam,
			  min_active_cutoff = FLT_MAX,
			  max_active_cutoff = FLT_MAX;

		//list的总数大于max_active，则由小到大排序max_active个，取第max_active个weight为最大阈值
		if (elemCount > static_cast<size_t>(decoder->config_.max_active)) {
			int indx = 99;
			int countTmp = a_tmp[0];
			for (int i = 1; i < 100; i++)
			{
				countTmp += a_tmp[i];
				if (countTmp>decoder->config_.max_active)
				{
					indx = i - 1;
					break;
				}
			}
			max_active_cutoff = indx/10.0;
		}
		if (elemCount > static_cast<size_t>(decoder->config_.min_active)) {
			if (decoder->config_.min_active == 0)
				min_active_cutoff = best_weight;
			else {
				int indx = 99;
				int countTmp = a_tmp[0];
				for (int i = 1; i < 100; i++)
				{
					countTmp += a_tmp[i];
					if (countTmp>decoder->config_.min_active)
					{
						indx = i - 1;
						break;
					}
				}
				min_active_cutoff = indx / 10.0;
			}
		}

		if (max_active_cutoff < beam_cutoff) { 		// max_active is tighter than beam.
			if (adaptive_beam)
				*adaptive_beam = max_active_cutoff - best_weight + decoder->config_.beam_delta;
			return max_active_cutoff;
		}
		else if (min_active_cutoff > beam_cutoff) { // min_active is looser than beam.
			if (adaptive_beam)
				*adaptive_beam = min_active_cutoff - best_weight + decoder->config_.beam_delta;
			return min_active_cutoff;
		}
		else {
			*adaptive_beam = decoder->config_.beam;
			return beam_cutoff;
		}
	}
}
#else
float GetCutoff(LatticeFasterDecoder * decoder,Elem_StateId_Token *list_head, unsigned int *tok_count,
	float *adaptive_beam, Elem_StateId_Token **best_elem)
{
	float best_weight = FLT_MAX;
	size_t count = 0;
	//如果个数无限制(max_active和min_active为个数的最大最小值)，则best_weight取list里的最小值
	if (decoder->config_.max_active == __INT_MAX__ && decoder->config_.min_active == 0) {
		for (Elem_StateId_Token *e = list_head; e != NULL; e = e->tail, count++) {
			float w = static_cast<float>(e->val->tot_cost);
			if (w < best_weight) {
				best_weight = w;
				if (best_elem)
					*best_elem = e;
			}
		}
		if (tok_count != NULL)
			*tok_count = count;
		if (adaptive_beam != NULL)
			*adaptive_beam = decoder->config_.beam;
		return best_weight + decoder->config_.beam;
	}
	else {
		//如果个数有限制,求list的weight最小值，把tot的值存在tmp_array_中
		//后面是保证保留下来的token的个数在min_active和max_active范围内，而计算阈值min_active_cutoff和max_active_cutoff
		decoder->tmp_array_.clear();
		for (Elem_StateId_Token *e = list_head; e != NULL; e = e->tail, count++) {
			float w = e->val->tot_cost;
			decoder->tmp_array_.push_back(w);
			if (w < best_weight) {
				best_weight = w;
				if (best_elem)
					*best_elem = e;
			}
		}
		if (tok_count != NULL)
			*tok_count = count;

		float beam_cutoff = best_weight + decoder->config_.beam,
			  min_active_cutoff = FLT_MAX,
		      max_active_cutoff = FLT_MAX;
		//list的总数大于max_active，则由小到大排序max_active个，取第max_active个weight为最大阈值
		if (decoder->tmp_array_.size() > static_cast<size_t>(decoder->config_.max_active)) {
			std::nth_element(decoder->tmp_array_.begin(),
				decoder->tmp_array_.begin() + decoder->config_.max_active,
				decoder->tmp_array_.end());
			max_active_cutoff = decoder->tmp_array_[decoder->config_.max_active];
		}
		if (decoder->tmp_array_.size() > static_cast<size_t>(decoder->config_.min_active)) {
			if (decoder->config_.min_active == 0)
				min_active_cutoff = best_weight;
			else {
				std::nth_element(decoder->tmp_array_.begin(),
					decoder->tmp_array_.begin() + decoder->config_.min_active,
					decoder->tmp_array_.size() > static_cast<size_t>(decoder->config_.max_active) ?
					decoder->tmp_array_.begin() + decoder->config_.max_active :
					decoder->tmp_array_.end());
				min_active_cutoff = decoder->tmp_array_[decoder->config_.min_active];
			}
		}

		if (max_active_cutoff < beam_cutoff) { // max_active is tighter than beam.
			if (adaptive_beam)
				*adaptive_beam = max_active_cutoff - best_weight + decoder->config_.beam_delta;
			return max_active_cutoff;
		}
		else if (min_active_cutoff > beam_cutoff) { // min_active is looser than beam.
			if (adaptive_beam)
				*adaptive_beam = min_active_cutoff - best_weight + decoder->config_.beam_delta;
			return min_active_cutoff;
		}
		else {
			*adaptive_beam = decoder->config_.beam;
			return beam_cutoff;
		}
	}
}
#endif

void PossiblyResizeHash(LatticeFasterDecoder * decoder,unsigned int num_toks)
{
	size_t new_sz = static_cast<size_t>(static_cast<float>(num_toks) * decoder->config_.hash_ratio);
	if (new_sz > decoder->toks_.hash_size_) 
	{
		decoder->toks_.hash_size_ = new_sz;
		if (new_sz > decoder->toks_.buckets_.size())
		{
			HashBucket_Elem_StateId_Token hashBucket;
			HashBucket_Elem_StateId_Token_Init(&hashBucket, 0, NULL);
			decoder->toks_.buckets_.resize(new_sz, hashBucket);
		}
	}
}

Token *FindOrAddToken(LatticeFasterDecoder * decoder,int state, int frame_plus_one, float tot_cost, bool *changed)
{
	Token *toks = decoder->active_toks_[frame_plus_one].toks;
	Elem_StateId_Token *e_found = HashListFind(&(decoder->toks_), state);
	if (e_found == NULL) {  // no such token presently.
		const float extra_cost = 0.0;
		Token *new_tok = (Token *)malloc(sizeof(Token));
		Token_Init(new_tok, tot_cost, extra_cost, NULL, toks);
		// NULL: no forward links yet
		toks = new_tok;
		decoder->active_toks_[frame_plus_one].toks = toks;
		decoder->num_toks_++;
		HashListInsert(&(decoder->toks_), state, new_tok);

		if (changed)
			*changed = true;
		return new_tok;
	}
	else {
		Token *tok = e_found->val;  // There is an existing Token for this state.
		if (tok->tot_cost > tot_cost) {  // replace old token
			tok->tot_cost = tot_cost;
			if (changed)
				*changed = true;
		}
		else {
			if (changed)
				*changed = false;
		}
		return tok;
	}
}

bool ApproxEqual(float a, float b, float relative_tolerance)
{
	if (a == b) return true;
	float diff = abs(a - b);
	if (diff == FLT_MAX || diff != diff)
		return false;
	return (diff <= relative_tolerance*(abs(a) + abs(b)));
}

void TopSortTokens(Token *tok_list,
	std::vector<Token*> *topsorted_list)
{
	unordered_map<Token*, int32> token2pos;
	typedef unordered_map<Token*, int32>::iterator IterType;
	int32 num_toks = 0;
	for (Token *tok = tok_list; tok != NULL; tok = tok->next)
		num_toks++;
	int32 cur_pos = 0;
	// We assign the tokens numbers num_toks - 1, ... , 2, 1, 0.
	// This is likely to be in closer to topological order than
	// if we had given them ascending order, because of the way
	// new tokens are put at the front of the list.
	for (Token *tok = tok_list; tok != NULL; tok = tok->next)
		token2pos[tok] = num_toks - ++cur_pos;

	unordered_set<Token*> reprocess;
	for (IterType iter = token2pos.begin(); iter != token2pos.end(); ++iter) {
		Token *tok = iter->first;
		int32 pos = iter->second;
		for (ForwardLink *link = tok->links; link != NULL; link = link->next) {
			if (link->ilabel == 0) {
				// We only need to consider epsilon links, since non-epsilon links
				// transition between frames and this function only needs to sort a list
				// of tokens from a single frame.
				IterType following_iter = token2pos.find(link->next_tok);
				if (following_iter != token2pos.end()) { // another token on this frame,
					// so must consider it.
					int32 next_pos = following_iter->second;
					if (next_pos < pos) { // reassign the position of the next Token.
						following_iter->second = cur_pos++;
						reprocess.insert(link->next_tok);
					}
				}
			}
		}
		// In case we had previously assigned this token to be reprocessed, we can
		// erase it from that set because it's "happy now" (we just processed it).
		reprocess.erase(tok);
	}
	//设置最大循环次数的原因是如果帧内部有闭环可能会导致死循环
	size_t max_loop = 1000000, loop_count; // max_loop is to detect epsilon cycles.
	for (loop_count = 0;
		!reprocess.empty() && loop_count < max_loop; ++loop_count) {
		std::vector<Token*> reprocess_vec;
		for (unordered_set<Token*>::iterator iter = reprocess.begin();
			iter != reprocess.end(); ++iter)
			reprocess_vec.push_back(*iter);
		reprocess.clear();
		for (std::vector<Token*>::iterator iter = reprocess_vec.begin();
			iter != reprocess_vec.end(); ++iter) {
			Token *tok = *iter;
			int32 pos = token2pos[tok];
			// Repeat the processing we did above (for comments, see above).
			for (ForwardLink *link = tok->links; link != NULL; link = link->next) {
				if (link->ilabel == 0) {
					IterType following_iter = token2pos.find(link->next_tok);
					if (following_iter != token2pos.end()) {
						int32 next_pos = following_iter->second;
						if (next_pos < pos) {
							following_iter->second = cur_pos++;
							reprocess.insert(link->next_tok);
						}
					}
				}
			}
		}
	}

	topsorted_list->clear();
	topsorted_list->resize(cur_pos, NULL);  // create a list with NULLs in between.
	for (IterType iter = token2pos.begin(); iter != token2pos.end(); ++iter)
		(*topsorted_list)[iter->second] = iter->first;
}

bool ReachedFinal(LatticeFasterDecoder * decoder)
{
	if (!decoder->decoding_finalized_) {
		float relative_cost;
		ComputeFinalCosts(decoder, NULL, &relative_cost, NULL);
		return relative_cost != FLT_MAX;
	}
	else {
		// we're not allowed to call that function if FinalizeDecoding() has
		// been called; return a cached value.
		return decoder->final_relative_cost_ != FLT_MAX;
	}
}
