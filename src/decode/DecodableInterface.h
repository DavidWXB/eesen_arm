#ifndef DECODABLEINTERFACE_
#define DECODABLEINTERFACE_

#include "../utils/kaldi-types.h"

struct Matrix_Float
{
	float* data_;		// data memory
	int    num_cols_;   // Number of columns
	int    num_rows_;   // Number of rows
	int    stride_;		// Number byte of each rows
};

struct DecodableInterface
{
	Matrix_Float likes_;
	float scale_;
};

float DecodableInterfaceLogLikelihood(DecodableInterface *decodable, int frame, int tid);
bool DecodableInterfaceIsLastFrame(DecodableInterface *decodable, int frame);

void DecodableInterfaceInit(DecodableInterface *decodable, Matrix_Float *matrix, float acoustic_scale);
void DecodableInterfaceDelete(DecodableInterface *decodable);

#endif
