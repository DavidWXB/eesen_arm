#include <stdlib.h>
#include "TokenList.h"
#include "HashList_StateId_Token.h"

ForwardLink* ForwardLink_Init(Token *next_tok, int ilabel, int olabel,
	float graph_cost, float acoustic_cost, ForwardLink *next)
{
	ForwardLink *forwardLink = (ForwardLink *)malloc(sizeof(ForwardLink));
	forwardLink->next_tok = next_tok;
	forwardLink->ilabel = ilabel;
	forwardLink->olabel = olabel;
	forwardLink->graph_cost = graph_cost;
	forwardLink->acoustic_cost = acoustic_cost;
	forwardLink->next = next;

	return forwardLink;
}

void Token_Init(Token *token, float tot_cost, float extra_cost, ForwardLink *links, Token *next)
{
	token->tot_cost = tot_cost;
	token->extra_cost = extra_cost;
	token->links = links;
	token->next = next;
}

void DeleteForwardLinks(Token *token) {
	ForwardLink *l = token->links, *m;
	while (l != NULL) {
		m = l->next;
		delete l;
		l = m;
	}
	token->links = NULL;
}

void TokenList_Init(TokenList *tokenList)
{
	tokenList->toks = NULL;
	tokenList->must_prune_forward_links = true;
	tokenList->must_prune_tokens = true;
}

void TokenList_Init2(TokenList *tokenList, Token *toks, bool must_prune_forward_links, bool must_prune_tokens)
{
	tokenList->toks = toks;
	tokenList->must_prune_forward_links = must_prune_forward_links;
	tokenList->must_prune_tokens = must_prune_tokens;
}
