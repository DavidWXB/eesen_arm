#include <stddef.h>
#include <stdlib.h>
#include "HashList_StateId_Token.h"

void HashBucket_Elem_StateId_Token_Init(HashBucket_Elem_StateId_Token *HashBucket_Elem, size_t i, Elem_StateId_Token *e)
{
	HashBucket_Elem->prev_bucket = i;
	HashBucket_Elem->last_elem = e;
}

void HashListDelete(HashList_StateId_Token *hashlist,Elem_StateId_Token *e)
{
	e->tail = hashlist->freed_head_;
	hashlist->freed_head_ = e;
}

//递归函数,开辟token空间，一次开辟1024个，用freed_head_指针记录有用的指针位置
Elem_StateId_Token* New_Elem_StateId_Token_Init(HashList_StateId_Token *hashlist)
{
	if (hashlist->freed_head_) {
		Elem_StateId_Token *ans = hashlist->freed_head_;
		hashlist->freed_head_ = hashlist->freed_head_->tail;

		return ans;
	}
	else {
		//一次性开辟hashlist->allocate_block_size_（1024）个节点
		Elem_StateId_Token *tmp = (Elem_StateId_Token *)malloc(hashlist->allocate_block_size_*sizeof(Elem_StateId_Token));
		//把新开辟的token都串起来
		for (size_t i = 0; i + 1 < hashlist->allocate_block_size_; i++)
			tmp[i].tail = tmp + i + 1;
		//新加最后节点的token的tail赋值null
		tmp[hashlist->allocate_block_size_ - 1].tail = NULL;
		hashlist->freed_head_ = tmp;
		hashlist->allocated_.push_back(tmp);

		return New_Elem_StateId_Token_Init(hashlist);
	}
}

Elem_StateId_Token* HashListClear(HashList_StateId_Token *hashlist)
{
	for (size_t cur_bucket = hashlist->bucket_list_tail_;
		cur_bucket != static_cast<size_t>(-1);
		cur_bucket = hashlist->buckets_[cur_bucket].prev_bucket) {
		hashlist->buckets_[cur_bucket].last_elem = NULL;  // this is how we indicate "empty".
	}
	hashlist->bucket_list_tail_ = static_cast<size_t>(-1);
	Elem_StateId_Token *ans = hashlist->list_head_;
	hashlist->list_head_ = NULL;

	return ans;
}

void HashListSetSize(HashList_StateId_Token *hashlist, size_t size)
{
	hashlist->hash_size_ = size;
	hashlist->bucket_list_tail_ == static_cast<size_t>(-1);
	if (size > hashlist->buckets_.size()) {
		HashBucket_Elem_StateId_Token HashBucket_Elem;
		HashBucket_Elem_StateId_Token_Init(&HashBucket_Elem, 0, NULL);
		hashlist->buckets_.resize(size, HashBucket_Elem);
	}
}

void HashListGetList(HashList_StateId_Token *hashlist)
{
}


void HashListInsert(HashList_StateId_Token *tokenList, int key, Token* val)
{
	size_t index = (static_cast<size_t>(key) % tokenList->hash_size_);
	HashBucket_Elem_StateId_Token &bucket = tokenList->buckets_[index];
	Elem_StateId_Token *elem = New_Elem_StateId_Token_Init(tokenList);
	elem->key = key;
	elem->val = val;

	if (bucket.last_elem == NULL) {  // Unoccupied bucket.  Insert at
		// head of bucket list (which is tail of regular list, they go in
		// opposite directions).
		if (tokenList->bucket_list_tail_ == static_cast<size_t>(-1)) {
			// list was empty so this is the first elem.
			tokenList->list_head_ = elem;
		}
		else {
			// link in to the chain of Elems
			tokenList->buckets_[tokenList->bucket_list_tail_].last_elem->tail = elem;
		}
		elem->tail = NULL;
		bucket.last_elem = elem;
		bucket.prev_bucket = tokenList->bucket_list_tail_;
		tokenList->bucket_list_tail_ = index;
	}
	else {
		// Already-occupied bucket.  Insert at tail of list of elements within
		// the bucket.
		elem->tail = bucket.last_elem->tail;
		bucket.last_elem->tail = elem;
		bucket.last_elem = elem;
	}
}

Elem_StateId_Token *HashListFind(HashList_StateId_Token *tokenList, int key)
{
	size_t index = (static_cast<size_t>(key) % tokenList->hash_size_);
	HashBucket_Elem_StateId_Token &bucket = tokenList->buckets_[index];
	if (bucket.last_elem == NULL) {
		return NULL;  // empty bucket.
	}
	else {
		Elem_StateId_Token *head = (bucket.prev_bucket == static_cast<size_t>(-1) ?
			tokenList->list_head_ :
			tokenList->buckets_[bucket.prev_bucket].last_elem->tail),
				   *tail = bucket.last_elem->tail;
		for (Elem_StateId_Token *e = head; e != tail; e = e->tail)
			if (e->key == key)
				return e;

		return NULL;  // Not found.
	}
}
