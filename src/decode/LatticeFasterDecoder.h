#ifndef LATTICE_FASTER_DECODER_
#define LATTICE_FASTER_DECODER_

#include <vector>
#include <unordered_map>
#include "TokenList.h"
#include "DecodableInterface.h"
#include "HashList_StateId_Token.h"
#include "../wfst/VectorFstOfStdArc.h"
#include "LatticeFasterDecoderConfig.h"

using namespace eesen;

struct  LatticeFasterDecoder
{
	HashList_StateId_Token toks_;
	std::vector<TokenList> active_toks_; 	// Lists of tokens, indexed by
	std::vector<int> queue_;  				// temp variable used in ProcessNonemitting,
	std::vector<float> tmp_array_;  		// used in GetCutoff.
	VectorFstOfStdArc fst_;
	std::vector<float> cost_offsets_; 		//存储每一帧最优的token// This contains, for
	LatticeFasterDecoderConfig config_;
	int num_toks_; 							// current total #toks allocated...
	bool decoding_finalized_;
	unordered_map<Token*, float> final_costs_;
	float final_relative_cost_;
	float final_best_cost_;
};
void LatticeFasterDecoderInit(LatticeFasterDecoder * decoder, VectorFstOfStdArc *fst,
	LatticeFasterDecoderConfig config);

bool DeleteLatticeFasterDecoder(LatticeFasterDecoder * decoder);

bool Decode(LatticeFasterDecoder * decoder, DecodableInterface *decodable);

void InitDecoding(LatticeFasterDecoder * decoder);

int NumFramesDecoded(LatticeFasterDecoder * decoder);

void DeleteElems(HashList_StateId_Token *toks_, Elem_StateId_Token *elem);

void ClearActiveTokens(LatticeFasterDecoder * decoder);
void ProcessEmitting(LatticeFasterDecoder * decoder, DecodableInterface *decodable);
void ProcessNonemitting(LatticeFasterDecoder * decoder);

void FinalizeDecoding(LatticeFasterDecoder * decoder);
void ComputeFinalCosts(LatticeFasterDecoder * decoder, unordered_map<Token*, float> *final_costs, float *final_relative_cost, float *final_best_cost);

void PruneForwardLinksFinal(LatticeFasterDecoder * decoder);

void PruneActiveTokens(LatticeFasterDecoder * decoder, float delta);

void PruneForwardLinks(LatticeFasterDecoder * decoder, int frame_plus_one, bool *extra_costs_changed,
	bool *links_pruned, float delta);

void PruneTokensForFrame(LatticeFasterDecoder * decoder, int frame_plus_one);

float GetCutoff(LatticeFasterDecoder * decoder, Elem_StateId_Token *list_head, unsigned int *tok_count,
	float *adaptive_beam, Elem_StateId_Token **best_elem);

void PossiblyResizeHash(LatticeFasterDecoder * decoder, unsigned int num_toks);

Token *FindOrAddToken(LatticeFasterDecoder * decoder, int state, int frame_plus_one, float tot_cost, bool *changed);

bool ApproxEqual(float a, float b, float relative_tolerance = 0.001);


void TopSortTokens(Token *tok_list, std::vector<Token*> *topsorted_list);

bool ReachedFinal(LatticeFasterDecoder * decoder);

void DestroyHashList(HashList_StateId_Token *tok_);

#endif
