#ifndef HASHLIST_STATEID_TOKEN_
#define HASHLIST_STATEID_TOKEN_

#include <vector>
#include "TokenList.h"

struct HashBucket_Elem_StateId_Token;
struct Elem_StateId_Token {
	int key;
	Token *val;
	Elem_StateId_Token *tail;
};

struct HashBucket_Elem_StateId_Token
{
	size_t prev_bucket;  	// index to next bucket (-1 if list tail).  Note: list of buckets
							// goes in opposite direction to list of Elems.
	Elem_StateId_Token *last_elem;  // pointer to last element in this bucket (NULL if empty)
};

struct HashList_StateId_Token
{
	Elem_StateId_Token *list_head_;  	// head of currently stored list.
	size_t bucket_list_tail_;  			// tail of list of active hash buckets.
	size_t hash_size_;  				// number of hash buckets.
	std::vector<HashBucket_Elem_StateId_Token> buckets_;
	Elem_StateId_Token *freed_head_; 	//有用token的最后一个节点，在freed_head_之后的节点都是暂时没有用到的 // head of list of currently freed elements. [ready for allocation]
	std::vector<Elem_StateId_Token*> allocated_;  		// list of allocated blocks.
	static const size_t allocate_block_size_ = 1024;  	// Number of Elements to allocate in one block.  Must be
};

void HashBucket_Elem_StateId_Token_Init(HashBucket_Elem_StateId_Token *HashBucket_Elem, size_t i, Elem_StateId_Token *e);

void HashListDelete(HashList_StateId_Token *hashlist, Elem_StateId_Token *e);
Elem_StateId_Token* HashListClear(HashList_StateId_Token *hashlist);
void HashListGetList(HashList_StateId_Token *hashlist);
Elem_StateId_Token *HashListFind(int stateId);
void HashListSetSize(HashList_StateId_Token *hashlist, size_t size);
void HashListInsert(HashList_StateId_Token *tokenList, int key, Token* val);
Elem_StateId_Token *HashListFind(HashList_StateId_Token *tokenList, int key);

#endif
