#ifndef LATTICE_FASTER_DECODER_CONFIG_
#define LATTICE_FASTER_DECODER_CONFIG_

//解码参数类
struct DeterminizeLatticePhonePrunedOptions
{
	// delta: a small offset used to measure equality of weights.
	float delta;
	// max_mem: if > 0, determinization will fail and return false when the
	// algorithm's (approximate) memory consumption crosses this threshold.
	int max_mem;
	// phone_determinize: if true, do a first pass determinization on both phones
	// and words.
	bool phone_determinize;
	// word_determinize: if true, do a second pass determinization on words only.
	bool word_determinize;
	// minimize: if true, push and minimize after determinization.
	bool minimize;
};

struct LatticeFasterDecoderConfig
{
	float beam;                 //加链阈值
	int max_active;
	int min_active;
	float lattice_beam;         //残差裁剪阈值
	int prune_interval;
	bool determinize_lattice;   // not inspected by this class... used in command-line program.
	float beam_delta;           // has nothing to do with beam_ratio
	float hash_ratio;
	float prune_scale;          // Note: we don't make this configurable on the command line,
	DeterminizeLatticePhonePrunedOptions det_opts;
};

#endif
