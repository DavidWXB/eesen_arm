#include "DecodableInterface.h"

float LikesGetData(Matrix_Float *matrix, int r, int c)
{
	return *(matrix->data_ + r * matrix->stride_ + c);
}

float DecodableInterfaceLogLikelihood(DecodableInterface *decodable, int frame, int tid)
{
	return decodable->scale_ * LikesGetData(&(decodable->likes_), frame, tid - 1);
}

bool DecodableInterfaceIsLastFrame(DecodableInterface *decodable, int frame)
{
	return (frame == decodable->likes_.num_rows_ - 1);
}

void DecodableInterfaceInit(DecodableInterface *decodable, Matrix_Float *matrix, float acoustic_scale)
{
	decodable->likes_ = *matrix;
	decodable->scale_ = acoustic_scale;
}

void DecodableInterfaceDelete(DecodableInterface *decodable)
{
}
