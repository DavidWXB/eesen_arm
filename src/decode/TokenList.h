#ifndef TOKENLIST_
#define TOKENLIST_

struct Token;
struct ForwardLink {
	Token *next_tok; 		// the next token [or NULL if represents final-state]
	int ilabel; 			// ilabel on link.
	int olabel; 			// olabel on link.
	float graph_cost; 		// graph cost of traversing link (contains LM, etc.)
	float acoustic_cost; 	// acoustic cost (pre-scaled) of traversing link
	ForwardLink *next; 		// next in singly-linked list of forward links from a token.
};

void ForwardLink_Init(ForwardLink *forwardLink, Token *next_tok, int ilabel, int olabel,
	float graph_cost, float acoustic_cost, ForwardLink *next);

struct Token {
	float tot_cost; 	// would equal weight.Value()... cost up to this point.
	float extra_cost;	//路径相交时当前路径cost减去最小路径cost // >= 0.  After
	ForwardLink *links; // Head of singly linked list of ForwardLinks
	Token *next; 		// Next in list of tokens for this frame.
	Token *last; 		//sh 2016-12-01 上一帧token的指针
#ifdef OPT_TEST
	int num;			//sh 2016-12-02 当前Token属于哪一帧
#endif
};

void Token_Init(Token *token, float tot_cost, float extra_cost, ForwardLink *links, Token *next);

void DeleteForwardLinks(Token *token);

struct TokenList {
	Token *toks;
	bool must_prune_forward_links;
	bool must_prune_tokens;
};

void TokenList_Init(TokenList *tokenList);
void TokenList_Init2(TokenList *tokenList, Token *toks, bool must_prune_forward_links, bool must_prune_tokens);
ForwardLink* ForwardLink_Init(Token *next_tok, int ilabel, int olabel,
	float graph_cost, float acoustic_cost, ForwardLink *next);
void Token_Init(Token *token, float tot_cost, float extra_cost, ForwardLink *links, Token *next);

#endif
