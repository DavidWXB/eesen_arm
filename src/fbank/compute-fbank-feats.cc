// featbin/compute-fbank-feats.cc

// Copyright 2009-2012  Microsoft Corporation
//                      Johns Hopkins University (author: Daniel Povey)

// See ../../COPYING for clarification regarding multiple authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
// WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
// MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache 2 License for the specific language governing permissions and
// limitations under the License.
#include "feature-fbank.h"
#include "../utils/wave-reader.h"
#include "../utils/kaldi-common.h"
#include "../utils/common-utils.h"
#include "xil_types.h"

// global variable declare
extern float recored_04_c0[145464];

extern u32 const Words=32000;
extern float data_audio[Words];
extern float data_audio_0[43537];

// global variable definition
eesen::Matrix<double> stats;
eesen::Matrix<eesen::BaseFloat> feats;
eesen::Matrix<eesen::BaseFloat> features;
eesen::CompressedMatrix compressed_features;
eesen::Matrix<eesen::BaseFloat> fbank_output;

int main_fbank() {
	printf("fbank...\n");

    try {
    	using namespace eesen;

        // construct all the global objects
        FbankOptions fbank_opts;
        bool subtract_mean = false;
        BaseFloat vtln_warp = 1.0;
        Fbank fbank(fbank_opts);

        // body
        int this_chan = 0;

        //SubVector<BaseFloat> waveform(recored_04_c0, this_chan, 145464);

        //SubVector<BaseFloat> waveform(data_audio, this_chan, 145464);
        SubVector<BaseFloat> waveform(data_audio, this_chan, 64000);

        // fbank+lstm+wsft -- okay
        //SubVector<BaseFloat> waveform(data_audio_0, this_chan, 43537);
        try {
            fbank.Compute(waveform, vtln_warp, &features, NULL);
        } catch (...) {
            KALDI_WARN << "Failed to compute features for utterance";
        }

        if (subtract_mean) {
            Vector<BaseFloat> mean(features.NumCols());
            mean.AddRowSumMat(1.0, features);
            mean.Scale(1.0 / features.NumRows());
            for (int32 i = 0; i < features.NumRows(); i++)
                features.Row(i).AddVec(-1.0, mean);
        }
        return  0;
    } catch(const std::exception &e) {
        std::cerr << e.what();
        return -1;
    }
}

