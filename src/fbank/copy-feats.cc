// featbin/copy-feats.cc

// Copyright 2009-2011  Microsoft Corporation
//                2013  Johns Hopkins University (author: Daniel Povey)

// See ../../COPYING for clarification regarding multiple authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
// WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
// MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache 2 License for the specific language governing permissions and
// limitations under the License.
#include "../utils/matrix.h"
#include "../utils/kaldi-common.h"
#include "../utils/common-utils.h"

// global variable declare
extern eesen::Matrix<eesen::BaseFloat> features;
extern eesen::CompressedMatrix compressed_features;

/// TODO remove un-used Branch, accutually support the only one is enough.
int main_copyfeats() {
	printf("copyfeats...\n");

    try {
        using namespace eesen;

        ParseOptions po(NULL);
        bool binary = true;
        bool htk_in = false;
        bool sphinx_in = false;
        bool compress  = true;

        if (true) {
            // Copying tables of features.
        	int32 num_done = 0;
            std::string rspecifier;
            std::string wspecifier;

            if (!compress) {	// un-compress
                BaseFloatMatrixWriter kaldi_writer(wspecifier);
                if (htk_in) {
                    SequentialTableReader<HtkMatrixHolder> htk_reader(rspecifier);
                    for (; !htk_reader.Done(); htk_reader.Next(), num_done++)
                        kaldi_writer.Write(htk_reader.Key(), htk_reader.Value().first);
                } else if (sphinx_in) {
                    SequentialTableReader<SphinxMatrixHolder<> > sphinx_reader(rspecifier);
                    for (; !sphinx_reader.Done(); sphinx_reader.Next(), num_done++)
                        kaldi_writer.Write(sphinx_reader.Key(), sphinx_reader.Value());
                } else {
                    SequentialBaseFloatMatrixReader kaldi_reader(rspecifier);
                    for (; !kaldi_reader.Done(); kaldi_reader.Next(), num_done++)
                        kaldi_writer.Write(kaldi_reader.Key(), kaldi_reader.Value());
                }
            } else {			// compress
                CompressedMatrixWriter kaldi_writer(wspecifier);
                if (htk_in) {
                    SequentialTableReader<HtkMatrixHolder> htk_reader(rspecifier);
                    for (; !htk_reader.Done(); htk_reader.Next(), num_done++)
                        kaldi_writer.Write(htk_reader.Key(), CompressedMatrix(htk_reader.Value().first));
                } else if (sphinx_in) {
                    SequentialTableReader<SphinxMatrixHolder<> > sphinx_reader(rspecifier);
                    for (; !sphinx_reader.Done(); sphinx_reader.Next(), num_done++)
                        kaldi_writer.Write(sphinx_reader.Key(), CompressedMatrix(sphinx_reader.Value()));
                } else {	// only support
                	compressed_features.CopyFromMat(features);
                	features.~Matrix();	// free space used by features
                }
            }

            return 0;
        } else {
            KALDI_ASSERT(!compress && "Compression not yet supported for single files");

            std::string feat_rxfilename = po.GetArg(1), feat_wxfilename = po.GetArg(2);

            Matrix<BaseFloat> feat_matrix;
            if (htk_in) {
                Input ki(feat_rxfilename); 	// Doesn't look for read binary header \0B, because no bool* pointer supplied.
                HtkHeader header; 			// we discard this info.
                ReadHtk(ki.Stream(), &feat_matrix, &header);
            } else if (sphinx_in) {
                KALDI_ERR << "For single files, sphinx input is not yet supported.";
            } else {
                ReadKaldiObject(feat_rxfilename, &feat_matrix);
            }

            WriteKaldiObject(feat_matrix, feat_wxfilename, binary);
            KALDI_LOG << "Copied features from " << PrintableRxfilename(feat_rxfilename)
                      << " to " << PrintableWxfilename(feat_wxfilename);
            return 0;
        }
    } catch(const std::exception &e) {
        std::cerr << e.what();
        return -1;
    }
}


